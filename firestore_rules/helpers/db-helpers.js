const { assertFails, assertSucceeds } = require("@firebase/testing");
const firebase = require("@firebase/testing");

module.exports.readSuccess = async (db, docPath, doc) => {
  let ref;
  if (doc == true) {
    ref = db.doc(docPath);
  } else {
    ref = db.collection(docPath);
  }
  return assertSucceeds(ref.get());
};

module.exports.readFailure = async (db, docPath, doc) => {
  let ref;
  if (doc == true) {
    ref = db.doc(docPath);
  } else {
    ref = db.collection(docPath);
  }
  return assertFails(ref.get());
};

module.exports.createSuccess = async (db, collectionPath, projectID) => {
  firebase.clearFirestoreData({
    projectId: projectID
  });
  let ref = db.collection(collectionPath);
  return assertSucceeds(ref.add({}));
};

module.exports.createFailure = async (db, collectionPath) => {
  let ref = db.collection(collectionPath);
  return assertFails(ref.add({}));
};

module.exports.createDocumentSuccess = async (db, docPath, projectID) => {
  firebase.clearFirestoreData({
    projectId: projectID
  });
  let ref = db.doc(docPath);
  return assertSucceeds(ref.set({}));
};

module.exports.createDocumentFailure = async (db, docPath, projectID) => {
  firebase.clearFirestoreData({
    projectId: projectID
  });
  let ref = db.doc(docPath);
  return assertFails(ref.set({}));
};

module.exports.deleteSuccess = async (db, docPath) => {
  let ref = db.doc(docPath);
  return assertSucceeds(ref.delete());
};

module.exports.deleteFailure = async (db, docPath) => {
  let ref = db.doc(docPath);
  return assertFails(ref.delete());
};

module.exports.updateSuccess = async (db, docPath) => {
  let ref = db.doc(docPath);
  return assertSucceeds(ref.update({ abc: 1 }));
};

module.exports.updateFailure = async (db, docPath) => {
  let ref = db.doc(docPath);
  return assertFails(ref.update({ abc: 1 }));
};
