const { setup, setupWithProjectID } = require("./helpers");

const mockData = {
  "users/admin": {
    uid: "admin",
    roles: {
      admin: true
    },
    email: "a@c.com"
  },
  "users/customer": {
    uid: "customer",
    roles: {
      customer: true
    },
    email: "c@b.com"
  },
  "users/worker": {
    uid: "worker",
    roles: {
      worker: true
    },
    email: "w@b.com"
  },
  "users/receptionist": {
    uid: "receptionist",
    roles: {
      worker: true,
      receptionist: true
    },
    email: "r@b.com"
  },
  "users/inspector": {
    uid: "inspector",
    roles: {
      worker: true,
      inspector: true
    },
    email: "i@b.com"
  },
  "users/subvendor": {
    uid: "subvendor",
    roles: {
      subvendor: true
    },
    email: "s@b.com"
  },
  "users/owner": {
    uid: "owner",
    roles: {
      owner: true
    },
    email: "a@b.com"
  },
  "fcmTokens/admin": {
    token: "asdf"
  },
  "fcmTokens/owner": {
    token: "asdf"
  },
  "meta/billingDetails": {
    email: "asd.com"
  },
  "meta/departments": {
    list: [
      {
        name: "punching"
      },
      {
        name: "punching2"
      }
    ]
  },
  "meta/dueDate": {
    date: "5",
    month: "12",
    year: "2019"
  },
  "meta/modules": {
    basic: true
  },
  "meta/owner": {
    pin: "asdfff"
  },
  "meta/usersLimit": {
    admin: {
      current: 1,
      infinite: true
    },
    customer: {
      current: 1,
      infinite: true
    },
    subvendor: {
      current: 1,
      infinite: true
    }
  },
  "customerSummary/customer": {
    creditAmount: 123,
    name: "Babu",
    uid: "customer"
  },
  "subvendorSummary/subvendor": {
    creditAmount: 123,
    name: "Babu2",
    uid: "subvendor"
  },
  "workerSummary/worker": {
    creditAmount: 123,
    name: "Babu3",
    uid: "worker"
  },
  "orders/id1": {
    id: "id1",
    customerUID: "customer",
    status: "pending",
    currentSubProcessSubvendorUID: ["subvendor"]
  },
  "orders/id2": {
    id: "id2",
    customerUID: "customer2",
    status: "pending",
    currentSubProcessSubvendorUID: ["subvendor2"]
  },
  "receipts/r1": {
    id: "r1"
  },
  "bills/b1": {
    id: "b1"
  },
  "transactions/t1": {
    id: "t1"
  }
};

module.exports.setupAdmin = async () => {
  return setup({ email: "a@b.com", uid: "admin", admin: true }, mockData);
};
module.exports.setupOwner = async () => {
  return setup({ email: "o@b.com", uid: "owner", owner: true }, mockData);
};
module.exports.setupReceptionist = async () => {
  return setup(
    { email: "r@b.com", uid: "receptionist", receptionist: true },
    mockData
  );
};
module.exports.setupInspector = async () => {
  return setup(
    { email: "i@b.com", uid: "inspector", inspector: true },
    mockData
  );
};
module.exports.setupWorker = async () => {
  return setup({ email: "w@b.com", uid: "worker", worker: true }, mockData);
};
module.exports.setupCustomer = async () => {
  return setup({ email: "c@b.com", uid: "customer", customer: true }, mockData);
};
module.exports.setupSubvendor = async () => {
  return setup(
    { email: "s@b.com", uid: "subvendor", subvendor: true },
    mockData
  );
};

module.exports.setupUnauthenticatedUser = async () => {
  return setup(null, mockData);
};

module.exports.setupAdminWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "a@b.com", uid: "admin", admin: true },
    mockData,
    projectID
  );
};
module.exports.setupOwnerWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "o@b.com", uid: "owner", owner: true },
    mockData,
    projectID
  );
};
module.exports.setupReceptionistWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "r@b.com", uid: "receptionist", receptionist: true },
    mockData,
    projectID
  );
};
module.exports.setupInspectorWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "i@b.com", uid: "inspector", inspector: true },
    mockData,
    projectID
  );
};
module.exports.setupWorkerWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "w@b.com", uid: "worker", worker: true },
    mockData,
    projectID
  );
};
module.exports.setupCustomerWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "c@b.com", uid: "customer", customer: true },
    mockData,
    projectID
  );
};
module.exports.setupSubvendorWithProjectID = async projectID => {
  return setupWithProjectID(
    { email: "s@b.com", uid: "subvendor", subvendor: true },
    mockData,
    projectID
  );
};

module.exports.setupUnauthenticatedUserWithProjectID = async projectID => {
  return setupWithProjectID(null, mockData, projectID);
};
