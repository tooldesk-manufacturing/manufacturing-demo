const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupInspectorWithProjectID,
  setupAdminWithProjectID,
  setupCustomer,
  setupCustomerWithProjectID,
  setupOwnerWithProjectID,
  setupReceptionist,
  setupReceptionistWithProjectID,
  setupSubvendor,
  setupSubvendorWithProjectID,
  setupUnauthenticatedUser,
  setupUnauthenticatedUserWithProjectID,
  setupWorker,
  setupWorkerWithProjectID
} = require("../helpers/login-helper");

const {
  readSuccess,
  readFailure,
  deleteFailure,
  createSuccess,
  createFailure,
  updateSuccess,
  updateFailure
} = require("../helpers/db-helpers");
describe("Order rules", () => {
  test("admin should be able to read", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "orders/id1", true));
  });
  test("owner should be able to read", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "orders/id1", true));
  });
  test("receptionist should be able to read", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "orders/id1", true));
  });
  test("inspector should be able to read", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "orders/id1", true));
  });
  test("worker should be able to read", async () => {
    let db = await setupWorker();
    expect(await readSuccess(db, "orders/id1", true));
  });
  test("customer should be able to read", async () => {
    let db = await setupCustomer();
    expect(await readSuccess(db, "orders/id1", true));
  });
  test("customer should not be able to read other customer orders", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "orders/id2", true));
  });
  test("subvendor should be able to read", async () => {
    let db = await setupSubvendor();
    expect(await readSuccess(db, "orders/id1", true));
  });

  test("subvendor should not be able to read other subvendor orders", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "orders/id2", true));
  });

  test("unauthenticated should not be able to read", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "orders/id1", true));
  });

  test("admin should be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupAdminWithProjectID(projectId);
    expect(await createSuccess(db, "orders", projectId));
  });
  test("owner should be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupOwnerWithProjectID(projectId);
    expect(await createSuccess(db, "orders", projectId));
  });
  test("receptionist should be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupReceptionistWithProjectID(projectId);
    expect(await createSuccess(db, "orders", projectId));
  });
  test("inspector should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupInspectorWithProjectID(projectId);
    expect(await createFailure(db, "orders", projectId));
  });
  test("worker should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupWorkerWithProjectID(projectId);
    expect(await createFailure(db, "orders", projectId));
  });
  test("customer should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupCustomerWithProjectID(projectId);
    expect(await createFailure(db, "orders", projectId));
  });
  test("subvendor should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupSubvendorWithProjectID(projectId);
    expect(await createFailure(db, "orders", projectId));
  });
  test("unauthenticated should not be able to  createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupUnauthenticatedUserWithProjectID(projectId);
    expect(await createFailure(db, "orders", projectId));
  });

  test("admin should be able to update", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "orders/id1"));
  });
  test("owner should be able to update", async () => {
    let db = await setupOwner();
    expect(await updateSuccess(db, "orders/id1"));
  });
  test("receptionist should be able to update", async () => {
    let db = await setupReceptionist();
    expect(await updateSuccess(db, "orders/id1"));
  });
  test("inspector should be able to update", async () => {
    let db = await setupInspector();
    expect(await updateSuccess(db, "orders/id1"));
  });
  test("worker should be able to update", async () => {
    let db = await setupWorker();
    expect(await updateSuccess(db, "orders/id1"));
  });
  test("customer not should be able to update", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "orders/id1"));
  });
  test("subvendor should be able to update", async () => {
    let db = await setupSubvendor();
    expect(await updateSuccess(db, "orders/id1"));
  });
  test("unauthenticated should not be able to  update", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "orders/id1"));
  });

  test("admin should not be able to delete", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("owner should not be able to delete", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("receptionist should not be able to delete", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("inspector should not be able to delete", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("worker should not be able to delete", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("customer should not be able to delete", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("subvendor should not be able to delete", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "orders/id1"));
  });
  test("unauthenticated should not be able to  delete", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "orders/id1"));
  });
  afterAll(async () => {
    await teardown();
  });
});
