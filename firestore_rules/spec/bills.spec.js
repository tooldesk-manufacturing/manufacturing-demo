const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupInspectorWithProjectID,
  setupAdminWithProjectID,
  setupCustomer,
  setupCustomerWithProjectID,
  setupOwnerWithProjectID,
  setupReceptionist,
  setupReceptionistWithProjectID,
  setupSubvendor,
  setupSubvendorWithProjectID,
  setupUnauthenticatedUser,
  setupUnauthenticatedUserWithProjectID,
  setupWorker,
  setupWorkerWithProjectID
} = require("../helpers/login-helper");

const {
  readSuccess,
  readFailure,
  deleteFailure,
  createSuccess,
  createFailure,
  updateFailure
} = require("../helpers/db-helpers");
describe("Bills rules", () => {
  test("admin should be able to read", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "bills/b1", true));
  });
  test("owner should be able to read", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "bills/b1", true));
  });
  test("receptionist should be able to read", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "bills/b1", true));
  });
  test("inspector should not be able to read", async () => {
    let db = await setupInspector();
    expect(await readFailure(db, "bills/b1", true));
  });
  test("worker should not be able to read", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "bills/b1", true));
  });
  test("customer should not be able to read", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "bills/b1", true));
  });
  test("subvendor should not be able to read", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "bills/b1", true));
  });
  test("unauthenticated should not be able to read", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "bills/b1", true));
  });
  test("admin should be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupAdminWithProjectID(projectId);
    expect(await createSuccess(db, "bills", projectId));
  });
  test("owner should be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupOwnerWithProjectID(projectId);
    expect(await createSuccess(db, "bills", projectId));
  });
  test("receptionist should be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupReceptionistWithProjectID(projectId);
    expect(await createSuccess(db, "bills", projectId));
  });
  test("inspector should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupInspectorWithProjectID(projectId);
    expect(await createFailure(db, "bills", projectId));
  });
  test("worker should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupWorkerWithProjectID(projectId);
    expect(await createFailure(db, "bills", projectId));
  });
  test("customer should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupCustomerWithProjectID(projectId);
    expect(await createFailure(db, "bills", projectId));
  });
  test("subvendor should not be able to createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupSubvendorWithProjectID(projectId);
    expect(await createFailure(db, "bills", projectId));
  });
  test("unauthenticated should not be able to  createDocument", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupUnauthenticatedUserWithProjectID(projectId);
    expect(await createFailure(db, "bills", projectId));
  });

  test("admin should not be able to update", async () => {
    let db = await setupAdmin();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("owner should not be able to update", async () => {
    let db = await setupOwner();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("receptionist not should be able to update", async () => {
    let db = await setupReceptionist();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("inspector should not be able to update", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("worker should not be able to update", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("customer should not be able to update", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("subvendor should not be able to update", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "bills/b1"));
  });
  test("unauthenticated should not be able to  update", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "bills/b1"));
  });

  test("admin should not be able to delete", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("owner should not be able to delete", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("receptionist should not be able to delete", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("inspector should not be able to delete", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("worker should not be able to delete", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("customer should not be able to delete", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("subvendor should not be able to delete", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "bills/b1"));
  });
  test("unauthenticated should not be able to  delete", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "bills/b1"));
  });
  afterAll(async () => {
    await teardown();
  });
});
