const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupCustomer,
  setupReceptionist,
  setupSubvendor,
  setupUnauthenticatedUser,
  setupWorker
} = require("../helpers/login-helper");

const {
  readSuccess,
  readFailure,
  deleteFailure,
  updateFailure,
  updateSuccess
} = require("../helpers/db-helpers");

describe("Subvendor summary rules", () => {
  test("admin should be able to read", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "subvendorSummary/subvendor", true));
  });
  test("owner should be able to read", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "subvendorSummary/subvendor", true));
  });
  test("receptionist should be able to read", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "subvendorSummary/subvendor", true));
  });
  test("inspector should  be able to read", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "subvendorSummary/subvendor", true));
  });
  test("worker should not be able to read", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "subvendorSummary/subvendor", true));
  });
  test("customer should not be able to read", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "subvendorSummary/subvendor", true));
  });
  test("subvendor should not be able to read", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "subvendorSummary/subvendor", true));
  });
  test("unauthenticated should not be able to read", async () => {
    let db = await setupUnauthenticatedUser();

    expect(await readFailure(db, "subvendorSummary/subvendor", true));
  });
  //
  test("admin should be able to update", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "subvendorSummary/subvendor"));
  });
  test("owner should be able to update", async () => {
    let db = await setupOwner();
    expect(await updateSuccess(db, "subvendorSummary/subvendor"));
  });
  test("receptionist should be able to update", async () => {
    let db = await setupReceptionist();
    expect(await updateSuccess(db, "subvendorSummary/subvendor"));
  });
  test("inspector should not be able to update", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "subvendorSummary/subvendor"));
  });
  test("worker should not be able to update", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "subvendorSummary/subvendor"));
  });
  test("customer should not be able to update", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "subvendorSummary/subvendor"));
  });
  test("subvendor should not be able to update", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "subvendorSummary/subvendor"));
  });
  test("unauthenticated should not be able to  update", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "subvendorSummary/subvendor"));
  });

  test("admin should not be able to delete", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("owner should not be able to delete", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("receptionist not should be able to delete", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("inspector should not be able to delete", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("worker should not be able to delete", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("customer should not be able to delete", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("subvendor should not be able to delete", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });
  test("unauthenticated should not be able to  delete", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "subvendorSummary/subvendor"));
  });

  afterAll(async () => {
    await teardown();
  });
});
