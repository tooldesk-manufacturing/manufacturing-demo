const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupInspectorWithProjectID
} = require("../helpers/login-helper");
const {
  readSuccess,
  readFailure,
  deleteFailure,
  createDocumentSuccess,
  createDocumentFailure,
  updateSuccess,
  updateFailure
} = require("../helpers/db-helpers");

describe("FCM Tokens rules", () => {
  test("user should be able to read his token", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "fcmTokens/admin", true));
  });

  test("user should not be able to read somebody else's token", async () => {
    let db = await setupOwner();
    expect(await readFailure(db, "fcmTokens/admin", true));
  });

  test("user should not be able to delete fcmTokens", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "fcmTokens/admin"));
  });

  test("user should be able to create fcmToken for himself", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupInspectorWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "fcmTokens/inspector", projectId));
  });

  test("user should not be able to create fcmToken for other user", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupInspectorWithProjectID(projectId);
    expect(await createDocumentFailure(db, "fcmTokens/customer", projectId));
  });

  test("user should be able to update fcmToken for himself", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "fcmTokens/admin"));
  });

  test("user should not be able to update fcmToken for others", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "fcmTokens/admin"));
  });

  afterAll(async () => {
    await teardown();
  });
});
