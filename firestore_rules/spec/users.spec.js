const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupCustomer,
  setupReceptionist,
  setupSubvendor,
  setupUnauthenticatedUser,
  setupWorker
} = require("../helpers/login-helper");
const {
  readSuccess,
  readFailure,
  deleteFailure,
  createFailure,
  updateSuccess,
  updateFailure
} = require("../helpers/db-helpers");

describe("Order rules", () => {
  test("admin should be able to read", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "users/admin", true));
  });

  test("owner should be able to read", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "users/owner", true));
  });

  test("receptionist should be able to read", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "users/receptionist", true));
  });

  test("user should be able to read his document", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "users/inspector", true));
  });

  test("user should not be able to read others document", async () => {
    let db = await setupInspector();
    expect(await readFailure(db, "users/customer", true));
  });

  test("unauthenticated should not be able to read", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "users/admin", true));
  });

  test("admin should not be able to createDocument", async () => {
    let db = await setupAdmin();
    expect(await createFailure(db, "users"));
  });
  test("owner should not be able to createDocument", async () => {
    let db = await setupOwner();
    expect(await createFailure(db, "users"));
  });
  test("receptionist should not be able to createDocument", async () => {
    let db = await setupReceptionist();
    expect(await createFailure(db, "users"));
  });
  test("inspector should not be able to createDocument", async () => {
    let db = await setupInspector();
    expect(await createFailure(db, "users"));
  });
  test("worker should not be able to createDocument", async () => {
    let db = await setupWorker();
    expect(await createFailure(db, "users"));
  });
  test("customer should not be able to createDocument", async () => {
    let db = await setupCustomer();
    expect(await createFailure(db, "users"));
  });
  test("subvendor should not be able to createDocument", async () => {
    let db = await setupSubvendor();
    expect(await createFailure(db, "users"));
  });
  test("unauthenticated should not be able to  createDocument", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await createFailure(db, "users"));
  });

  test("admin should be able to update", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "users/admin"));
  });
  test("owner should be able to update", async () => {
    let db = await setupOwner();
    expect(await updateSuccess(db, "users/owner"));
  });
  test("receptionist should be able to update", async () => {
    let db = await setupReceptionist();
    expect(await updateSuccess(db, "users/receptionist"));
  });
  test("inspector should not be able to update", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "users/inspector"));
  });
  test("worker should not be able to update", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "users/worker"));
  });
  test("customer not should be able to update", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "users/customer"));
  });
  test("subvendor should not be able to update", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "users/subvendor"));
  });
  test("unauthenticated should not be able to  update", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "users/admin"));
  });

  test("admin should not be able to delete", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "users/admin"));
  });
  test("owner should not be able to delete", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "users/owner"));
  });
  test("receptionist not should be able to delete", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "users/receptionist"));
  });
  test("inspector should not be able to delete", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "users/inspector"));
  });
  test("worker should not be able to delete", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "users/worker"));
  });
  test("customer should not be able to delete", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "users/customer"));
  });
  test("subvendor should not be able to delete", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "users/subvendor"));
  });
  test("unauthenticated should not be able to  delete", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "users/admin"));
  });
  afterAll(async () => {
    await teardown();
  });
});
