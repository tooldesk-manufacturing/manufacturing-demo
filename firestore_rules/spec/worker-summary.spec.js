const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupCustomer,
  setupReceptionist,
  setupSubvendor,
  setupUnauthenticatedUser,
  setupWorker
} = require("../helpers/login-helper");

const {
  readSuccess,
  readFailure,
  updateFailure,
  deleteFailure
} = require("../helpers/db-helpers");

describe("Worker summary rules", () => {
  test("admin should be able to read", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "workerSummary/worker", true));
  });
  test("owner should be able to read", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "workerSummary/worker", true));
  });
  test("receptionist should be able to read", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "workerSummary/worker", true));
  });
  test("inspector should  be able to read", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "workerSummary/worker", true));
  });
  test("worker should not be able to read", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "workerSummary/worker", true));
  });
  test("customer should not be able to read", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "workerSummary/worker", true));
  });
  test("subvendor should not be able to read", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "workerSummary/worker", true));
  });
  test("unauthenticated should not be able to read", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "workerSummary/worker", true));
  });
  //
  test("admin should not be able to update", async () => {
    let db = await setupAdmin();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("owner should not be able to update", async () => {
    let db = await setupOwner();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("receptionist not should be able to update", async () => {
    let db = await setupReceptionist();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("inspector should not be able to update", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("worker should not be able to update", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("customer should not be able to update", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("subvendor should not be able to update", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "workerSummary/worker"));
  });
  test("unauthenticated should not be able to  update", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "workerSummary/worker"));
  });

  test("admin should not be able to delete", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("owner should not be able to delete", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("receptionist not should be able to delete", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("inspector should not be able to delete", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("worker should not be able to delete", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("customer should not be able to delete", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("subvendor should not be able to delete", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });
  test("unauthenticated should not be able to  delete", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "workerSummary/worker"));
  });

  afterAll(async () => {
    await teardown();
  });
});
