const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupCustomer,
  setupReceptionist,
  setupSubvendor,
  setupUnauthenticatedUser,
  setupWorker
} = require("../helpers/login-helper");

const {
  readSuccess,
  readFailure,
  deleteFailure,
  updateFailure,
  updateSuccess
} = require("../helpers/db-helpers");

describe("Customer summary rules", () => {
  test("admin should be able to read", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "customerSummary/customer", true));
  });
  test("owner should be able to read", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "customerSummary/customer", true));
  });
  test("receptionist should be able to read", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "customerSummary/customer", true));
  });
  test("inspector should  be able to read", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "customerSummary/customer", true));
  });
  test("worker should not be able to read", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "customerSummary/customer", true));
  });
  test("customer should not be able to read", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "customerSummary/customer", true));
  });
  test("subvendor should not be able to read", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "customerSummary/customer", true));
  });
  test("unauthenticated should not be able to read", async () => {
    let db = await setupUnauthenticatedUser();

    expect(await readFailure(db, "customerSummary/customer", true));
  });
  //
  test("admin should be able to update", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "customerSummary/customer"));
  });
  test("owner should be able to update", async () => {
    let db = await setupOwner();
    expect(await updateSuccess(db, "customerSummary/customer"));
  });
  test("receptionist should be able to update", async () => {
    let db = await setupReceptionist();
    expect(await updateSuccess(db, "customerSummary/customer"));
  });
  test("inspector should not be able to update", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "customerSummary/customer"));
  });
  test("worker should not be able to update", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "customerSummary/customer"));
  });
  test("customer should not be able to update", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "customerSummary/customer"));
  });
  test("subvendor should not be able to update", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "customerSummary/customer"));
  });
  test("unauthenticated should not be able to  update", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "customerSummary/customer"));
  });

  test("admin should not be able to delete", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("owner should not be able to delete", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("receptionist not should be able to delete", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("inspector should not be able to delete", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("worker should not be able to delete", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("customer should not be able to delete", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("subvendor should not be able to delete", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });
  test("unauthenticated should not be able to  delete", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "customerSummary/customer"));
  });

  afterAll(async () => {
    await teardown();
  });
});
