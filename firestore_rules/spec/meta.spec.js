const {
  setupAdmin,
  setupOwner,
  setupInspector,
  setupInspectorWithProjectID,
  setupAdminWithProjectID,
  setupCustomer,
  setupCustomerWithProjectID,
  setupOwnerWithProjectID,
  setupReceptionist,
  setupReceptionistWithProjectID,
  setupSubvendor,
  setupSubvendorWithProjectID,
  setupUnauthenticatedUser,
  setupUnauthenticatedUserWithProjectID,
  setupWorker,
  setupWorkerWithProjectID
} = require("../helpers/login-helper");

const {
  readFailure,
  readSuccess,
  deleteFailure,
  createDocumentFailure,
  createDocumentSuccess,
  updateSuccess,
  updateFailure
} = require("../helpers/db-helpers");

describe("Meta rules", () => {
  test("admin should be able to read billing details", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "meta/billingDetails", true));
  });
  test("owner should be able to read billing details", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "meta/billingDetails", true));
  });
  test("receptionist should be able to read billing details", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "meta/billingDetails", true));
  });
  test("inspector should not be able to read billing details", async () => {
    let db = await setupInspector();
    expect(await readFailure(db, "meta/billingDetails", true));
  });
  test("worker should not be able to read billing details", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "meta/billingDetails", true));
  });
  test("customer should not be able to read billing details", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "meta/billingDetails", true));
  });
  test("subvendor should not be able to read billing details", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "meta/billingDetails", true));
  });
  test("unauthenticated should not be able to read billing details", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "meta/billingDetails", true));
  });
  test("admin should  be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupAdminWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "meta/billingDetails", projectId));
  });
  test("owner should  be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupOwnerWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "meta/billingDetails", projectId));
  });
  test("receptionist should  be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupReceptionistWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "meta/billingDetails", projectId));
  });
  test("inspector should not be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupInspectorWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/billingDetails", projectId));
  });
  test("worker should not be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupWorkerWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/billingDetails", projectId));
  });
  test("customer should not be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupCustomerWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/billingDetails", projectId));
  });
  test("subvendor should not be able to createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupSubvendorWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/billingDetails", projectId));
  });
  test("unauthenticated should not be able to  createDocument billing details", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupUnauthenticatedUserWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/billingDetails", projectId));
  });

  test("admin should be able to update billing details", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "meta/billingDetails"));
  });

  test("owner should be able to update billing details", async () => {
    let db = await setupOwner();
    expect(await updateSuccess(db, "meta/billingDetails"));
  });
  test("receptionist should be able to update billing details", async () => {
    let db = await setupReceptionist();
    expect(await updateSuccess(db, "meta/billingDetails"));
  });
  test("inspector should not be able to update billing details", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "meta/billingDetails"));
  });
  test("worker should not be able to update billing details", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "meta/billingDetails"));
  });
  test("customer should not be able to update billing details", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "meta/billingDetails"));
  });
  test("subvendor should not be able to update billing details", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "meta/billingDetails"));
  });
  test("unauthenticated should not be able to  update billing details", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "meta/billingDetails"));
  });

  test("admin should be able to delete billing details", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("owner should be able to delete billing details", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("receptionist should be able to delete billing details", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("inspector should not be able to delete billing details", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("worker should not be able to delete billing details", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("customer should not be able to delete billing details", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("subvendor should not be able to delete billing details", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });
  test("unauthenticated should not be able to  delete billing details", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "meta/billingDetails"));
  });

  //

  test("admin should be able to read departments", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "meta/departments", true));
  });
  test("owner should be able to read departments", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "meta/departments", true));
  });
  test("receptionist should be able to read departments", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "meta/departments", true));
  });
  test("inspector should not be able to read departments", async () => {
    let db = await setupInspector();
    expect(await readFailure(db, "meta/departments", true));
  });
  test("worker should not be able to read departments", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "meta/departments", true));
  });
  test("customer should not be able to read departments", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "meta/departments", true));
  });
  test("subvendor should not be able to read departments", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "meta/departments", true));
  });
  test("unauthenticated should not be able to read departments", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "meta/departments", true));
  });
  test("admin should  be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupAdminWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "meta/departments", projectId));
  });
  test("owner should  be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupOwnerWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "meta/departments", projectId));
  });
  test("receptionist should  be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupReceptionistWithProjectID(projectId);
    expect(await createDocumentSuccess(db, "meta/departments", projectId));
  });
  test("inspector should not be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupInspectorWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/departments", projectId));
  });
  test("worker should not be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupWorkerWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/departments", projectId));
  });
  test("customer should not be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupCustomerWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/departments", projectId));
  });
  test("subvendor should not be able to createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupSubvendorWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/departments", projectId));
  });
  test("unauthenticated should not be able to  createDocument departments", async () => {
    const projectId = `rules-spec-${Date.now()}`;
    let db = await setupUnauthenticatedUserWithProjectID(projectId);
    expect(await createDocumentFailure(db, "meta/departments", projectId));
  });

  test("admin should be able to update departments", async () => {
    let db = await setupAdmin();
    expect(await updateSuccess(db, "meta/departments"));
  });
  test("owner should be able to update departments", async () => {
    let db = await setupOwner();
    expect(await updateSuccess(db, "meta/departments"));
  });
  test("receptionist should be able to update departments", async () => {
    let db = await setupReceptionist();
    expect(await updateSuccess(db, "meta/departments"));
  });
  test("inspector should not be able to update departments", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "meta/departments"));
  });
  test("worker should not be able to update departments", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "meta/departments"));
  });
  test("customer should not be able to update departments", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "meta/departments"));
  });
  test("subvendor should not be able to update departments", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "meta/departments"));
  });
  test("unauthenticated should not be able to  update departments", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "meta/departments"));
  });

  test("admin should be able to delete departments", async () => {
    let db = await setupAdmin();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("owner should be able to delete departments", async () => {
    let db = await setupOwner();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("receptionist should be able to delete departments", async () => {
    let db = await setupReceptionist();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("inspector should not be able to delete departments", async () => {
    let db = await setupInspector();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("worker should not be able to delete departments", async () => {
    let db = await setupWorker();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("customer should not be able to delete departments", async () => {
    let db = await setupCustomer();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("subvendor should not be able to delete departments", async () => {
    let db = await setupSubvendor();
    expect(await deleteFailure(db, "meta/departments"));
  });
  test("unauthenticated should not be able to  delete departments", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await deleteFailure(db, "meta/departments"));
  });

  //

  test("admin should be able to read dueDate", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("owner should be able to read dueDate", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("receptionist should be able to read dueDate", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("inspector should be able to read dueDate", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("worker should be able to read dueDate", async () => {
    let db = await setupWorker();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("customer should be able to read dueDate", async () => {
    let db = await setupCustomer();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("subvendor should be able to read dueDate", async () => {
    let db = await setupSubvendor();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("unauthenticated should be able to read dueDate", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readSuccess(db, "meta/dueDate", true));
  });
  test("admin should be able to update dueDate", async () => {
    let db = await setupAdmin();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("owner should be able to update dueDate", async () => {
    let db = await setupOwner();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("receptionist should be able to update dueDate", async () => {
    let db = await setupReceptionist();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("inspector should not be able to update dueDate", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("worker should not be able to update dueDate", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("customer should not be able to update dueDate", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("subvendor should not be able to update dueDate", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "meta/dueDate"));
  });
  test("unauthenticated should not be able to  update dueDate", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "meta/dueDate"));
  });

  //

  test("admin should be able to read modules", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("owner should be able to read modules", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("receptionist should be able to read modules", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("inspector should be able to read modules", async () => {
    let db = await setupInspector();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("worker should be able to read modules", async () => {
    let db = await setupWorker();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("customer should be able to read modules", async () => {
    let db = await setupCustomer();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("subvendor should be able to read modules", async () => {
    let db = await setupSubvendor();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("unauthenticated should be able to read modules", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readSuccess(db, "meta/modules", true));
  });
  test("admin should be able to update modules", async () => {
    let db = await setupAdmin();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("owner should be able to update modules", async () => {
    let db = await setupOwner();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("receptionist should be able to update modules", async () => {
    let db = await setupReceptionist();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("inspector should not be able to update modules", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("worker should not be able to update modules", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("customer should not be able to update modules", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("subvendor should not be able to update modules", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "meta/modules"));
  });
  test("unauthenticated should not be able to  update modules", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "meta/modules"));
  });

  //

  test("admin should be able to read usersLimit", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "meta/usersLimit", true));
  });
  test("owner should be able to read usersLimit", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "meta/usersLimit", true));
  });
  test("receptionist should be able to read usersLimit", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "meta/usersLimit", true));
  });
  test("inspector should not be able to read usersLimit", async () => {
    let db = await setupInspector();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("worker should not be able to read usersLimit", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("customer should not be able to read usersLimit", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("subvendor should not be able to read usersLimit", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("unauthenticated should be able to read usersLimit", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("admin should be able to update usersLimit", async () => {
    let db = await setupAdmin();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("owner should be able to update usersLimit", async () => {
    let db = await setupOwner();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("receptionist should be able to update usersLimit", async () => {
    let db = await setupReceptionist();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("inspector should not be able to update usersLimit", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("worker should not be able to update usersLimit", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("customer should not be able to update usersLimit", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("subvendor should not be able to update usersLimit", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("unauthenticated should not be able to  update usersLimit", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "meta/usersLimit"));
  });

  //
  test("admin should be able to read usersLimit", async () => {
    let db = await setupAdmin();
    expect(await readSuccess(db, "meta/usersLimit", true));
  });
  test("owner should be able to read usersLimit", async () => {
    let db = await setupOwner();
    expect(await readSuccess(db, "meta/usersLimit", true));
  });
  test("receptionist should be able to read usersLimit", async () => {
    let db = await setupReceptionist();
    expect(await readSuccess(db, "meta/usersLimit", true));
  });
  test("inspector should be able to read usersLimit", async () => {
    let db = await setupInspector();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("worker should not not be able to read usersLimit", async () => {
    let db = await setupWorker();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("customer should not not be able to read usersLimit", async () => {
    let db = await setupCustomer();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("subvendor should not not be able to read usersLimit", async () => {
    let db = await setupSubvendor();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("unauthenticated should not be able to read usersLimit", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await readFailure(db, "meta/usersLimit", true));
  });
  test("admin should not be able to update usersLimit", async () => {
    let db = await setupAdmin();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("owner should not be able to update usersLimit", async () => {
    let db = await setupOwner();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("receptionist should not be able to update usersLimit", async () => {
    let db = await setupReceptionist();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("inspector should not not be able to update usersLimit", async () => {
    let db = await setupInspector();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("worker should not not be able to update usersLimit", async () => {
    let db = await setupWorker();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("customer should not not be able to update usersLimit", async () => {
    let db = await setupCustomer();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("subvendor should not not be able to update usersLimit", async () => {
    let db = await setupSubvendor();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  test("unauthenticated should not not be able to  update usersLimit", async () => {
    let db = await setupUnauthenticatedUser();
    expect(await updateFailure(db, "meta/usersLimit"));
  });
  afterAll(async () => {
    await teardown();
  });
});
