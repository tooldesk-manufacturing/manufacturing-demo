# ToolDesk ProSuite

## Getting started

- Install Node v10.x
- Install Yarn
- Run `yarn` in the repository's root directory

## Development Server

- Run `yarn run start` in the root directory

## Testing

### Angular

- Run `yarn run test` for testing with Jest
- Run `yarn run e2e` for testing with Cypress

### Firestore Rules

- Run `yarn run setup-emulator` in the `firestore_rules` directory(Requires Java)
- Run `yarn run ci`
