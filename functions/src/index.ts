import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

import * as userUtil from "./user-util";
import * as notifications from "./notification";
import * as utils from "./utils";
import * as cors from "cors";
import { User } from "./models";

admin.initializeApp(functions.config().firebase);
admin.firestore().settings({ timestampsInSnapshots: true });

const corsHandler = cors({ origin: true });

exports.createUser = functions.https.onCall(
  async (data: User & { password: string; userType: string }, context) => {
    try {
      const userType = data.userType;
      delete data.userType;
      await utils.checkRegisterAuthorization(data, context);
      console.log("authorized.");
      await utils.checkEmailExists(data.email);
      console.log("email doesn't exist.");
      const ref = await userUtil.createUserData(data);
      console.log("user data created.");
      await userUtil.registerUser(ref.id, data);
      console.log("user registered.");
      await admin.auth().setCustomUserClaims(ref.id, data.roles);
      console.log("user custom claims set.");
      await userUtil.deletePassword(ref.id);
      console.log("password deleted.");
      const result = await userUtil.returnSummary(data, ref);
      if (result) {
        console.log("summary obtained.");
        await userUtil.createUserSummary(
          result.collection,
          ref.id,
          result.summary
        );
        console.log("summary created.");
      }
      await userUtil.updateUserLimit(userType);
      console.log("user limit updated");
      return { message: "User created successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.creditCheck = functions.https.onRequest((req, res) => {
  res.statusMessage = "Credit check called.";
  res.status(200).end();
});

exports.markPresent = functions.https.onCall(
  async (data: string[], context) => {
    try {
      await utils.checkAttendanceAuthorization(context);
      console.log("authorized.");
      await userUtil.markPresent(data);
      console.log("attendance marked.");
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.deleteUser = functions.https.onCall(
  async (data: { pin: string; uid: string; roleType: string }, context) => {
    try {
      await utils.checkUpdatePinAuthorization(context);
      console.log("authorized.");
      await userUtil.checkPin(data.pin);
      console.log("pin matches.");
      await userUtil.deleteUser(data.uid, data.roleType);
      console.log("user deleted.");
      await userUtil.updateUserLimit(data.roleType, false);
      console.log("user limit updated");
      return { message: "User deleted successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.createAdmin = functions.https.onRequest(async (req, res) => {
  // tslint:disable-next-line:no-empty
  corsHandler(req, res, () => {});
  if (req.body && req.body.pin === "789456") {
    try {
      const data = req.body.userData;
      await utils.checkEmailExists(data.email);
      console.log("email doesn't exist.");
      const ref = await userUtil.createUserData(data);
      console.log("user data created.");
      await userUtil.registerUser(ref.id, data);
      console.log("user registered.");
      await userUtil.deletePassword(ref.id);
      console.log("password deleted.");
      await admin.auth().setCustomUserClaims(ref.id, { admin: true });
      console.log("custom claims set");
      res.statusMessage = "Has created admin";
      res.status(200).end();
    } catch (reason) {
      console.error(reason);
      res.statusMessage = reason;
      res.status(401).end();
    }
  } else {
    console.log("no data sent");
    res.status(417).end();
  }
});

exports.deleteOrder = functions.https.onCall(
  async (data: { pin: string; orderUID: string }, context) => {
    try {
      await utils.checkUpdatePinAuthorization(context);
      console.log("authorized.");
      await userUtil.checkPin(data.pin);
      console.log("pin matches.");
      await userUtil.deleteOrder(data.orderUID);
      console.log("order deleted.");
      return { message: "Order deleted successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.deleteTransaction = functions.https.onCall(
  async (data: { pin: string; transactionUID: string }, context) => {
    try {
      await utils.checkUpdatePinAuthorization(context);
      console.log("authorized.");
      await userUtil.checkPin(data.pin);
      console.log("pin matches.");
      await userUtil.deleteTransaction(data.transactionUID);
      console.log("transaction deleted.");
      return { message: "Transaction deleted successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);
exports.deleteBill = functions.https.onCall(
  async (data: { pin: string; billUID: string; billNo: string }, context) => {
    try {
      await utils.checkUpdatePinAuthorization(context);
      console.log("authorized.");
      await userUtil.checkPin(data.pin);
      console.log("pin matches.");
      await userUtil.deleteBill(data.billUID, data.billNo);
      console.log("bill deleted.");
      return { message: "Bill deleted successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.deleteReceipt = functions.https.onCall(
  async (data: { pin: string; receiptUID: string }, context) => {
    try {
      await utils.checkUpdatePinAuthorization(context);
      console.log("authorized.");
      await userUtil.checkPin(data.pin);
      console.log("pin matches.");
      await userUtil.deleteReceipt(data.receiptUID);
      console.log("receipt deleted.");
      return { message: "Receipt deleted successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.packages = functions.https.onRequest(async (req, res) => {
  // tslint:disable-next-line:no-empty
  corsHandler(req, res, () => {});
  if (req.body) {
    try {
      await admin
        .firestore()
        .collection("meta")
        .doc("modules")
        .set(req.body);
      console.log("packages set.");
      res.statusMessage = "Has set packages.";
      res.status(200).end();
    } catch (reason) {
      console.error(reason);
      res.statusMessage = reason;
      res.status(401).end();
    }
  } else {
    console.log("no data sent");
    res.status(417).end();
  }
});

exports.attendanceSummary = functions.https.onRequest(async (req, res) => {
  // tslint:disable-next-line:no-empty
  corsHandler(req, res, () => {});
  try {
    const snap = await admin
      .firestore()
      .collection("workerSummary")
      .get();
    console.log("got summary");
    await Promise.all(
      snap.docs.map(async doc => {
        try {
          await doc.ref.set({ presentThisMonth: 0 }, { merge: true });
        } catch (reason) {
          console.error(reason);
          res.statusMessage = reason;
          res.status(500).end();
        }
      })
    );
    console.log("set all summary docs");
    res.statusMessage = "Attendance reset to 0.";
    res.status(200).end();
  } catch (reason) {
    console.error(reason);
    res.statusMessage = reason;
    res.status(500).end();
  }
});

exports.dueDate = functions.https.onRequest(async (req, res) => {
  // tslint:disable-next-line:no-empty
  corsHandler(req, res, () => {});
  if (req.body) {
    try {
      await admin
        .firestore()
        .collection("meta")
        .doc("dueDate")
        .set(req.body);
      console.log("due date set.");
      res.statusMessage = "Due Date set successfully.";
      res.status(200).end();
    } catch (reason) {
      console.error(reason);
      res.statusMessage = reason;
      res.status(417).end();
    }
  } else {
    console.log("no data sent.");
    res.status(417).end();
  }
});

exports.markOutTime = functions.https.onCall(
  async (data: string[], context) => {
    try {
      await utils.checkAttendanceAuthorization(context);
      console.log("authorized.");
      await userUtil.markOutTime(data);
      console.log("out time marked");
      return { message: "Out time marked successfully." };
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.updatePin = functions.https.onCall(
  async (data: { currentPIN: string; newPIN: string }, context) => {
    try {
      await utils.checkUpdatePinAuthorization(context);
      console.log("authorized.");
      await userUtil.checkPin(data.currentPIN);
      console.log("pin matches.");
      await userUtil.updatePin(data);
      console.log("pin updated.");
    } catch (reason) {
      console.error(reason);
      throw new functions.https.HttpsError("internal", reason);
    }
  }
);

exports.sendNotification = functions.https.onRequest(async (req, res) => {
  // tslint:disable-next-line:no-empty
  corsHandler(req, res, () => {});
  try {
    console.log(req);
    await notifications.sendNotification(req);
    console.log("notification sent");
    res.statusMessage = "Sent notification successfully";
    res.status(200).end();
  } catch (reason) {
    console.error(reason);
    res.statusMessage = reason;
    res.status(500).end();
  }
});
