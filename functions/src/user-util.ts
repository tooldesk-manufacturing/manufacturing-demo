import * as admin from "firebase-admin";
import {
  WorkerSummary,
  CustomerSummary,
  SubvendorSummary,
  User,
  UsersLimit,
  IndividualUserLimit,
  Order,
  Transaction
} from "./models";
import * as url from "url";
import * as path from "path";

export function registerUser(
  uid: string,
  data: User & { password: string }
): Promise<string> {
  return new Promise(async (resolve, reject) => {
    const userData = {
      uid: uid,
      email: data.email,
      password: data.password,
      displayName: data.displayName
    };
    try {
      await admin.auth().createUser(userData);
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function deletePassword(uid: string) {
  return new Promise(async (resolve, reject) => {
    try {
      const snap = await admin
        .firestore()
        .collection("users")
        .doc(uid)
        .get();
      const data = snap.data() as User & { password: string };
      delete data.password;
      data.uid = uid;
      await admin
        .firestore()
        .collection("users")
        .doc(uid)
        .set(data);
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function updateUserLimit(role: string, increment?: boolean) {
  return new Promise(async (resolve, reject) => {
    try {
      const snap = await admin
        .firestore()
        .collection("meta")
        .doc("usersLimit")
        .get();
      const usersLimit = snap.data() as UsersLimit;
      console.log(usersLimit);
      console.log(role);
      console.log(usersLimit[role]);
      const updatedUserLimit: IndividualUserLimit = usersLimit[role];
      if (increment !== undefined && increment === false) {
        if (updatedUserLimit.current > 0) {
          updatedUserLimit.current -= 1;
        }
      } else {
        updatedUserLimit.current += 1;
      }
      await admin
        .firestore()
        .collection("meta")
        .doc("usersLimit")
        .set({ [role]: updatedUserLimit }, { merge: true });
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function createUserData(
  data: User & { password: string }
): Promise<FirebaseFirestore.DocumentReference> {
  return new Promise(async (resolve, reject) => {
    try {
      const docRef = await admin
        .firestore()
        .collection("users")
        .add(data);
      resolve(docRef);
    } catch (reason) {
      reject(reason);
    }
  });
}

export function createUserSummary(
  collection: string,
  uid: string,
  summary: WorkerSummary | CustomerSummary | SubvendorSummary
): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      await admin
        .firestore()
        .collection(collection)
        .doc(uid)
        .set(summary, { merge: true });
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function returnSummary(
  data: User & { password: string },
  ref: FirebaseFirestore.DocumentReference
): Promise<{
  summary: SubvendorSummary | CustomerSummary | WorkerSummary;
  collection: string;
}> {
  return new Promise((resolve, reject) => {
    if (data.roles.customer && data.roles.customer === true) {
      resolve({
        summary: {
          name: data.displayName,
          creditAmount: 0,
          uid: ref.id
        },
        collection: "customerSummary"
      });
    } else if (data.roles.worker && data.roles.worker === true) {
      if (data.roles.receptionist && data.roles.receptionist === true) {
        resolve({
          summary: {
            name: data.displayName,
            presentThisMonth: 0,
            uid: ref.id,
            type: "receptionist"
          },
          collection: "workerSummary"
        });
      } else if (data.roles.inspector && data.roles.inspector === true) {
        resolve({
          summary: {
            name: data.displayName,
            presentThisMonth: 0,
            uid: ref.id,
            type: "inspector"
          },
          collection: "workerSummary"
        });
      } else {
        resolve({
          summary: {
            name: data.displayName,
            presentThisMonth: 0,
            uid: ref.id
          },
          collection: "workerSummary"
        });
      }
    } else if (data.roles.subvendor && data.roles.subvendor === true) {
      resolve({
        summary: {
          name: data.displayName,
          amountDue: 0,
          uid: ref.id
        },
        collection: "subvendorSummary"
      });
    } else {
      resolve(null);
    }
  });
}

export function deleteUser(uid: string, roleType: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const userDocRef = admin
        .firestore()
        .collection("users")
        .doc(uid);
      const userData = await userDocRef.get();
      const batch = admin.firestore().batch();
      if (
        roleType === "worker" ||
        roleType === "receptionist" ||
        roleType === "inspector"
      ) {
        const summaryDocRef = admin
          .firestore()
          .collection("workerSummary")
          .doc(uid);
        batch.delete(summaryDocRef);
        const userAttendanceRef = admin
          .firestore()
          .collection("users")
          .doc(uid)
          .collection("attendance");
        const snap = await userAttendanceRef.get();
        snap.forEach(doc => {
          batch.delete(doc.ref);
        });
      } else if (roleType === "subvendor") {
        const summaryDocRef = admin
          .firestore()
          .collection("subvendorSummary")
          .doc(uid);
        batch.delete(summaryDocRef);
      } else if (roleType === "customer") {
        const summaryDocRef = admin
          .firestore()
          .collection("customerSummary")
          .doc(uid);
        batch.delete(summaryDocRef);
      }
      if ((userData.data() as User).photoURL) {
        console.log((userData.data() as User).photoURL);
        const imageName = url.parse((userData.data() as User).photoURL);
        const imagePath = path.basename(imageName.pathname);
        const bucket = admin.storage().bucket();
        const decodedImagePath = decodeURIComponent(imagePath);
        console.log(decodedImagePath);
        const profileImage = bucket.file(decodedImagePath);
        await profileImage.delete();
        console.log("image deleted");
      }
      batch.delete(userDocRef);
      await batch.commit();
      await admin.auth().deleteUser(uid);
      console.log("auth user deleted");
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function markPresent(data: string[]): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const weekday = new Array(
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      );
      const today = new Date();
      const monthYear =
        today.getFullYear().toString() +
        "-" +
        (today.getMonth() + 1).toString();
      const batch = admin.firestore().batch();
      for (const uid of data) {
        const attendanceRef = admin
          .firestore()
          .collection("users")
          .doc(uid)
          .collection("attendance")
          .doc(monthYear);
        batch.set(
          attendanceRef,
          {
            [today.getDate().toString() + " - " + weekday[today.getDay()]]: ""
          },
          { merge: true }
        );
      }
      await batch.commit();
      const batch2 = admin.firestore().batch();
      for (const uid of data) {
        const attendanceRef = admin
          .firestore()
          .collection("users")
          .doc(uid)
          .collection("attendance")
          .doc(monthYear);
        const summaryRef = admin
          .firestore()
          .collection("workerSummary")
          .doc(uid);
        const attendanceData = await attendanceRef.get();
        let count = 0;
        for (const _key of Object.keys(attendanceData.data())) {
          count++;
        }
        batch2.set(summaryRef, { presentThisMonth: count }, { merge: true });
      }
      await batch2.commit();
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function markOutTime(data: string[]): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const weekday = new Array(
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
      );
      const today = new Date();
      const nDate = new Date().toLocaleString("en-US", {
        timeZone: "Asia/Kolkata"
      });
      const nDate2 = new Date(nDate);
      const hourMinute =
        nDate2.getHours().toString() + ":" + nDate2.getMinutes().toString();
      const monthYear =
        today.getFullYear().toString() +
        "-" +
        (today.getMonth() + 1).toString();
      const batch = admin.firestore().batch();
      for (const uid of data) {
        const attendanceRef = admin
          .firestore()
          .collection("users")
          .doc(uid)
          .collection("attendance")
          .doc(monthYear);
        batch.set(
          attendanceRef,
          {
            [today.getDate().toString() +
            " - " +
            weekday[today.getDay()]]: hourMinute
          },
          { merge: true }
        );
      }
      await batch.commit();
      const batch2 = admin.firestore().batch();
      for (const uid of data) {
        const attendanceRef = admin
          .firestore()
          .collection("users")
          .doc(uid)
          .collection("attendance")
          .doc(monthYear);
        const summaryRef = admin
          .firestore()
          .collection("workerSummary")
          .doc(uid);
        const attendanceData = await attendanceRef.get();
        let count = 0;
        for (const _key of Object.keys(attendanceData.data())) {
          count++;
        }
        batch2.set(summaryRef, { presentThisMonth: count }, { merge: true });
      }
      await batch2.commit();
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function updatePin(data: {
  currentPIN: string;
  newPIN: string;
}): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      await admin
        .firestore()
        .collection("meta")
        .doc("owner")
        .set({ pin: data.newPIN });
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function deleteOrder(orderUID: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const batch = admin.firestore().batch();
      const orderRef = admin
        .firestore()
        .collection("orders")
        .doc(orderUID);
      const orderSnapshot = await orderRef.get();
      const orderData = orderSnapshot.data() as Order;
      const bucket = admin.storage().bucket();
      for (const item of orderData.items) {
        if (item.images) {
          for (const imageURL of item.images) {
            const imageName = url.parse(imageURL);
            const imagePath = path.basename(imageName.pathname);
            const decodedImagePath = decodeURIComponent(imagePath);
            console.log(decodedImagePath);
            const profileImage = bucket.file(decodedImagePath);
            await profileImage.delete();
            console.log("image deleted");
          }
        }
      }
      const receiptsCollectionRef = admin.firestore().collection("receipts");
      const receiptQuery = receiptsCollectionRef.where(
        "orderUID",
        "==",
        orderUID
      );
      const receipts = await receiptQuery.get();
      if (receipts.size > 0) {
        receipts.forEach(receipt => {
          batch.delete(receipt.ref);
        });
      }
      const billsCollectionRef = admin.firestore().collection("bills");
      const billQuery = billsCollectionRef.where(
        "orderUID",
        "array-contains",
        orderUID
      );
      const bills = await billQuery.get();
      if (bills.size > 0) {
        bills.forEach(bill => {
          batch.delete(bill.ref);
        });
      }
      const transactionCollectionRef = admin
        .firestore()
        .collection("transactions");
      const transactionQuery = transactionCollectionRef.where(
        "orderUID",
        "array-contains",
        orderUID
      );
      const transactions = await transactionQuery.get();
      if (transactions.size > 0) {
        for (const transaction of transactions.docs) {
          const transactionData = transaction.data() as Transaction;
          if (
            transactionData.customerUID &&
            !transactionData.cash &&
            !transactionData.paymentReceived
          ) {
            const sumRef = admin
              .firestore()
              .collection("customerSummary")
              .doc(transactionData.customerUID);
            const customerSummary = await admin
              .firestore()
              .collection("customerSummary")
              .doc(transactionData.customerUID)
              .get();
            batch.set(
              sumRef,
              {
                creditAmount:
                  (customerSummary.data() as CustomerSummary).creditAmount -
                  transactionData.amount
              },
              {
                merge: true
              }
            );
          }
          if (
            transactionData.subvendorUID &&
            !transactionData.cash &&
            !transactionData.paymentReceived
          ) {
            const sumRef = admin
              .firestore()
              .collection("subvendorSummary")
              .doc(transactionData.subvendorUID);
            const subvendorSummary = await admin
              .firestore()
              .collection("subvendorSummary")
              .doc(transactionData.subvendorUID)
              .get();
            batch.set(
              sumRef,
              {
                amountDue:
                  (subvendorSummary.data() as SubvendorSummary).amountDue -
                  transactionData.amount
              },
              {
                merge: true
              }
            );
          }
          batch.delete(transaction.ref);
        }
      }
      batch.delete(orderRef);
      await batch.commit();
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function deleteTransaction(transactionUID: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const transaction = await admin
        .firestore()
        .collection("transactions")
        .doc(transactionUID)
        .get();
      const transactionData = transaction.data() as Transaction;
      const batch = admin.firestore().batch();
      if (
        transactionData.customerUID &&
        !transactionData.cash &&
        !transactionData.paymentReceived
      ) {
        console.log("customer credit amount");
        const sumRef = admin
          .firestore()
          .collection("customerSummary")
          .doc(transactionData.customerUID);
        const customerSummary = await admin
          .firestore()
          .collection("customerSummary")
          .doc(transactionData.customerUID)
          .get();
        batch.set(
          sumRef,
          {
            creditAmount:
              (customerSummary.data() as CustomerSummary).creditAmount -
              transactionData.amount
          },
          { merge: true }
        );
      }
      if (
        transactionData.subvendorUID &&
        !transactionData.cash &&
        !transactionData.paymentReceived
      ) {
        console.log("subvendor amount due");
        const sumRef = admin
          .firestore()
          .collection("subvendorSummary")
          .doc(transactionData.subvendorUID);
        const subvendorSummary = await admin
          .firestore()
          .collection("subvendorSummary")
          .doc(transactionData.subvendorUID)
          .get();
        batch.set(
          sumRef,
          {
            amountDue:
              (subvendorSummary.data() as SubvendorSummary).amountDue -
              transactionData.amount
          },
          { merge: true }
        );
      }
      batch.delete(transaction.ref);
      await batch.commit();
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function deleteBill(billUID: string, billNo: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const batch = admin.firestore().batch();
      const billRef = admin
        .firestore()
        .collection("bills")
        .doc(billUID);
      batch.delete(billRef);
      const transactionCollectionRef = admin
        .firestore()
        .collection("transactions");
      const query = transactionCollectionRef.where("billNo", "==", billNo);
      const transactionRefs = await query.get();
      if (transactionRefs.size > 0) {
        for (const transaction of transactionRefs.docs) {
          const transactionData = transaction.data() as Transaction;
          if (
            transactionData.customerUID &&
            !transactionData.cash &&
            !transactionData.paymentReceived
          ) {
            const sumRef = admin
              .firestore()
              .collection("customerSummary")
              .doc(transactionData.customerUID);
            const customerSummary = await admin
              .firestore()
              .collection("customerSummary")
              .doc(transactionData.customerUID)
              .get();
            batch.set(
              sumRef,
              {
                creditAmount:
                  (customerSummary.data() as CustomerSummary).creditAmount -
                  transactionData.amount
              },
              { merge: true }
            );
          }
          if (
            transactionData.subvendorUID &&
            !transactionData.cash &&
            !transactionData.paymentReceived
          ) {
            const sumRef = admin
              .firestore()
              .collection("subvendorSummary")
              .doc(transactionData.subvendorUID);
            const subvendorSummary = await admin
              .firestore()
              .collection("subvendorSummary")
              .doc(transactionData.subvendorUID)
              .get();
            batch.set(
              sumRef,
              {
                amountDue:
                  (subvendorSummary.data() as SubvendorSummary).amountDue -
                  transactionData.amount
              },
              { merge: true }
            );
          }
          batch.delete(transaction.ref);
        }
      }
      await batch.commit();
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function deleteReceipt(receiptUID: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      await admin
        .firestore()
        .collection("receipts")
        .doc(receiptUID)
        .delete();
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export function checkPin(pin: string): Promise<string> {
  return new Promise(async (resolve, reject) => {
    try {
      const snap = await admin
        .firestore()
        .collection("meta")
        .doc("owner")
        .get();
      if (snap.data().pin === pin) {
        resolve();
      } else {
        reject("Wrong PIN");
      }
    } catch (reason) {
      reject(reason);
    }
  });
}

export class UserUtils {}
