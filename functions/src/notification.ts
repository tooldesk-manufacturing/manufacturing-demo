import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

export function sendNotification(req: functions.Request) {
  return new Promise(async function(resolve, reject) {
    const uid = req.body.uid;
    const title = req.body.title;
    const message = req.body.message;
    const actionURL = functions.config().client.url + req.body.actionurl;

    console.log(uid);
    console.log(title);
    console.log(message);
    console.log(actionURL);
    const payload = {
      notification: {
        title: title,
        body: message,
        icon: "https://m.tooldesk.app/assets/sample.jpg",
        click_action: actionURL
      }
    };
    try {
      const snap = await admin
        .firestore()
        .collection("fcmTokens")
        .doc(uid)
        .get();
      const token = snap.get("token");
      await admin.messaging().sendToDevice(token, payload);
      resolve();
    } catch (reason) {
      reject(reason);
    }
  });
}

export class notifications {}
