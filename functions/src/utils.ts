import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { User } from './models';

export function checkRegisterAuthorization(
  data: User & { password: string },
  context: functions.https.CallableContext
): Promise<string> {
  return new Promise(async function(resolve, reject) {
    if (!context.auth.uid) {
      reject('Not authorized.');
    } else if (data.roles.admin === true) {
      reject('Not allowed to create Admin Users.');
    } else if (data.roles.owner === true) {
      try {
        const roles = context.auth.token;
        if (roles.admin === true) {
          resolve();
        } else {
          reject('Not authorized to create user.');
        }
      } catch (reason) {
        reject(reason);
      }
    } else {
      resolve();
    }
  });
}

export function checkEmailExists(email: string): Promise<string> {
  return new Promise(async function(resolve, reject) {
    try {
      await admin.auth().getUserByEmail(email);
      reject('Email already exists');
    } catch (reason) {
      resolve();
    }
  });
}

export function checkAttendanceAuthorization(
  context: functions.https.CallableContext
): Promise<string> {
  return new Promise(async function(resolve, reject) {
    if (!context.auth.uid) {
      reject('Not authorized.');
    } else {
      try {
        const roles = context.auth.token;
        if (
          roles.admin === true ||
          roles.owner === true ||
          roles.receptionist === true
        ) {
          resolve();
        } else {
          reject('Not authorized.');
        }
      } catch (reason) {
        reject(reason);
      }
    }
  });
}
export function checkUpdatePinAuthorization(
  context: functions.https.CallableContext
): Promise<string> {
  return new Promise(async function(resolve, reject) {
    if (!context.auth.uid) {
      reject('Not authorized.');
    } else {
      try {
        const roles = context.auth.token;
        if (roles.owner === true || roles.admin === true) {
          resolve();
        } else {
          reject('Not authorized.');
        }
      } catch (reason) {
        reject(reason);
      }
    }
  });
}
export class utils {}
