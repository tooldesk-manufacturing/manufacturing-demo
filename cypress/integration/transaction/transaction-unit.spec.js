/// <reference types="cypress"/>

let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http:localhost:4200/";
var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
const todaysDate = Cypress.moment().format("D/M/YYYY");
var btnsum;
var output =
  (("" + day).length < 2 ? "" : "") +
  day +
  "/" +
  (("" + month).length < 2 ? "" : "") +
  month +
  "/" +
  d.getFullYear();

describe("Transaction unit spec Page", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transaction")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });

  it("Heading,search field,date field,search button icon,close button, add button", () => {
    cy.get(".search-card", { timeout: 4000 })
      .find(".mat-select-value")
      .should("be.visible");
    cy.get(".mat-input-element[formcontrolname= fromDate]", {
      timeout: 4000
    }).should("be.visible");
    cy.get(".right-margin", { timeout: 4000 })
      .find(".mat-icon")
      .should("be.visible");
    cy.get(".mat-button-wrapper", { timeout: 4000 }).should("be.visible");
  });

  it("table should have paginator", () => {
    cy.get(".mat-paginator-navigation-previous", { timeout: 4000 });
    cy.get(".mat-paginator-navigation-next", { timeout: 4000 });
  });

  it.skip("Total revenue should show (income- expenditure for that day)", () => {
    cy.get(".cdk-column-cash > strong", { timeout: 4000 }).then($btn2 => {
      let text2 = $btn2.text().slice(0, 1);
      console.log(text2);
      let text3 = +$btn2.text().slice(2, 4);
      console.log(text3);
      cy.get(".add-circle-button", { timeout: 4000 }).click();
      cy.get(".mat-select[formcontrolname = type]", { timeout: 4000 })
        .type("{enter}")
        .type("{downarrow}");
      cy.get("input[formcontrolname = amount]", { timeout: 4000 }).type("123");
      cy.get(".mat-stroked-button", { timeout: 4000 }).click();

      if (text2 == "-") {
        cy.get(".mat-table", { timeout: 4000 })
          .find("mat-footer-row")
          .find(":nth-child(4)")
          .should("contain", text3 + 123);
      } else {
        cy.get(".mat-table", { timeout: 4000 })
          .find("mat-footer-row")
          .find(":nth-child(4)")
          .should("contain", text3 - 123);
      }
    });
  });

  it("check every single search field validators", () => {
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 }).type(
      "{enter}"
    );
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 });
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 });
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 });
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 });
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 });
  });

  it("check search button disabled for invalid input", () => {
    cy.get("mat-select[formcontrolname= searchField]", { timeout: 4000 }).type(
      "{enter}"
    );
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type("abc");
    cy.get(".right-margin", { timeout: 5000 })
      .click({ force: true })
      .should("be.disabled");
  });

  it("clicking search button should launch loading bar", () => {
    cy.get(".right-margin", { timeout: 5000 }).click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });

  it("clicking close button should make searchfield payment received date and search term today's date", () => {
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(
      "8/5/2019"
    );
    cy.get(".card-no-shadow", { timeout: 5000 })
      .find(".mat-icon-button")
      .contains("close")
      .click();
    cy.get(".mat-select-value", { timeout: 5000 }).contains("Date");
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).should(
      "have.value",
      output
    );
  });
  it("clicking on add button should launch add transaction modal", () => {
    cy.get(".add-circle-button", { timeout: 4000 }).click({ force: true });
    cy.get(".add-transaction-card").should("be.visible", { timeout: 3000 });
    cy.get(".close-button").click({ force: true });
  });
  it("search button enabled for valid input", () => {
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 })
      .type(" ")
      .click({ force: true });
    cy.get(".right-margin", { timeout: 5000 }).should("not.be.disabled");
  });

  after(() => {
    cy.logout();
  });
});
