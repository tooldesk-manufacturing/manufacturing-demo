/// <reference types="cypress"/>

let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

describe("Transaction details e2e  ", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transaction")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );

    cy.addTransactions();
    cy.get(".mat-row")
      .first()
      .find("button")
      .find(".mat-accent")
      .click({ force: true });
  });
  after(() => {
    cy.deleteTransactions();
  });
  it("Should contain header", () => {
    cy.get("mat-card-title", { timeout: 4000 }).should("be.visible");
    cy.get("mat-dialog-container").contains("Amount");
    cy.get("mat-dialog-container").contains("Cash");
    cy.get("mat-dialog-container").contains("Created At");
    cy.get("mat-dialog-container").contains("Id");
    cy.get("mat-dialog-container").contains("Payment Received");
    cy.get("mat-dialog-container").contains("Received At");
    cy.get("mat-dialog-container").contains("Type");
  });

  it("should contain details that match created details", () => {
    cy.get(".close-button").click({ force: true });
    cy.get(".mat-header-row", { timeout: 5000 })
      .find(".mat-primary")
      .click({ force: true });
    cy.get(".mat-dialog-container", { timeout: 5000 }).should("be.visible");

    cy.get("input[formcontrolname=amount]").type("200");
    cy.get("mat-select[formcontrolname=type]", { timeout: 5000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get(".mat-stroked-button").click();
    cy.get("mat-table")
      .should("be.visible", { timeout: 4000 })
      .find(":nth-child(2)")
      .find(":nth-child(4)")
      .then($create1 => {
        let createText1 = $create1.text().slice(1, 5);
        console.log(createText1);
        cy.get(".mat-row")
          .first()
          .find("button")
          .find(".mat-accent")
          .click({ force: true });
        cy.get("mat-dialog-container")
          .should("be.visible", { timeout: 4000 })
          .find("table", { timeout: 4000 })
          .find(":nth-child(1)")
          .find(":nth-child(2)")
          .then($create2 => {
            let createText2 = $create2.text().slice(1, 5);
            expect(createText2).to.eq(createText1);
          });
      });
  });

  it("close button works", () => {
    cy.get(".close-button").click({ force: true });
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transaction")
      .should("be.visible");
  });
});
