/// <reference types="cypress"/>

let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

describe("Transaction delete", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transaction")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.addTransactions();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "transactions", { timeout: 40000 })
    );
    cy.get(".mat-row")
      .first()
      .find("button")
      .contains("delete")
      .click({ force: true });
  });
  it("pin field validators", () => {
    cy.get(".mat-input-element[formcontrolname=currentPIN]", { timeout: 4000 })
      .type("123456789")
      .click({ force: true });
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
  });
  it("delete button disabled for wrong input", () => {
    cy.get(".mat-input-element[formcontrolname=currentPIN]", { timeout: 4000 })
      .type("12345678")
      .click({ force: true });
    cy.get(".card-actions", { timeout: 4000 })
      .contains("Delete Transaction")
      .should("be.disabled");
  });
  it("delete button disabled for missing input", () => {
    cy.get(".mat-input-element[formcontrolname=currentPIN]", { timeout: 4000 })
      .type(bspace)
      .click({ force: true });
    cy.get(".card-actions", { timeout: 4000 })
      .contains("Delete Transaction")
      .should("be.disabled");
  });
  it("close button works", () => {
    cy.get(".close-button").click({ force: true });
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transaction")
      .should("be.visible");
  });
  after(() => {
    cy.logout();
  });
});
