let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

describe("Transaction delete e2e", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transaction")
      .click({ force: true });
          cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.addTransactions();
  });
  it("deletes Transaction", () => {
    cy.get(".mat-row")
      .first()
      .find("button")
      .contains("delete")
      .click({ force: true });
    cy.get(".mat-input-element[formcontrolname=currentPIN]", { timeout: 4000 })
      .type("asdf12")
      .click({ force: true });
    cy.get(".mat-stroked-button").click({ force: true });
  });
});
