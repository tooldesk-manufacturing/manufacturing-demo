/// <reference types="cypress"/>
describe("Receipt-add", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Transaction")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-header-row", { timeout: 5000 })
      .find(".mat-primary")
      .click({ force: true });
  });
  after(() => {
    cy.logout();
  });

  it("Add transaction should pop up should appear", () => {
    cy.get(".mat-dialog-container", { timeout: 5000 }).should("be.visible");
  });
  it("Check each field vaildators", () => {
    cy.get("input[formcontrolname=amount]", { timeout: 5000 }).click({
      force: true
    });
    cy.get(".mat-stroked-button", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
  });
  it("Button disable for wrong input", () => {
    cy.get("input[formcontrolname=amount]", { timeout: 5000 }).type("fde");
    cy.get(".mat-stroked-button", { timeout: 5000 }).should("be.disabled");
  });
  it("Button disable for missing input", () => {
    cy.get("input[formcontrolname=amount]").type(" ");
    cy.get(".mat-stroked-button").should("be.disabled");
  });
  it("close button should exist", () => {
    cy.get(".close-button", { timeout: 5000 }).should("be.visible");
  });
  it("close button should close dialog", () => {
    cy.get(".close-button", { timeout: 5000 }).click();
    cy.get(".mat-dialog-container", { timeout: 5000 }).should("not.be.visible");
  });
});
