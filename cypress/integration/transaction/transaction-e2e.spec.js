/// <reference types="cypress"/>

let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

describe("transaction e2e test", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Transaction")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "transactions", { timeout: 40000 })
    );
    cy.get(".mat-header-row", { timeout: 5000 })
      .find(".mat-primary")
      .click({ force: true });
  });
  after(() => {
    cy.logout();
  });

  it("Add transaction when tapped on the add button", () => {
    cy.get(".mat-dialog-container", { timeout: 5000 }).should("be.visible");

    cy.get("input[formcontrolname=amount]").type("200");
    cy.get("mat-select[formcontrolname=type]", { timeout: 5000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get(".mat-stroked-button").click();
    cy.deleteTransactions();
  });

  it("When credit/cash transaction should be added", () => {
    cy.addOrderWithQuantity("Sample123", "3");
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-expansion-panel-header")
      .click();
    cy.get("button")
      .contains("check_circle")
      .click({ force: true });
    cy.billingNavigation();
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type(bspace);
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type("20");
    cy.get("button")
      .contains("Generate", { timeout: 4000 })
      .click();
    cy.url().should("eq", base_url + "billing");

    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transactions")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "transactions");
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(
      "Sample123"
    );
    cy.get("button", { timeout: 5000 })
      .contains("search")
      .click({ force: true });

    cy.get("mat-row")
      .last()
      .find(":nth-child(4)")
      .find("span")
      .contains("71");
    cy.deleteTransactions();
  });
});
