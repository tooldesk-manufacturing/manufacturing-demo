// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("delete billing e2e tests", () => {
  before(() => {
    cy.loginWithOwner();

    cy.addOrderWithQuantity("Sample123", "3");
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find(".mat-expansion-indicator")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find("button")
      .contains("check_circle")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.billingNavigation();
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type(bspace);
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type("20");
    cy.get("button")
      .contains("Generate", { timeout: 4000 })
      .click();
    cy.waitUntil(() => cy.url().should("eq", base_url + "billing"));
  });
  after(() => {
    cy.logout();
  });

  it("deletes the order and the transaction related to that order", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transactions")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );

    cy.waitUntil(() => cy.url().should("eq", base_url + "transactions"));
    cy.get("app-transactions").should("be.visible");
    cy.get("mat-select[formcontrolname=searchField]").should("be.visible");

    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}");

    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(
      "Sample123"
    );
    cy.get("button", { timeout: 5000 })
      .contains("search")
      .click();

    cy.get("mat-row")
      .last()
      .find(":nth-child(4)")
      .find("span")
      .contains("71");
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Billing")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url().should("eq", base_url + "billing"));
    cy.get("mat-row")
      .first()
      .find(":nth-child(3)")
      .find("span")
      .contains("71");

    cy.get("mat-row", { timeout: 4000 })
      .first()
      .find(":nth-child(5)")
      .find("button")
      .contains("delete", { timeout: 4000 })
      .click();
    cy.get(".enter-pin-card", { timeout: 4000 })
      .should("be.visible")
      .find("input[formcontrolname=currentPIN]")
      .type("asdf12");
    cy.get(".mat-stroked-button", { timeout: 4000 }).click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.get("mat-table", { timeout: 4000 }).should("not.contain", "71");
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Transactions")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );

    cy.waitUntil(() => cy.url().should("eq", base_url + "transactions"));
    cy.get("app-transactions").should("be.visible");
    cy.get("mat-select[formcontrolname=searchField]").should("be.visible");

    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(
      "Sample123"
    );
    cy.get("button", { timeout: 5000 })
      .contains("search")
      .click();

    cy.get("mat-table").should("not.contain", "71");
  });
});
