/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
const todaysDate = Cypress.moment().format("D/M/YYYY");

var output =
  (("" + day).length < 2 ? "" : "") +
  day +
  "/" +
  (("" + month).length < 2 ? "" : "") +
  month +
  "/" +
  d.getFullYear();
describe(" bill unit tests", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Billing")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });
  after(() => {
    cy.logout();
  });
  it("heading, search,close button, table headers, paginator should be there", () => {
    cy.get(".search-button", { timeout: 5000 }).should("be.visible");
    cy.get(".mat-card-title", { timeout: 5000 }).should("be.visible");
    cy.get(".card-no-shadow", { timeout: 5000 })
      .find(".mat-icon-button")
      .contains("close")
      .should("be.visible");
    cy.get(".mat-paginator-navigation-previous");
    cy.get(".mat-paginator-navigation-next");
  });
  it("add bill button should be there", () => {
    cy.get(".multiple-billing-button", { timeout: 5000 }).should("be.visible");
  });
  it("add bill should redirect to '/multiple-billing'", () => {
    cy.get(".multiple-billing-button", { timeout: 5000 })
      .should("be.visible")
      .click();
    cy.url().should("eq", base_url + "billing/multiple-billing");
    cy.get(".mat-list-item-content")
      .contains("Billing")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });
  it("date field-date required", () => {
    cy.waitUntil(() => cy.get(".mat-select-value").should("be.visible"));
    cy.get(".mat-select-value", { timeout: 5000 }).contains("Date");
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(bspace);
    cy.get(".search-button", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
  });
  it("date field-date format", () => {
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(output);

    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).should(
      "have.value",
      todaysDate
    );
  });
  it("button disabled for wrong input", () => {
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(
      "axd/dd/tt"
    );
    cy.get(".search-button", { timeout: 5000 }).should("be.disabled");
  });
  it("close button resets to date today", () => {
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(bspace);
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(
      "8/5/2019"
    );
    cy.get(".card-no-shadow", { timeout: 5000 })
      .find(".mat-icon-button")
      .contains("close")
      .click();
    cy.get(".mat-select-value", { timeout: 5000 }).contains("Date");
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).should(
      "have.value",
      output
    );
  });
  it("billNo-requried", () => {
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".search-button", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
  });

  it("Customer Dc-required", () => {
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".search-button", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
  });
  it("InternalDC-required", () => {
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("downarrow")
      .type("{downarrow}")
      .type("downarrow");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".search-button", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
  });
  it("customerName-customer name format", () => {
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("downarrow")
      .type("{downarrow}")
      .type("downarrow");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).type(bspace);
    cy.get(".search-button", { timeout: 5000 }).click({ force: true });
    cy.get("mat-error", { timeout: 5000 }).should("be.visible");
  });
  it("customerName-customer list shows up", () => {
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).click();
    cy.get(".mat-autocomplete-panel")
      .children()
      .should("have.length.greaterThan", 0);
  });

  it("customerName -date format", () => {
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(bspace);
    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).type(output);

    cy.get("input[formcontrolname=fromDate]", { timeout: 5000 }).should(
      "have.value",
      todaysDate
    );
  });

  it("button disabled for missing input", () => {
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{uparrow}");
    cy.get("input[formcontrolname=searchTerm]", { timeout: 5000 }).should(
      "be.empty"
    );
    cy.get(".search-button", { timeout: 5000 }).should("be.disabled");
  });
});
