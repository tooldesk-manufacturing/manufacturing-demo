/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("billing e2e tests", () => {
  before(() => {
    //cy.viewport("macbook-15");
    cy.loginWithOwner();
    cy.addOrderWithName("Sample Industries", "9");
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find(".mat-expansion-indicator")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find("button")
      .contains("check_circle")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.billingNavigation();
  });
  after(() => {
    cy.deleteBill();
    cy.logout();
  });

  it("after order and bill created, details button shows up and opens bill and details match", () => {
    cy.get(".bill-table-outer")
      .find(":nth-child(6) > .ng-untouched")
      .type("20");
    cy.get(".bill-table-outer")
      .find(".ng-star-inserted > :nth-child(7)")
      .contains("40");
    cy.get(".mat-checkbox-layout", { timeout: 2000 })
      .find(".mat-checkbox-label")
      .contains("Bill on Weight")
      .click();
    cy.get(".mat-stroked-button", { timeout: 4000 }).click();

    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url("eq", base_url + "billing"));

    cy.waitUntil(() => cy.get("app-billing").should("be.visible"));
    cy.waitUntil(() =>
      cy.get(":nth-child(2) > .createdCell").should("be.visible")
    );
    cy.get("mat-table")
      .find(":nth-child(2)")
      .find(":nth-child(4)")
      .contains("212")
      .parent()
      .parent()
      .find(":nth-child(5)")
      .contains("info");
  });
});
