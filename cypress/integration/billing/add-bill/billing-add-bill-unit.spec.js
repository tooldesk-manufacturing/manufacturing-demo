/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("add billing unit tests", () => {
  before(() => {
    cy.loginWithOwner();

    cy.addOrder();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find(".mat-expansion-indicator")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find("button")
      .contains("check_circle")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.billingNavigation();
  });
  after(() => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Order")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.deleteOrder();
    cy.logout();
  });

  it.skip("bill on tax changes taxable value", () => {
    cy.get(".bill-table-outer")
      .find(":nth-child(6) > .ng-untouched")
      .type("20");
    cy.get(".bill-table-outer")
      .find(".ng-star-inserted > :nth-child(7)")
      .contains("40");
    cy.get(".mat-checkbox-layout", { timeout: 2000 })
      .find(".mat-checkbox-label")
      .contains("Bill on Weight")
      .click();
    cy.get(".bill-table-outer")
      .find(":nth-child(6) > .ng-untouched")
      .type("20");
    cy.get(".bill-table-outer")
      .find(".ng-star-inserted > .text-align-right")
      .contains("0");
  });
  it("cash,bill on weight checks,generate button,close button present", () => {
    cy.get(".mat-checkbox-layout", { timeout: 2000 })
      .find(".mat-checkbox-label")
      .contains("Cash")
      .should("be.visible");
    cy.get(".mat-checkbox-layout", { timeout: 2000 })
      .find(".mat-checkbox-label")
      .contains("Bill on Weight")
      .should("be.visible");
    cy.get(".mat-stroked-button", { timeout: 2000 }).should("be.visible");
    cy.get(".close-button", { timeout: 2000 }).should("be.visible");
  });
  it("close button redirects to billing", () => {
    cy.get(".close-button", { timeout: 2000 })
      .should("be.visible")
      .click();
    cy.url().should("eq", base_url + "billing");
    cy.get(".mat-list-item-content")
      .contains("Orders")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.billingNavigation();
  });
  it("generate button with no rate error", () => {
    cy.get(".mat-stroked-button", { timeout: 2000 })
      .should("be.visible")
      .click();
    cy.waitUntil(() => cy.get(".toast-info").should("be.visible"), {
      timeout: 50000
    });
    cy.waitUntil(() => cy.get(".toast-info").should("not.be.visible"), {
      timeout: 50000
    });
  });

  it("item detail editable", () => {
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(2)")
      .find(":nth-child(2)")
      .find("input")
      .type(" ");
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(2)")
      .find(":nth-child(3)")
      .find("input")
      .type(" ");
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(2)")
      .find(":nth-child(5)")
      .find("input")
      .type(" ");
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(2)")
      .find(":nth-child(6)")
      .find("input")
      .type(" ");
  });

  it("bill detail editable", () => {
    cy.get(".bill-details", { timeout: 2000 })
      .find(":nth-child(3)")
      .type(" ");
    cy.get(".bill-details", { timeout: 2000 })
      .find(":nth-child(5)")
      .type(" ");
    cy.get(".bill-details", { timeout: 2000 })
      .find(":nth-child(10)")
      .type(" ");
    cy.get(".bill-details", { timeout: 2000 })
      .find(":nth-child(12)")
      .type(" ");
    cy.get(".bill-details", { timeout: 2000 })
      .find(":nth-child(14)")
      .type(" ");
    cy.get(".bill-details", { timeout: 2000 })
      .find(":nth-child(16)")
      .type(" ");
  });

  it("item rate edit net amount change", () => {
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type(bspace);
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type("20");
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(".ng-star-inserted > :nth-child(7)")
      .contains("40");
  });

  it("gst edit, gross amount change", () => {
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type(bspace);
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type("20");
    cy.get(".mat-checkbox-layout", { timeout: 2000 })
      .find(".mat-checkbox-label")
      .contains("Bill on Weight")
      .should("be.visible")
      .click();
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(1)")
      .find(":nth-child(3)")
      .contains("40");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(2)")
      .find(":nth-child(3)")
      .contains("3.6");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(3)")
      .find(":nth-child(3)")
      .contains("3.6");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(4)")
      .find(":nth-child(3)")
      .contains("7.2");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(5)")
      .find(":nth-child(3)")
      .contains("0.2");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(7)")
      .find(":nth-child(2)")
      .contains("Forty");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(8)")
      .find(":nth-child(2)")
      .contains("47");
    cy.get(".final-table", { timeout: 2000 })
      .find("tbody")
      .find(":nth-child(9)")
      .find(":nth-child(2)")
      .contains("3.6");
    cy.get(".final-table", { timeout: 2000 })
      .find(":nth-child(9)")
      .find(":nth-child(3)")
      .contains("3.6");
  });
});
