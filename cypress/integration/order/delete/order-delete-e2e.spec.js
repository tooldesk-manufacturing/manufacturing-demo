/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("order delete  tests", () => {
  before(() => {
    cy.loginWithOwner();
    cy.addOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
  });

  it("deletes order", () => {
    cy.deleteOrder();
  });
});
