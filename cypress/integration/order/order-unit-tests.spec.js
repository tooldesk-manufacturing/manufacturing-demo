/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.env("RETRIES", 1);
describe("order unit testing", () => {
  const isVisible = elem =>
    !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  before(() => {
    cy.loginWithOwner();
    cy.addOrder();
  });

  after(() => {
    cy.loginWithOwner();
    cy.deleteOrder();
    cy.logout();
  });

  it("internal dc;created date ", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("table")
      .find("tr")
      .first()
      .find(":nth-child(3)")
      .should("be.visible");
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("table")
      .find("tr")
      .first()
      .find(":nth-child(4)")
      .should("not.be.empty");

    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("table")
      .find(":nth-child(2)")
      .find(":nth-child(3)")
      .should("be.visible");
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("table")
      .find(":nth-child(2)")
      .find(":nth-child(4)")
      .should("not.be.empty");
  });

  it("item arrow", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find(".mat-expansion-indicator")
      .should("be.visible");
  });

  it("item quantity", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find(".mat-expansion-indicator")
      .should("be.visible")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-card-content", { timeout: 4000 })
      .find("table")
      .find("tr")
      .first()
      .find(":nth-child(1)")
      .contains("Quantity");

    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-card-content", { timeout: 4000 })
      .find("table")
      .find("tr")
      .first()
      .find(":nth-child(2)")
      .should("not.be.empty");
  });

  it("delete order button", () => {
    cy.get("button", { timeout: 4000 })
      .find("mat-icon")
      .contains("more_vert")
      .click();
    cy.get("button", { timeout: 4000 })
      .contains("Delete")
      .should("be.visible");
    cy.get("button", { timeout: 4000 })
      .find("mat-icon")
      .contains("more_vert")
      .click({ force: true });
    cy.get(".mat-menu-content").should("not.be.visible");
  });

  it("if pending, add process and finish item buttons should be visible", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find(".mat-expansion-indicator")
      .should("be.visible")
      .click();
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .should("be.visible");
    cy.get("button")
      .contains("add_circle", { timeout: 4000 })
      .should("be.visible");
  });

  it("if ongoing subcontractor, finish subvendorprocess button should be visible", () => {
    cy.addSubvendorToOrder();
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Billing")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Order")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 40000 })
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("mat-card-actions")
      .find("button", { timeout: 4000 })
      .should("be.visible");
  });

  it("menu should display dc and billing", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("button", { timeout: 4000 })
      .find("mat-icon")
      .contains("more_vert")
      .click();
    cy.get("button", { timeout: 4000 })
      .contains("Generate DC")
      .should("be.visible");
    cy.get("button", { timeout: 4000 })
      .contains("Bill Completed Items")
      .should("be.visible");
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("button", { timeout: 4000 })
      .find("mat-icon")
      .contains("more_vert")
      .click({ force: true });
    cy.get(".mat-menu-content").should("not.be.visible");
  });
  it("billing should errror out if no completed items", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("button", { timeout: 4000 })
      .find("mat-icon")
      .contains("more_vert")
      .click();
    cy.get("button", { timeout: 4000 })
      .contains("Bill Completed Items")
      .should("be.visible")
      .click();
    cy.get(".toast-error", { timeout: 4000 }).should("be.visible");
  });

  it("timeline should open dialog", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .find(".timeline")
      .should("be.visible")
      .click();
    cy.get("mat-dialog-container").should("be.visible");
    cy.get(".close-button").click();
  });

  it("dc should redirect to delivery - challan / new", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("button", { timeout: 4000 })
      .find("mat-icon")
      .contains("more_vert")
      .click();
    cy.get("button", { timeout: 4000 })
      .contains("Generate DC")
      .should("be.visible")
      .click();
    cy.url().should("eq", base_url + "delivery-challan/new");
  });

  it("it redirects to worker tab", () => {
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Order")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.wait(60000);
    cy.addOrder();
    cy.addWorkerToOrder();
    cy.logout();
    cy.loginWithWorker();
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Order")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });

  it("if ongoing,claim item should be visible", () => {
    cy.get("app-order", { timeout: 40000 })
      .first()
      .find("mat-expansion-panel-header", { timeout: 4000 })
      .find("mat-panel-description")
      .find("mat-chip-list")
      .contains("ongoing");
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-expansion-panel-header", { timeout: 4000 })
      .contains("Chemical X")
      .click();
    cy.get("button")
      .contains("build")
      .should("be.visible")
      .click();
  });

  it("if claimed, unclaim button should be visible", () => {
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-expansion-panel-header", { timeout: 4000 })
      .find("mat-panel-description")
      .find("mat-chip-list")
      .contains("claimed");
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-expansion-panel-header", { timeout: 4000 })
      .click();
    cy.get("button")
      .contains("check_circle")
      .should("be.visible")
      .click();
    cy.get(".close-button").click();
  });
});
