/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.env("RETRIES", 2);
describe("order timeline unit testing", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.addOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
  });

  after(() => {
    cy.deleteOrder();
    cy.logout();
  });

  it("heading and icon exists ", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .find(".timeline")
      .should("be.visible")
      .click();
    cy.get("mat-card-title").should("be.visible");
  });

  it("at least one item exists", () => {
    cy.get("table")
      .children()
      .should("have.length.greaterThan", 0);
  });

  it("close button works", () => {
    cy.get(".close-button").click();
    cy.get("mat-dialog-container").should("not.be.visible");
  });
});
