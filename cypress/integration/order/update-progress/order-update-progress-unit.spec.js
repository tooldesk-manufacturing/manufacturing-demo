/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.env("RETRIES", 2);
describe("order update progress unit testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.addOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
    cy.addWorkerToOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
    cy.logout();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.loginWithWorker();
  });

  after(() => {
    cy.loginWithOwner();
    cy.deleteOrder();
    cy.logout();
  });

  it("going to worker", () => {
    cy.loginWithWorker();
  });

  it("heading, input,close button ", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("build", { timeout: 4000 })
      .click();
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get("mat-card-title").should("be.visible");
    cy.get(".close-button").should("be.visible");
    cy.get("input[formcontrolname=progress]").should("be.visible");
    cy.get(".close-button").click();
  });

  it("progress > 0 ", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type(bspace);
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type("-1");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
    cy.get(".close-button").click();
  });

  it("progress < quantity ", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type(bspace);
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type("4");
    cy.get(".mat-stroked-button").should("be.disabled");
    cy.get(".close-button").click();
  });

  it.skip("progress required ", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type(bspace);
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type(" ");
    cy.get(".mat-stroked-button").click();
    cy.get("mat-error").should("be.visible");
    cy.get(".close-button").click();
  });

  it("close button works", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get(".close-button").click();
    cy.get("mat-dialog-container").should("not.be.visible");
  });
});
