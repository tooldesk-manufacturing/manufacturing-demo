/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.env("RETRIES", 2);
describe("order create unit testing", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.get(".themeIcon", { timeout: 2000 }).click({ force: true });

    cy.get("button")
      .contains("Create a new order")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "order/create");
  });

  after(() => {
    cy.logout();
  });

  it("heading.close button,add item button,create button, 3 inputs exist ", () => {
    cy.get(".mat-card-title").should("be.visible");
    cy.get(".close-button").should("be.visible");
    cy.get("input[formControlName=customerName]").should("be.visible");
    cy.get("input[formControlName=customerDC]").should("be.visible");
    cy.get("input[formControlName=estimatedFinishDate]").should("be.visible");
    cy.get("button")
      .contains("Create Order")
      .should("be.visible");
    cy.get("button")
      .contains("add_circle")
      .should("be.visible");
  });

  it("Should check for functionality of the delete icon in the pop up opened on clicking the check button", () => {
    cy.get("input[formControlName=customerName]").type(bspace);

    cy.get("input[formControlName=customerName]").type("Sample Industries");
    cy.get("button")
      .contains("add_circle")
      .click();
    cy.get(".mat-icon-button")
      .find("mat-icon")
      .contains("delete")
      .click();
    cy.get(".mat-elevation-z8").should("not.be.visible");
  });

  it("item quantity required", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("Sample Industries");
    cy.get("button")
      .contains("add_circle")
      .click();
    cy.get(".mat-elevation-z8")
      .find("input[formControlName=quantity]")
      .type(" ");
    cy.get("button")
      .contains("Create Order")
      .click();
    cy.get("mat-error").should("be.visible");
  });

  it("item should contain name,quanttity,weight,hsn,remarks", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("Sample Industries");
    cy.get("button")
      .contains("add_circle")
      .click();
    cy.get(".mat-elevation-z8")
      .find("input[formControlName=name]")
      .should("be.visible");
    cy.get(".mat-elevation-z8")
      .find("input[formControlName=weight]")
      .should("be.visible");

    cy.get(".mat-elevation-z8").find("textarea[formControlName=remark]");

    cy.get(".mat-elevation-z8").find("input[formControlName=quantity]");
    cy.get(".mat-icon-button")
      .find("mat-icon")
      .contains("delete")
      .click();
  });

  it("button enabled for right input", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("Sample Industries");
    cy.get("button")
      .contains("add_circle")
      .parent()
      .parent()
      .should("not.be.disabled");
  });

  it("Should check for the disabling of the check button", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type(" ");
    cy.get("button")
      .contains("add_circle")
      .click()
      .parent()
      .parent()
      .should("be.disabled");
  });

  it("estimated finish date format error", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("Sample Industries");
    cy.get(".mat-icon-button")
      .find("svg")
      .click();
    cy.get(".mat-calendar-content")
      .find("mat-month-view")
      .find("tbody")
      .find("tr")
      .first()
      .find(":nth-child(2)")
      .click();

    cy.contains("Create Order").click();
    cy.get("mat-error").contains(
      "Estimated Finish Date has to be after today."
    );
  });

  it("customer no input", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type(" ");
    cy.get("button")
      .contains("Create Order")
      .click({ force: true });
    cy.get("mat-error").contains("Please select customer from the list");
  });

  it("customer wrong input", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("New");
    cy.get("button")
      .contains("Create Order")
      .click({ force: true });
    cy.get("mat-error").should("be.visible");
  });

  it("Should check if the options for customer is working while adding an order", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("Sample");
    cy.get("mat-option").contains("Sample Industries");
  });

  it("add item button adds item", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("Sample Industries");
    cy.get("input[formControlName=estimatedFinishDate]").type(bspace);
    cy.get("mat-icon")
      .contains("add_circle")
      .click();
    cy.get("input[formControlName=name]").type("Chemical X");
    cy.get("input[formControlName=weight]").type("2");
    cy.get("input[formControlName=quantity]").type("2");
    cy.get("button")
      .contains("Create Order")
      .parent()
      .should("not.be.disabled");
  });

  it("button disabled for wrong input", () => {
    cy.get("input[formControlName=customerName]").type(bspace);
    cy.get("input[formControlName=customerName]").type("New Industries");
    cy.get("button")
      .contains("Create Order")
      .parent()
      .click();
    cy.get("button")
      .contains("Create Order")
      .parent()
      .should("be.disabled");
  });

  it("close button redirects to order", () => {
    cy.get(".close-button").click();
    cy.url().should("eq", base_url + "order");
  });
});
