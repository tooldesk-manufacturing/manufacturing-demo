/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.env("RETRIES", 2);
describe("order e2e testing", () => {
  const isVisible = elem =>
    !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  before(() => {
    cy.loginWithOwner();
    cy.addOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
    cy.addSubvendorToOrder();
  });

  after(() => {
    cy.logout();
    cy.loginWithOwner();
    cy.deleteOrder();
  });

  it("finish subvendor process from Receptionist,Admin,Owner,Inspector side", () => {
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find(".mat-expansion-indicator")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find("button")
      .contains("check_circle")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success", { timeout: 40000 }).should("be.visible")
    );
  });

  it("moving to subvendor", () => {
    cy.addSubvendorToOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
  });
  it("finish subvendor process from subvendor side", () => {
    cy.loginWithSubvendor();
    cy.reload(true);
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("mat-expansion-panel-header", { timeout: 4000 })
      .contains("Chemical X")
      .click();
    cy.get("button")
      .contains("check_circle")
      .should("be.visible")
      .click();
    cy.get(".toast-success").should("be.visible");
  });

  it("moving to worker", () => {
    cy.logout();
    cy.loginWithOwner();
    cy.addWorkerToOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
    cy.get("app-order")
      .first()
      .find("mat-chip")
      .contains("ongoing");
    cy.logout();
    cy.loginWithWorker();
  });

  it("claim and unclaim full item", () => {
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("build", { timeout: 4000 })
      .click();
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type(bspace);
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type("2");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.contains("laiming");
    cy.logout();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );

    cy.loginWithOwner();

    cy.get("app-order")
      .first()
      .find("mat-chip")
      .contains("pending");
  });

  it("moving to worker", () => {
    cy.logout();
    cy.loginWithOwner();
    cy.addWorkerToOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
    cy.logout();
    cy.loginWithWorker();
  });

  it("claim and unclaim item", () => {
    cy.reload(true);
    cy.get("app-order")
      .first()
      .find("mat-expansion-panel-header")
      .click({ force: true });
    cy.get("button")
      .contains("build", { timeout: 4000 })
      .click();
    cy.get("button")
      .contains("check_circle", { timeout: 4000 })
      .click();
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type(bspace);
    cy.get("input[formcontrolname=progress]")
      .should("be.visible")
      .type("1");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.contains("laiming");
  });
});
