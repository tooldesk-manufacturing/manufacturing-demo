/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("Order home unit tests", () => {
  it("check if load more button exists", () => {
    cy.loginWithOwner();

    cy.get("button")
      .contains("more_horiz")
      .should("be.visible");
    cy.addOrder();
    cy.logout();
  });
  it("worker should have only pending tab", () => {
    cy.loginWithWorker();

    cy.url().should("eq", base_url + "order");
    cy.get(".mat-tab-labels", { timeout: 5000 })
      .children()
      .should("have.length", 1)
      .should("contain", "Pending");
    cy.logout();
  });

  it("customer should only have pending tab", () => {
    cy.loginWithCustomer();

    cy.get(".mat-tab-labels", { timeout: 5000 })
      .children()
      .should("have.length", 1)
      .should("contain", "Pending");
    cy.logout();
  });

  it("pending tab ui present", () => {
    cy.loginWithReceptionist();

    cy.get("#mat-input-3").should("be.visible");
    cy.get(".more-button").should("be.visible");
  });

  it("search tab ui", () => {
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .last()
      .click();
    cy.get("form")
      .children()
      .should("have.length.greaterThan", 4);
  });

  it("customer dc, internal dc field required", () => {
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .last()
      .click();
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{rightarrow}");
    cy.get("input[formcontrolname=searchTerm]").should("be.visible");
    cy.get("input[formcontrolname=searchTerm]").type(bspace);
    cy.get(".search-button").click({ force: true });
    cy.contains("Search term is required").should("be.visible");
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{rightarrow}");
    cy.get("input[formcontrolname=searchTerm]").should("be.visible");
    cy.get("input[formcontrolname=searchTerm]").type(bspace);
    cy.get(".search-button").click({ force: true });
    cy.contains("Search term is required").should("be.visible");
    cy.get(".search-button").should("be.disabled");
  });

  it("date field required and datepicker shows up", () => {
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .last()
      .click();
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow");
    cy.get("input[formcontrolname=fromDate]")
      .should("be.visible")
      .type(bspace);
    cy.get(".search-button").click({ force: true });
    cy.contains("Date is required").should("be.visible");
    cy.get("input[formcontrolname=fromDate]").type(bspace);
    cy.get("input[formcontrolname=fromDate]").type("asdewr");
    cy.get(".search-button").click({ force: true });
    cy.contains("Date is required").should("be.visible");
    cy.get(".search-button").should("be.disabled");
    cy.get("input[formcontrolname = fromDate]")
      .parent()
      .parent()
      .find(".mat-datepicker-toggle > .mat-icon-button")
      .should("be.visible")
      .click();
    cy.get("mat-calendar").should("be.visible");
    cy.get(".search-button").should("be.disabled");
  });

  it("customer name format and date required", () => {
    cy.get("button[color=accent]").click({ force: true });
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow}")
      .type("{downarrow");
    cy.get("input[formcontrolname=fromDate]").should("be.visible");
    cy.get("input[formcontrolname=toDate]").should("be.visible");
    cy.get("input[formcontrolname=searchTerm]")
      .should("be.visible")
      .type(bspace);
    cy.get(".search-button").click({ force: true });
    cy.contains("Customer is required").should("be.visible");
    cy.get("input[formcontrolname=searchTerm]").type(bspace);
    cy.get("input[formcontrolname=searchTerm]").type("asdasd");
    cy.get(".search-button").click({ force: true });
    cy.contains("Please select customer from the list").should("be.visible");
    cy.get("input[formcontrolname=fromDate]").type(bspace);
    cy.get("input[formcontrolname=fromDate]").type("asdewr");
    cy.get(".search-button").click({ force: true });
    cy.contains("Date is required").should("be.visible");
    cy.get("input[formcontrolname=toDate]").type(bspace);
    cy.get("input[formcontrolname=toDate]").type("asdewr");
    cy.get(".search-button").click({ force: true });
    cy.contains("Date is required").should("be.visible");
    cy.get(".search-button").should("be.disabled");
  });

  it("close button resets form", () => {
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .last()
      .click();
    cy.get("button[color=accent]").click();
    cy.get("form")
      .children()
      .should("have.length", 4);
    cy.get(".search-button").should("be.disabled");
  });

  it("search button disabled for no input", () => {
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .last()
      .click();
    cy.get("button[color=accent]").click();
    cy.get(".search-button").should("be.disabled");
    cy.get("mat-select[formcontrolname=searchField]", { timeout: 4000 })
      .type("{enter}")
      .type("{downarrow}")
      .type("{enter}");
    cy.get(".search-button").should("be.disabled");
  });

  it("search form visible when tab changed", () => {
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Transaction")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );

    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "transactions", { timeout: 40000 })
    );
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Order")
      .click({ force: true });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .last()
      .should("be.visible");
  });

  it("filter present in pending", () => {
    cy.get(".mat-tab-labels", { timeout: 4000 })
      .children()
      .first()
      .should("be.visible")
      .click();
    cy.get("[name='orderFilter']").should("be.visible");
  });

  after(() => {
    cy.logout();
  });
});
