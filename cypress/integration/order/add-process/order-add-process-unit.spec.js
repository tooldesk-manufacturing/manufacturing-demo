/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("Order add  process unit tests", () => {
  before(() => {
    cy.loginWithOwner();

    cy.addOrder();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );

    cy.get("mat-expansion-panel-header")
      .contains("Chemical X")
      .click();
    cy.get("button")
      .contains("add_circle")
      .click();
  });

  it("heading, radio group close button, exists", () => {
    cy.get("mat-radio-button")
      .children()
      .should("have.length", 2);
    cy.get(".close-button").should("be.visible");
    cy.get(".mat-card-title").should("be.visible");
  });

  it("department field missing", () => {
    cy.get("button")
      .contains("Add Process")
      .parent()
      .should("be.disabled");
  });

  it("button enabled for department field value", () => {
    const isVisible = elem =>
      !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
    cy.get(".add-process-card > .mat-card-content", { timeout: 4000 })
      .find("mat-form-field")
      .first()
      .find("div.mat-form-field-infix > mat-select")
      .focus()
      .type("{enter}{downarrow}", { force: true });
    cy.get("mat-select[formControlName=department]").then($matsel => {
      if ($matsel.text().includes("punching")) {
        return false;
      } else if ($matsel.text().includes("Department")) {
        cy.get(".add-process-card > .mat-card-content")
          .find("mat-form-field")
          .first()
          .find("div.mat-form-field-infix > mat-select")
          .focus()
          .type("{enter} {downarrow}", { force: true });
        if ($matsel.text().includes("Department")) {
          cy.get(".add-process-card > .mat-card-content")
            .find("mat-form-field")
            .first()
            .find("div.mat-form-field-infix > mat-select")
            .focus()
            .type("{enter} {downarrow}", { force: true });
        } else if ($matsel.text().includes("punching")) {
          return false;
        } else {
          cy.get("mat-option")
            .contains("punching")
            .should($mop3 => {
              expect(isVisible($mop3[0])).to.be.true;
              cy.contains("punching").click();
            });
        }
      } else {
        cy.get("mat-option")
          .contains("punching")
          .should($mop3 => {
            expect(isVisible($mop3[0])).to.be.true;
            cy.contains("punching").click();
          });
      }
      cy.get("button")
        .contains("Add Process")
        .parent()
        .should("not.be.disabled");
    });
    cy.get(".close-button").click({ force: true });
    cy.get(".add-process-card").should("not.be.visible");
  });

  it("button disabled subcontractor field missing", () => {
    cy.get("mat-expansion-panel-header")
      .contains("Chemical X")
      .click();
    cy.get("button")
      .contains("add_circle")
      .click();
    cy.get("mat-radio-button")
      .find(".mat-radio-label")
      .contains("Sub")
      .click();
    cy.get("button")
      .contains("Add Process")
      .parent()
      .should("be.disabled");
    cy.get(".close-button").click({ force: true });
    cy.get(".add-process-card").should("not.be.visible");
  });

  it("button enabled for valid subvendor field value", () => {
    cy.get("mat-expansion-panel-header")
      .contains("Chemical X")
      .click();
    cy.get("button")
      .contains("add_circle")
      .click();
    cy.get("mat-radio-button")
      .find(".mat-radio-label")
      .contains("Sub")
      .click();
    cy.get(".add-process-card > .mat-card-content", { timeout: 4000 })
      .find("mat-form-field")
      .first()
      .find("div.mat-form-field-infix > mat-select")
      .focus()
      .type("{enter}{downarrow}", { force: true });
    cy.get("mat-option", { timeout: 40000 }).then($mop3 => {
      if ($mop3.text().includes("Subcontractoro")) {
        cy.get(".mat-option-text")
          .contains("Subcontractoro", { timeout: 4000 })
          .click({ force: true });
        cy.get("button")
          .contains("Add Process")
          .parent()
          .should("not.be.disabled");
      }
    });
    cy.get(".close-button").click({ force: true });
    cy.get(".add-process-card").should("not.be.visible");
  });

  it("subcontractor fields - cash, price,description exists", () => {
    cy.get("mat-expansion-panel-header")
      .contains("Chemical X")
      .click();
    cy.get("button")
      .contains("add_circle")
      .click();
    cy.get("mat-radio-button")
      .find(".mat-radio-label")
      .contains("Sub")
      .click();
    cy.get("input[formControlName=description]").should("be.visible");
    cy.get(".mat-checkbox-layout").should("be.visible");
    cy.get("input[formControlName=price]").should("be.visible");
  });

  it("close button closes the tab", () => {
    cy.get(".close-button").click();
    cy.get(".add-process-card").should("not.be.visible");
  });

  after(() => {
    cy.deleteOrder();
    cy.logout();
  });
});
