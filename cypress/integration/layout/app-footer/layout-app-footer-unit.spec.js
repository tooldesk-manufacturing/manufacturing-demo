/// <reference types="cypress"/>
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

Cypress.env("RETRIES", 2);
describe("layout app footer unit tests", () => {
  before(() => {
    cy.loginWithOwner();
  });
  it("bottom arrow present", () => {
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "order", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get("mat-card-footer").should("be.visible", { timeout: 30000 })
    );
    cy.get(".themeIcon").should("be.visible", { timeout: 30000 });
  });
  it("bottom arrow opens sheet", () => {
    cy.get("button", { timeout: 4000 })
      .contains("keyboard_arrow_up")
      .should("be.visible")
      .click();
    cy.get("mat-bottom-sheet-container", { timeout: 3000 }).should(
      "be.visible"
    );
  });
  it("sheet contains buttons", () => {
    cy.get("mat-bottom-sheet-container", { timeout: 3000 })
      .find("button")
      .should("have.length", 2);
  });
  it("register button works", () => {
    cy.get("mat-bottom-sheet-container", { timeout: 3000 })
      .find("button")
      .contains("Register a new user")
      .click();
    cy.waitUntil(() => cy.url("eq", base_url + "management/register"));
  });
  it("create order button works", () => {
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.get("button", { timeout: 4000 })
      .contains("keyboard_arrow_up")
      .should("be.visible")
      .click();
    cy.get("mat-bottom-sheet-container", { timeout: 3000 })
      .find("button")
      .contains("Register a new user")
      .click();
    cy.waitUntil(() => cy.url("eq", base_url + "order/create"));
  });
});
