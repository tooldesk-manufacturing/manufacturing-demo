/// <reference types="cypress"/>
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

Cypress.env("RETRIES", 2);
describe("layout app nav unit testing", () => {
  before(() => {
    cy.loginWithOwner();
  });
  it("name should be present", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("mat-label")
      .contains("Tommy");
  });
  it("arrow next to name", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible");
  });
  it("arrow should open menu", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click();
    cy.get(".mat-menu-content").should("be.visible");
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
  });
  it("Orders link present", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Order")
      .should("be.visible");
  });
  it("Orders link works", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Order")
      .should("be.visible")
      .click({ force: true });
    cy.url("eq", base_url + "order");
  });

  it("update email option available", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("Update Email");
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
  });
  it("logout option available", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("Logout");
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
  });
  it("update email link works", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("Update Email")
      .click();
    cy.url("eq", base_url + "account/email");
  });
  it("transactions link present and works", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Order")
      .should("be.visible")
      .click({ force: true });
    cy.url("eq", base_url + "transactions");
  });
  it("billing link present and works", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Billing")
      .should("be.visible")
      .click({ force: true });
    cy.url("eq", base_url + "billing");
  });
  it("user management link present and works", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("User Management")
      .should("be.visible")
      .click({ force: true });
    cy.url("eq", base_url + "management");
  });
  it("dc link present and works", () => {
    cy.get(".mat-list-item-content", { timeout: 4000 })
      .contains("Delivery Challan")
      .should("be.visible")
      .click({ force: true });
    cy.url("eq", base_url + "delivery-challan");
  });
  it("update billing details link present and works", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("Update Billing")
      .click();
    cy.url("eq", base_url + "account/billing-details");
  });
  it("update departments link present and works", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("Update Department")
      .click();
    cy.url("eq", base_url + "account/departments");
  });
  it("view current plan present and works", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("View Current")
      .click();
    cy.url("eq", base_url + "account/current-plan");
  });
  it("update pin present and works", () => {
    cy.get("mat-toolbar", { timeout: 4000 })
      .find("button")
      .should("be.visible")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .should("be.visible")
      .contains("Update PIN")
      .click();
    cy.url("eq", base_url + "account/pin");
  });

  it("mobile view icons", () => {
    cy.viewport("iphone-6");

    cy.get("button", { timeout: 4000 })
      .contains("menu")
      .should("be.visible");
    cy.get("button", { timeout: 4000 })
      .contains("more_vert")
      .should("be.visible");
  });
});
