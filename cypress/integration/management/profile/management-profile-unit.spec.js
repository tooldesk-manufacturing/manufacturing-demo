let base_url = "http://localhost:4200/";
Cypress.env("RETRIES", 2);
describe("management-profile-unit testing", () => {
  before(() => {
    cy.loginWithOwner();

    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });

  it("name, email ,close button should be present", () => {
    cy.get("mat-row")
      .first()
      .find("mat-icon")
      .contains("info", { timeout: 3000 })
      .click();
    cy.get("mat-dialog-container").should("be.visible");
    cy.get(".user-header-image").should("be.visible");
    cy.get(".mat-card-title").should("be.visible");
    cy.get(".mat-chip").should("be.visible");
    cy.get(".close-button").click({ force: true });
  });
  it("Close button closes dialog", () => {
    cy.get("mat-row")
      .first()
      .find(".mat-accent")
      .contains("info")
      .click();
    cy.get(".mat-dialog-container").should("be.visible");
    cy.get(".close-button")
      .should("be.visible")
      .click();
    cy.get(".mat-dialog-container").should("not.be.visible");
  });
});
