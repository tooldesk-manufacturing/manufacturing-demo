let base_url = "http://localhost:4200/";
Cypress.env("RETRIES", 2);
describe("management-monthly-attendance-unit testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });
  //heading, month and year should be present
  it("heading, month and year should be present", () => {
    cy.get("mat-table")
      .find(":nth-child(2)")
      .find(":nth-child(3)")
      .contains("info")
      .click();
    cy.get(".mat-dialog-container").should("be.visible");
    cy.get(".mat-typography").should("be.visible");
  });

  //Close button closes dialog
  it("Close button closes dialog", () => {
    cy.get(".mat-dialog-container").should("be.visible");
    cy.get(".close-button")
      .should("be.visible")
      .click();
    cy.get(".mat-dialog-container").should("not.be.visible");
  });
});
