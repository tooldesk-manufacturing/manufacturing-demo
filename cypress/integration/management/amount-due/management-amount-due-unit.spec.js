let base_url = "http://localhost:4200/";
Cypress.env("RETRIES", 2);
describe("management-amount due-unit testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });
  /**
   * close button closes dialog
   */
  it("close button closes dialog", () => {
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .click();
    cy.get(".mat-accent")
      .contains("info", { timeout: 40000 })
      .first()
      .click({ force: true });
    cy.get("mat-dialog-container").should("be.visible");
    cy.get(".close-button").click();
    cy.get("mat-dialog-container").should("not.be.visible");
  });

});
