/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let enter =
  "{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}{enter}";
describe("management amount due update e2e testing", () => {
  before(() => {
    cy.loginWithOwner();
  });

  after(() => {
    cy.logout();
  });

  it("create order bill credit should show up for customer", () => {
    cy.addOrder();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find(".mat-expansion-indicator")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find("button")
      .contains("check_circle")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.billingNavigation();
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type(bspace);
    cy.get(".bill-table-outer", { timeout: 2000 })
      .find(":nth-child(6) > .ng-valid")
      .type("20");
    cy.get("button")
      .contains("Generate", { timeout: 4000 })
      .click();
    cy.url().should("eq", base_url + "billing");
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .click();
    cy.get("mat-row")
      .find(":nth-child(1)")
      .contains("Sample Industries")
      .parent()
      .find(".mat-accent")
      .click();
    cy.get("mat-dialog-container").should("be.visible");
    cy.get(":nth-child(2) > .td--5.ng-star-inserted > .mat-icon-button").should(
      "be.visible"
    );
    cy.get("table")
      .find(":nth-child(2)")
      .find(":nth-child(2)")
      .contains("47", { force: true });
    cy.get("button", { timeout: 4000 }).contains("attach_money");

    cy.get("button", { timeout: 4000 }).contains("notes");
    cy.get(".close-button").click();
  });

  it("create order,send to subvendor should show up", () => {
    cy.wait(60000);
    cy.addOrderWithName("Sample123", "2");
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find(".mat-expansion-indicator")
      .click();
    cy.get("app-order", { timeout: 4000 })
      .first()
      .should("be.visible")
      .find("mat-expansion-panel")
      .find("button")
      .contains("add_circle")
      .click();
    cy.get(".mat-radio-label-content", { timeout: 4000 })
      .contains("Subcontractor")
      .click();
    cy.get("input[formcontrolname=price]", { timeout: 4000 }).type("3000");
    cy.get(".mat-checkbox-layout", { timeout: 4000 }).click();
    cy.get("mat-select[formcontrolname=subvendorName]", { timeout: 4000 }).type(
      enter
    );
    cy.get(".mat-stroked-button", { timeout: 4000 }).click({ force: true });
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.get("app-order", { timeout: 4000 })
      .first()
      .find("button")
      .contains("more_vert")
      .click();
    cy.get(".mat-menu-item", { timeout: 4000 })
      .contains("View Transactions")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor", { timeout: 4000 })
      .click();
    cy.get("mat-row")
      .first()
      .find(".mat-accent")
      .click();

    cy.get(".td--5 > .mat-icon-button").should("be.visible");
    cy.get("table")
      .find(":nth-child(2)")
      .contains("3,000", { force: true });
    cy.get(".close-button").click();
    cy.get(".mat-list-item-content")
      .contains("Order")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });
});
