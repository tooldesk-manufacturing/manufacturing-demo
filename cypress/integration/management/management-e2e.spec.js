/// <reference types="cypress"/>
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

Cypress.env("RETRIES", 2);
describe("management e2e testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });

  /**
   * Marking the attendance
   */
  it("Marks the attendance", () => {
    cy.get("mat-table").should("be.visible");

    cy.get("mat-table")
      .find(":nth-child(2)")
      .find("mat-checkbox")
      .click();
    cy.get("button")
      .find("mat-icon")
      .contains("check_circle")
      .should("be.visible")
      .click();
    cy.contains("Marking attendance", { timeout: 3000 });
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("contain", "Successful")
    );
  });

  /**
   * Marking the time
   */
  it("Marks out the time", () => {
    cy.get("mat-table").should("be.visible");

    cy.get("mat-table")
      .find(":nth-child(2)")
      .find("mat-checkbox")
      .click();
    cy.get("button")
      .find("mat-icon")
      .contains("timer")
      .should("be.visible")
      .click();
    cy.contains("Marking out time", { timeout: 3000 });
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("contain", "Successful")
    );
  });
  it("edit the employee details and check the profile", () => {
    cy.get("mat-row")
      .first()
      .find(".edit-button", { timeout: 30000 })
      .should("be.visible")
      .click();
    cy.get(".edit-details-card").should("be.visible");
    cy.get("input[formControlName=phoneNumber]").type(bspace);
    cy.get("input[formControlName=phoneNumber]").type("1234567890");
    cy.get(".mat-stroked-button").click({ timeout: 30000 });
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("contain", "Successful")
    );
    cy.get("mat-row")
      .first()
      .find(".mat-primary", { timeout: 30000 })
      .should("be.visible")
      .click();
    cy.get("mat-dialog-container")
      .should("be.visible")
      .find("tr")
      .first()
      .find(":nth-child(2)")
      .should("contain", "1234567890");
    cy.get(".close-button").click();
  });

  it("Delete user works", () => {
    cy.addUser();
    cy.deleteUser();
  });
});
