/**
 * input field is disabled for email
 */
/// <reference types="cypress"/>
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";
Cypress.env("RETRIES", 2);
describe("management-edit details unit testing", () => {
  before(() => {
    cy.loginWithOwner();

    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });

  /**
   * Edit button shows up
   */
  it("Edit button shows up (employee)", () => {
    cy.get(".edit-button", { timeout: 30000 }).should("be.visible");
  });

  it("input field is disabled for email(employee)", () => {
    cy.get(".edit-button", { timeout: 30000 })
      .first()
      .click({ timeout: 4000 });

    cy.get("input[ng-reflect-type=email]").should(
      "have.attr",
      "readonly",
      "readonly"
    );
    cy.get(".close-button").click({ force: true });
  });

  it("input field is disabled for name(employee)", () => {
    cy.get(".edit-button", { timeout: 30000 })
      .first()
      .click({ timeout: 4000 });

    cy.get("input[ng-reflect-type=name]").should(
      "have.attr",
      "aria-invalid",
      "false"
    );
    cy.get(".close-button").click({ force: true });
  });

  /**
   * Tests for button (disabling and unabling)
   *
   */
  it("button is disabled for no update", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });

    cy.get(".mat-stroked-button").should("be.disabled");
    cy.get(".close-button").click({ force: true });
  });

  it("button is disabled for wrong update", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });

    cy.get("input[formControlName=phoneNumber]").type(bspace);
    cy.get("input[formControlName=phoneNumber]").type("1234567890123");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get(".mat-stroked-button").should("be.disabled");

    cy.get(".close-button").click({ force: true });
  });

  it("button is enabled for update", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });
    cy.get("input[formControlName=phoneNumber]").type(bspace);
    cy.get("input[formControlName=phoneNumber]").type("1234567890");

    cy.get(".mat-stroked-button").should("be.enabled");
    cy.get(".close-button").click({ force: true });
  });

  it("Tests for GSTIN validator (Customer)", () => {
    cy.get(".mat-tab-label-content")
      .contains("Customer")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ force: true });
    cy.get("input[formControlName=gstin]").type(bspace);
    cy.get("input[formControlName=gstin]").type("12345678901238971238791");
    cy.get("input[formControlName=landlineNumber]").type(
      "12345678901238971238791"
    );
    cy.get("mat-error", { timeout: 3000 }).should("be.visible");
    cy.get(".close-button").click({ force: true });
  });
  //

  it("Tests for tin format", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });
    cy.get("input[formControlName=tin]").type(bspace);
    cy.get("input[formControlName=tin]").type("12345678901238971238791");
    cy.get("input[formControlName=landlineNumber]").type(
      "12345678901238971238791"
    );
    cy.get("mat-error", { timeout: 3000 }).should("be.visible");
    cy.get(".close-button").click({ force: true });
  });

  it("Tests for landline format", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });
    cy.get("input[formControlName=landlineNumber]").type(
      "12345678901238971238791"
    );
    cy.get("input[formControlName=tin]").type("12345678901238971238791");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error", { timeout: 3000 }).should("be.visible");
    cy.get(".close-button").click({ force: true });
  });

  it("Tests for phone number validator", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });
    cy.get("input[formControlName=phoneNumber]").type(bspace);
    cy.get("input[formControlName=phoneNumber]").type("123");
    cy.get("textarea[formControlName=address]").type("12345678901238971238791");
    cy.get("mat-error", { timeout: 3000 }).should("be.visible");
    cy.get(".close-button").click({ force: true });
  });

  it("Tests for State code validator", () => {
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor")
      .click();
    cy.get(".edit-button")
      .first()
      .click({ timeout: 4000 });
    cy.get("input[formControlName=stateCode]").type(bspace);
    cy.get("input[formControlName=stateCode]").type("KAZ");
    cy.get("input[formControlName=tin]").type("12345678901238971238791");
    cy.get("mat-error", { timeout: 3000 }).should("be.visible");
    cy.get(".close-button").click({ force: true });
  });
});
