/// <reference types="cypress"/>

Cypress.env("RETRIES", 2);
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/management";
let base_url1 = "http://localhost:4200/";
let emailURL = Math.random();

describe("account register unit tests", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".themeIcon", { timeout: 40000 }).click();
    cy.get("button")
      .contains("Register a new user", { timeout: 40000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
  });

  it("Disable the button on the starting", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get(".mat-stroked-button").should("be.disabled");
  });

  it("Should show the validator for name (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=name]").type(" ");
    cy.get("input[formControlName=name]").type("{backspace}");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for no email (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=email]").type(" ");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for invalid email (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();

    cy.get("input[formControlName=email]").type("abc");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for no password (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();

    cy.get("input[formControlName=password]").type(" ");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("input[formControlName=password]").type("{backspace}");
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for invalid password (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=password]").type("adv");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("input[formControlName=password]").type("{backspace}");
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for no GSTIN(customer)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("input[formControlName=gstin]").type(bspace);
    cy.get("input[formControlName=gstin]").type(" ");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("input[formControlName=gstin]").type("{backspace}");
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for invalid GSTIN(customer)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("input[formControlName=gstin]").type(bspace);
    cy.get("input[formControlName=gstin]").type("123214");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for invalid State Code(customer)", () => {
    cy.get("input[formControlName=stateCode]").type(bspace);
    cy.get("input[formControlName=stateCode]").type("456");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").contains("Please enter a valid State Code");
    cy.get("input[formControlName=stateCode]").type(bspace);
    cy.get("input[formControlName=stateCode]").type("4");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").contains("Please enter a valid State Code");
  });

  it("When TIN is not entered properly error message should show up", () => {
    cy.get("input[formControlName=tin]").type(bspace);
    cy.get("input[formControlName=tin]").type("1231231");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").contains("Please enter a valid TIN");
  });

  it("Should show the validator for no confirm password (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();

    cy.get("input[formControlName=confirmPassword]", { timeout: 40000 }).type(
      " "
    );
    cy.get("input[formControlName=confirmPassword]").type(bspace);
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
  });

  it("Should show the validator for invalid confirm password (employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=password]").type("abv");
    cy.get("input[formControlName=confirmPassword]", { timeout: 40000 }).type(
      "adv"
    );
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
    cy.get("input[formControlName=confirmPassword]", { timeout: 40000 }).type(
      bspace
    );
  });

  it("Should enable the button for proper input(employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=name]").type(bspace);
    cy.get("input[formControlName=email]").type(bspace);
    cy.get("input[formControlName=password]").type(bspace);
    cy.get("input[formControlName=confirmPassword]", { timeout: 40000 }).type(
      bspace
    );
    cy.get("input[formControlName=name]").type("Sample");
    cy.get("input[formControlName=email]").type("Sample@" + emailURL + ".com");
    cy.get("input[formControlName=password]").type("asdf12");
    cy.get("input[formControlName=confirmPassword]", { timeout: 40000 }).type(
      "asdf12"
    );
    cy.get(".mat-stroked-button").should("not.be.disabled");
  });

  it("Disable the button for invalid input(employee)", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=name]").type("Sample");
    cy.get("input[formControlName=email]").type("Sample@" + emailURL + ".com");
    cy.get("input[formControlName=password]").type("asdf1");
    cy.get("input[formControlName=confirmPassword]", { timeout: 40000 }).type(
      "asdf12"
    );
    cy.get(".mat-stroked-button").should("be.disabled");
  });

  it("Should show limit reached if the number of registration for any employee is full", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get("mat-radio-button[ng-reflect-value=inspector]").click();
    cy.get("mat-card")
      .find("mat-card-title")
      .should("be.visible")
      .contains("limit reached");
    cy.get(".mat-stroked-button")
      .contains("View Current Plan")
      .should("be.visible");
  });

  it("View current plan button redirects to the current plan page", () => {
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-radio-button[ng-reflect-value=receptionist]").click();
    cy.get("mat-card")
      .find("mat-card-title")
      .should("be.visible");
    cy.get(".mat-stroked-button")
      .contains("View Current Plan")
      .click();
    cy.url().should("eq", base_url + "/current-plan");
  });

  it("admin should have owner tab too", () => {
    cy.logout();
    cy.loginWithAdmin();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url1 + "order", { timeout: 40000 })
    );
    cy.get(".themeIcon", { timeout: 40000 }).click();
    cy.get("button")
      .contains("Register a new user", { timeout: 40000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label-content")
      .contains("Owner", { timeout: 4000 })
      .should("be.visible");
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .should("be.visible");
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .should("be.visible");
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor", { timeout: 4000 })
      .should("be.visible");
  });

  it("Owner should have three tabs", () => {
    cy.logout();
    cy.loginWithOwner();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url1 + "order", { timeout: 40000 })
    );
    cy.get(".themeIcon", { timeout: 40000 }).click();
    cy.get("button")
      .contains("Register a new user", { timeout: 40000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label")
      .children()
      .should("have.length", 3);
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .should("be.visible");
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .should("be.visible");
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor", { timeout: 4000 })
      .should("be.visible");
  });

  it("Receptionist should be able to create workers", () => {
    cy.logout();
    cy.loginWithReceptionist();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().should("eq", base_url1 + "order", { timeout: 40000 })
    );
    cy.get(".themeIcon", { timeout: 40000 }).click();
    cy.get("button")
      .contains("Register a new user", { timeout: 40000 })
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() => cy.url().should("eq", base_url + "/register"));
    cy.get(".mat-tab-label")
      .children()
      .should("have.length", 3);
    cy.get(".mat-tab-label-content")
      .contains("Employee", { timeout: 4000 })
      .should("be.visible")
      .click();
    cy.get("mat-radio-group")
      .children()
      .should("have.length", 3);
  });

  after(() => {
    cy.logout();
  });
});
