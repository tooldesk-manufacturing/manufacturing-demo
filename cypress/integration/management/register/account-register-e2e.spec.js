/// <reference types="cypress"/>

Cypress.env("RETRIES", 2);
let base_url = "http://localhost:4200/";

describe("account register e2e tests", () => {
  before(() => {
    cy.loginWithOwner();
  });

  it("should update the plan values on adding a user", () => {
    cy.get("mat-toolbar")
      .find("button")
      .find("mat-icon")
      .contains("keyboard_arrow_down")
      .click({ force: true });
    cy.get(".mat-menu-content")
      .find("button")
      .contains("View Current")
      .click({ timeout: 3000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("table")
      .find(":nth-child(6)")
      .find("tr")
      .find(":nth-child(2)")
      .find("span")
      .then(oldWorkerCountRef => {
        let oldWorkerCount = +oldWorkerCountRef.text().slice(0, 1);
        console.log(oldWorkerCount);
        cy.get(".close-button", { timeout: 50000 }).click();
        cy.addUser();
        cy.get("mat-toolbar")
          .find("button")
          .find("mat-icon")
          .contains("keyboard_arrow_down")
          .click({ force: true });
        cy.get(".mat-menu-content")
          .find("button")
          .contains("View Current")
          .click({ timeout: 3000 });
        cy.waitUntil(() =>
          cy
            .get("ngx-loading-bar")
            .children()
            .should("have.length", 0, { timeout: 50000 })
        );
        cy.get("table")
          .find(":nth-child(6)")
          .find("tr")
          .find(":nth-child(2)")
          .find("span")
          .then(newWorkerCountRef => {
            let newWorkerCount = +newWorkerCountRef.text().slice(0, 1);
            console.log(newWorkerCount);
            const difference = newWorkerCount - oldWorkerCount;
            console.log(oldWorkerCount);
            expect(difference).to.eq(1);
          });
      });
    cy.deleteUser();
  });

  it("Should add a customer", () => {
    cy.addCustomer();
  });

  it("Should delete a customer", () => {
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.url().should("eq", base_url + "management");
    cy.get(".mat-tab-label-content")
      .contains("Customer", { timeout: 4000 })
      .click();
    cy.get("[name=nameFilter]").type("Sample123");
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("mat-table")
      .find(":nth-child(2)")
      .find("button")
      .contains("delete")
      .click({ force: true });
    cy.get("input[formcontrolname=currentPIN]").type("asdf12");
    cy.get(".mat-stroked-button").click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy
        .get(".toast-success")
        .should("contain", "Successful", { timeout: 40000 })
    );
  });

  it("Should add a subcontractor", () => {
    cy.addSubvendor();
  });

  it("Should delete a subcontractor", () => {
    cy.deleteSubvendor();
  });

  it("Should reject a user if email already exists employee", () => {
    cy.get("mat-icon")
      .contains("keyboard_arrow_up")
      .click();
    cy.get("button")
      .contains("Register a new user")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.url().then($url => $url == base_url + "management/register")
    );
    cy.get(".mat-tab-label-content")
      .contains("Employee")
      .click();
    cy.get("mat-radio-button[ng-reflect-value=worker]").click();
    cy.get("input[formControlName=name]").type("Sample");
    cy.get("input[formControlName=email]").type("a@c.com");
    cy.get("input[formControlName=password]").type("asdf12");
    cy.get("input[formControlName=confirmPassword]").type("asdf12");
    cy.get(".mat-stroked-button").click({ timeout: 3000 });
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("be.visible", { timeout: 40000 })
    );
  });

  after(() => {
    cy.logout();
  });
});
