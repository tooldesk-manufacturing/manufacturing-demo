/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.env("RETRIES", 2);
describe("management unit testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });

  it("employees customer subvendor tabs exists table header filter and paginator exist", () => {
    cy.get(".mat-tab-labels")
      .children()
      .should("have.length", 3);
    cy.get("[name=nameFilter]", { timeout: 4000 }).should("be.visible");
    cy.get(".mat-paginator-navigation-previous").should("be.visible");
    cy.get(".mat-paginator-navigation-next").should("be.visible");
  });

  it(" Mark attendance icon should be visible", () => {
    cy.get(
      "#mat-checkbox-2 > .mat-checkbox-layout > .mat-checkbox-inner-container"
    ).click();
    cy.get("button")
      .find("mat-icon")
      .contains("check_circle")
      .should("be.visible");
    cy.get(
      "#mat-checkbox-2 > .mat-checkbox-layout > .mat-checkbox-inner-container"
    ).click();
  });

  it(" Mark out time icon should be visibl", () => {
    cy.get(
      "#mat-checkbox-2 > .mat-checkbox-layout > .mat-checkbox-inner-container"
    ).click();
    cy.get("button")
      .find("mat-icon")
      .contains("timer")
      .should("be.visible");
    cy.get(
      "#mat-checkbox-2 > .mat-checkbox-layout > .mat-checkbox-inner-container"
    ).click();
  });
  it("Edit button shows up and open ups a dialog upon clicking", () => {
    cy.get("mat-row")
      .first()
      .find(".edit-button", { timeout: 30000 })
      .should("be.visible")
      .click();
    cy.get(".edit-details-card").should("be.visible");
    cy.get(".close-button").click();
  });
  it("Delete opens dialog", () => {
    cy.get("mat-row")
      .first()
      .find("button", { timeout: 30000 })
      .contains("delete")
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get(".close-button").click();
  });
  it("info button opens dialog", () => {
    cy.addAmountToSubvendor();
    cy.get(".mat-tab-label-content")
      .contains("Subcontractor", { timeout: 4000 })
      .click();
    cy.get(".mat-accent")
      .contains("info", { timeout: 40000 })
      .first()
      .click();
    cy.get("mat-dialog-container").should("be.visible");
    cy.get(".close-button").click();
    cy.deleteOrder();
  });
});
