let base_url = "http://localhost:4200/";
Cypress.env("RETRIES", 2);
describe("management-delete-unit testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.get(".mat-list-item-content")
      .contains("User Management")
      .click({ timeout: 10000 });
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.url().should("eq", base_url + "management");
  });

  after(() => {
    cy.logout();
  });

  it("Delete option is there for owner(employee) ", () => {
    cy.get("mat-row")
      .first()
      .find("button")
      .contains("delete")
      .should("be.visible");
  });

  it("heading and exit button exists", () => {
    cy.get("mat-row")
      .first()
      .find("button")
      .contains("delete")
      .should("be.visible")
      .click();
    cy.get(".mat-card-title", { timeout: 4000 }).should("be.visible");

    cy.get(".close-button", { timeout: 4000 }).should("be.visible");
  });

  it("Close button closes dialog ", () => {
    cy.get(".close-button", { timeout: 4000 })
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("not.be.visible");
  });

  it("Delete option opens a dialog ", () => {
    cy.get("app-management").should("be.visible");
    cy.get(
      ":nth-child(2) > .cdk-column-employeeEdit > .ng-star-inserted > .mat-button-wrapper > .mat-icon"
    )
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get(".close-button").click({ force: true });
  });

  it("Should show an error for invalid length PIN ", () => {
    cy.get("app-management").should("be.visible");
    cy.get(
      ":nth-child(2) > .cdk-column-employeeEdit > .ng-star-inserted > .mat-button-wrapper > .mat-icon"
    )
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get("input[formControlName=currentPIN]").type("asdw");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
    cy.get(".close-button").click({ force: true });
  });

  it("Should show an error for no PIN input", () => {
    cy.get("app-management").should("be.visible");
    cy.get(
      ":nth-child(2) > .cdk-column-employeeEdit > .ng-star-inserted > .mat-button-wrapper > .mat-icon"
    )
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get("input[formControlName=currentPIN]").type(" ");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("mat-error").should("be.visible");
    cy.get(".close-button").click({ force: true });
  });

  it("Should enable the button for valid length PIN ", () => {
    cy.get("app-management").should("be.visible");
    cy.get(
      ":nth-child(2) > .cdk-column-employeeEdit > .ng-star-inserted > .mat-button-wrapper > .mat-icon"
    )
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get("input[formControlName=currentPIN]").type("asdwew");
    cy.get(".mat-stroked-button").should("not.be.disabled");
    cy.get(".close-button").click({ force: true });
  });

  it("Should disable the button for invalid length PIN ", () => {
    cy.get("app-management")
      .should("be.visible")
      .should("be.visible");
    cy.get(
      ":nth-child(2) > .cdk-column-employeeEdit > .ng-star-inserted > .mat-button-wrapper > .mat-icon"
    )
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get("input[formControlName=currentPIN]").type("asd");
    cy.get(".mat-stroked-button").should("be.disabled");
    cy.get(".close-button").click({ force: true });
  });

  it("Should disable the button for no PIN ", () => {
    cy.get("app-management").should("be.visible");
    cy.get(
      ":nth-child(2) > .cdk-column-employeeEdit > .ng-star-inserted > .mat-button-wrapper > .mat-icon"
    )
      .should("be.visible")
      .click();
    cy.get(".enter-pin-card").should("be.visible");
    cy.get("input[formControlName=currentPIN]").type(" ");
    cy.get(".mat-stroked-button").should("be.disabled");
    cy.get(".close-button").click({ force: true });
  });
});
