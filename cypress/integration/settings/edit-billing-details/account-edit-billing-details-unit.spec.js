/// <reference types="cypress"/>

let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

describe("update billing unit tests", () => {
  before(() => {
    cy.loginWithAdmin();
  });

  it("Should contain all the texts", () => {
    cy.visit(base_url + "settings/billing-details");
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "settings/billing-details")
    );

    cy.get(".close-button").should("be.visible");
    cy.get("button")
      .contains("Update")
      .should("be.visible");
    cy.contains("Billing Details");
    cy.contains("Name");
    cy.contains("Email");
    cy.contains("Address");
    cy.contains("Phone Number");
    cy.contains("Second Phone Number");
    cy.contains("GSTIN");
    cy.contains("State Code");
    cy.contains("HSN Code");
    cy.contains("SAC Code");
    cy.contains("NEFT");
    cy.get("button").contains("Update");
  });
  it("update button enabled for correct input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type("ac@cb.com");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-card-actions")
      .find("button")
      .should("not.be.disabled");
  });
  it("  button disabled for no input", () => {
    cy.get("form")
      .find("mat-error")
      .should("be.not.visible");
    cy.get("input[formControlName=phoneNumber2]").type(bspace);
    cy.get("input[formControlName=phoneNumber2]").type("1234567890");
    cy.get("mat-card-actions")
      .find("button")
      .should("be.enabled");
  });

  it("button disabled for wrong input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type("asdasdasd");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid email address.");
    cy.get("mat-card-actions")
      .find("button")
      .should("be.disabled");
  });
  it(" button disabled for no input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("mat-card-actions")
      .find("button")
      .should("be.disabled");
  });

  it("name required", () => {
    cy.get("input[formControlName=name]").type(bspace);
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Name is required.");
  });

  it("address required", () => {
    cy.get("textarea[formControlName=address]").type(bspace);
    cy.get("textarea[formControlName=address]").type(bspace);
    cy.get("textarea[formControlName=address]").type(bspace);
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Address is required.");
  });

  it("email required", () => {
    cy.get("input[formControlName=email]").type(bspace);
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Email is required.");
  });

  it("email format", () => {
    cy.get("input[formControlName=email]").type("a");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid email address.");
  });

  it("phone required", () => {
    cy.get("input[formControlName=phoneNumber]").type(bspace);
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Phone number is required.");
  });

  it("phone required", () => {
    cy.get("input[formControlName=phoneNumber]").type("879312");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid phone number.");
  });

  it("second phone required", () => {
    cy.get("input[formControlName=phoneNumber2]").type("2189");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid phone number.");
  });

  it("second phone format", () => {
    cy.get("input[formControlName=phoneNumber2]").type(
      "{backspace}{backspace}{backspace}{backspace}"
    );
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid phone number.");
  });

  it("gstin required", () => {
    cy.get("input[formControlName=gstin]").type(bspace);
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("GSTIN is required");
  });

  it("gstin format", () => {
    cy.get("input[formControlName=gstin]").type(bspace);
    cy.get("input[formControlName=gstin]").type("12312312");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid GSTIN");
  });

  it("state code format 2 digits", () => {
    cy.get("input[formControlName=stateCode]").type(bspace);
    cy.get("input[formControlName=stateCode]").type("456");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid State Code");
    cy.get("input[formControlName=stateCode]").type(bspace);
    cy.get("input[formControlName=stateCode]").type("4");
    cy.get("button")
      .contains("Update")
      .click();
    cy.get("mat-error").contains("Please enter a valid State Code");
  });

  it("Close button redirects to order", () => {
    cy.visit(base_url + "settings/billing-details");
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "settings/billing-details")
    );
    cy.get(".close-button", { timeout: 4000 }).click();
    cy.url().should("eq", base_url + "order");
  });

  after(() => {
    cy.logout();
  });
});
