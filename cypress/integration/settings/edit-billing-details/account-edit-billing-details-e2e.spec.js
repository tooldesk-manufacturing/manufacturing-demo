/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("update billing e2e tests", () => {
  before(() => {
    cy.loginWithAdmin();
  });

  it("update details if valid", () => {
    cy.visit(base_url + "settings/billing-details");
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "settings/billing-details")
    );
    cy.get("input[formControlName=phoneNumber2]").type(bspace);
    cy.get("input[formControlName=phoneNumber2]").type("1234567890");
    cy.get("button")
      .contains("Update")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(
      () => cy.get(".toast-success").should("contain", "Successful"),
      { timeout: 40000 }
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("not.be.visible", { timeout: 40000 })
    );
  });

  after(() => {
    cy.logout();
  });
});
