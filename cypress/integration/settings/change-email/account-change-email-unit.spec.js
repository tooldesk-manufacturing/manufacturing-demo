/// <reference types="cypress"/>

let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";
let base_url = "http://localhost:4200/";

describe("update email unit tests", () => {
  before(() => {
    cy.loginWithAdmin();
  });

  it("should render heading, input field, close button and change email button", () => {
    cy.visit(base_url + "settings/email");
    cy.waitUntil(() => cy.url().should("eq", base_url + "settings/email"));
    cy.contains("Enter email");
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("form")
      .find("mat-label")
      .contains("Email");
    cy.get(".close-button").should("be.visible");
    cy.get("button").contains("Change Email");
  });

  it("update email button disabled for no input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type(" ");
    cy.get("mat-card-actions")
      .find("button")
      .should("be.disabled");
  });

  it("error for invalid email format", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type("abcom");
    cy.get("button")
      .contains("Change Email")
      .click();
    cy.contains("Please enter a valid email");
  });

  it("error for no input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type(" ");
    cy.get("button")
      .contains("Change Email")
      .click();
    cy.contains("Email is required");
  });

  it("update email button enabled for correct input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type("a@c.com");
    cy.get("mat-card-actions")
      .find("button")
      .should("be.enabled");
  });

  it("update email button disabled for wrong input", () => {
    cy.get("input[formcontrolname=email]").type(bspace);
    cy.get("input[formcontrolname=email]").type("abc");
    cy.get("mat-card-actions")
      .find("button")
      .click({ force: true });
    cy.get(".mat-stroked-button").should("be.disabled");
  });

  it("Close button redirects to order", () => {
    cy.get(".close-button", { timeout: 4000 }).click();
    cy.url().should("eq", base_url + "order");
  });

  after(() => {
    cy.logout();
  });
});
