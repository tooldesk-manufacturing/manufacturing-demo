/// <reference types="cypress"/>

describe("update email e2e tests", () => {
  beforeEach(() => {
    cy.loginWithAdmin();
  });
  it("updates email for right email ", () => {
    cy.visit(base_url + "settings/email");
    cy.waitUntil(() => cy.url().should("eq", base_url + "settings/email"));
    cy.get("input[formcontrolname=email]").type("a@c.com");
    cy.get("button")
      .contains("Change Email")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy
        .get(".toast-success")
        .should("contain", "Successful", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("not.be.visible", { timeout: 40000 })
    );
    cy.logout();
  });

  it("error when email is already used", () => {
    cy.visit(base_url + "settings/email");
    cy.waitUntil(() => cy.url().should("eq", base_url + "settings/email"));
    cy.get("input[formcontrolname=email]").type("o@b.com");
    cy.get("button")
      .contains("Change Email")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").contains("Error", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("not.be.visible", { timeout: 40000 })
    );
    cy.logout();
  });
});
