/// <reference types="cypress"/>
let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("change pin unit tests", () => {
  before(() => {
    cy.loginWithOwner();
  });

  it("update button enabled for correct input", () => {
    cy.visit(base_url + "settings/pin");
    cy.waitUntil(() => cy.url().should("eq", base_url + "settings/pin"));
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("input[formControlName=currentPIN]").type("asdf12");
    cy.get("input[formControlName=newPIN]").type("asdf12");
    cy.get("mat-card")
      .find(".mat-stroked-button")
      .should("not.be.disabled");
  });

  it("current PIN is 6 characters long", () => {
    cy.get("input[formControlName=currentPIN]").type("bpace");
    cy.get("button")
      .contains("Update PIN")
      .click();
    cy.get("mat-error").contains("Please enter a valid 6 digit PIN");
  });

  it("current PIN is required", () => {
    cy.get("input[formControlName=currentPIN]").type(bspace);
    cy.get("button")
      .contains("Update PIN")
      .click();
    cy.get("mat-error").contains("Current PIN is required");
  });

  it("new PIN is 6 characters long", () => {
    cy.get("input[formControlName=newPIN]").type("bpace");
    cy.get("button")
      .contains("Update PIN")
      .click();
    cy.get("mat-error").contains("Please enter a valid 6 digit PIN");
  });

  it("new PIN is required", () => {
    cy.get("input[formControlName=newPIN]").type(bspace);
    cy.get("button")
      .contains("Update PIN")
      .click();
    cy.get("mat-error").contains("Current PIN is required");
  });

  it("should render heading, two inputs, change button and close button", () => {
    cy.get(".close-button").should("be.visible");
    cy.get("mat-card-title").contains("Update PIN");
    cy.get("mat-label").contains("New PIN");
    cy.get("mat-label").contains("Current PIN");
    cy.get(".mat-button-wrapper").contains("Update PIN");
  });

  it("update PIN button disabled for missing input", () => {
    cy.get("input[formControlName=newPIN]").type(bspace);
    cy.get("mat-card")
      .find(".mat-stroked-button")
      .should("be.disabled");
  });

  it("update PIN button disabled for wrong input", () => {
    cy.get("input[formControlName=newPIN]").type("bspa");
    cy.get("mat-card")
      .find(".mat-stroked-button")
      .should("be.disabled");
  });

  it("Close button redirects to order", () => {
    cy.get(".close-button", { timeout: 4000 }).click();
    cy.url().should("eq", base_url + "order");
  });

  after(() => {
    cy.logout();
  });
});
