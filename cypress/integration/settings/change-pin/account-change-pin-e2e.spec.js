/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("change pin e2e tests", () => {
  before(() => {
    cy.loginWithOwner();
  });

  it("should update PIN for correct current PIN", () => {
    cy.visit(base_url + "settings/pin");
    cy.waitUntil(() => cy.url().should("eq", base_url + "settings/pin"));
    cy.get("input[formControlName=currentPIN]").type("asdf12");
    cy.get("input[formControlName=newPIN]").type("asdf12");
    cy.get("button")
      .contains("Update PIN")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy
        .get(".toast-success")
        .contains("Update PIN")
        .should("be.visible", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("not.be.visible", { timeout: 40000 })
    );
  });

  it("should reject for wrong current PIN", () => {
    cy.visit(base_url + "settings/pin");
    cy.waitUntil(() => cy.url().should("eq", base_url + "settings/pin"));
    cy.get("input[formControlName=currentPIN]").type(bspace);
    cy.get("input[formControlName=newPIN]").type(bspace);
    cy.get("input[formControlName=currentPIN]").type("aesd12");
    cy.get("input[formControlName=newPIN]").type("asdf12");
    cy.get("button")
      .contains("Update PIN")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("be.visible", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("not.be.visible", { timeout: 40000 })
    );
  });

  after(() => {
    cy.logout();
  });
});
