/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";

describe("update department e2e tests", () => {
  before(() => {
    cy.loginWithAdmin();
  });

  it("updates departments if valid", () => {
    cy.visit(base_url + "settings/departments");
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "settings/departments")
    );
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.get("button")
      .contains("add_circle")
      .click({ timeout: 4000 });
    cy.get("div.mat-form-field-infix")
      .find(".ng-invalid")
      .type("Trial");
    cy.get(".mat-stroked-button")
      .should("not.be.disabled")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy
        .get(".toast-success")
        .should("contain", "Successful", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("not.be.visible", { timeout: 40000 })
    );
    cy.get("div[ng-reflect-name=4]")
      .find("button")
      .find("mat-icon")
      .contains("delete")
      .click();
    cy.get(".mat-stroked-button")
      .should("not.be.disabled")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
  });

  after(() => {
    cy.logout();
  });
});
