/// <reference types="cypress"/>

Cypress.env("RETRIES", 2);

let base_url = "http://localhost:4200/";

describe("update department unit tests", () => {
  before(() => {
    cy.loginWithAdmin();
    cy.visit(base_url + "settings/departments");
    cy.waitUntil(() =>
      cy.url().should("eq", base_url + "settings/departments")
    );
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
  });

  it("button disabled for missing input", () => {
    cy.get(".mat-stroked-button").should("be.disabled");
  });

  it("button enabled for right input", () => {
    cy.get("button")
      .contains("add_circle")
      .click({ timeout: 4000 });
    cy.get("div.mat-form-field-infix")
      .find(".ng-invalid")
      .type("Trial");
    cy.get(".mat-stroked-button").should("not.be.disabled");
    cy.get("div[formarrayname=departments]")
      .children()
      .last()
      .find(".delete-button")
      .click();
  });

  it("delete input on delete button clicked", () => {
    cy.get("button")
      .contains("add_circle")
      .click({ timeout: 4000 });
    cy.get("div[formarrayname=departments]")
      .children()
      .last()
      .find(".delete-button")
      .click();
    cy.get(".mat-stroked-button").should("not.be.disabled");
  });

  it("new input on add button clicked", () => {
    cy.get("button")
      .contains("add_circle")
      .click({ timeout: 4000 });
    cy.get("div.mat-form-field-infix").find(".ng-invalid");
    cy.get("div[formarrayname=departments]")
      .children()
      .last()
      .find(".delete-button")
      .click();
  });

  it("validators for new department", () => {
    cy.get("button")
      .contains("add_circle")
      .click({ timeout: 4000 });
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("div[formarrayname=departments]")
      .children()
      .last()
      .find("input[formcontrolname=name]")
      .type("{backspace}");
    cy.get(".mat-stroked-button").click({ force: true });
    cy.get("div[formarrayname=departments]")
      .children()
      .last()
      .find("mat-error");
  });

  it("should show headings, close button, update department button,add button", () => {
    cy.get("mat-card-title").contains("Update Departments");
    cy.get(".close-button").should("be.visible");
    cy.get("button")
      .contains("Update Departments")
      .should("be.visible");
  });

  it("Close button redirects to order", () => {
    cy.get(".close-button", { timeout: 4000 }).click();
    cy.url().should("eq", base_url + "order");
  });

  after(() => {
    cy.logout();
  });
});
