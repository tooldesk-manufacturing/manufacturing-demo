/// <reference types="cypress"/>

import { timeout } from "q";
let base_url = "http://localhost:4200/";

describe("dc-add unit testing", () => {
  before(() => {
    cy.loginWithOwner();
    cy.addOrder()
    cy.dcNavigation();
  });
  after(() => {
    cy.get(".mat-list-item-content", { timeout: 5000 })
      .contains("Order")
      .click();
          cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.deleteOrder()
    cy.logout();
  });
  it("Generate button close button present", () => {
    cy.get(".mat-stroked-button", { timeout: 5000 }).should("be.visible");
    cy.get(".close-button").should("be.visible");
  });
  it("close DC redirect to Delivery challan", () => {
    cy.get(".close-button", { timeout: 5000 }).click();
    cy.url("eq", base_url + "delivery-challan");
    cy.get(".mat-list-item-content")
      .contains("Orders")
      .click();
    cy.waitUntil(() =>
      cy
        .get("ngx-loading-bar")
        .children()
        .should("have.length", 0, { timeout: 50000 })
    );
    cy.dcNavigation();
  });
  it("DC details editable", () => {
    cy.get(".customer-details", { timeout: 5000 })
      .find(":nth-child(2)")
      .type(" ");
    cy.get(".customer-details")
      .find(":nth-child(4)")
      .type(" ");
    cy.get(".customer-details")
      .find(":nth-child(6)")
      .type(" ");
    cy.get(".customer-details")
      .find(":nth-child(7)")
      .type(" ");
    cy.get(".customer-details", { timeout: 5000 })
      .find(":nth-child(8)")
      .type(" ");
    cy.get(".customer-details", { timeout: 5000 })
      .find(":nth-child(10)")
      .type(" ");
    cy.get(".customer-details", { timeout: 5000 })
      .find(":nth-child(12)")
      .type(" ");
    cy.get(".customer-details", { timeout: 5000 })
      .find(":nth-child(14)")
      .type(" ");
    cy.get(".receipt-details", { timeout: 5000 })
      .find(".ng-valid")
      .first()
      .type(" ");
    cy.get(".receipt-details", { timeout: 5000 })
      .find(":nth-child(4)")
      .type(" ");
    cy.get(".receipt-details", { timeout: 5000 })
      .find(":nth-child(6)")
      .type(" ");
    cy.get(".receipt-details", { timeout: 5000 })
      .find(":nth-child(12)")
      .type(" ");
    cy.get(".receipt-details", { timeout: 5000 })
      .find(":nth-child(14)")
      .type(" ");
    cy.get(".receipt-details", { timeout: 5000 })
      .find(":nth-child(16)")
      .type(" ");
  });
  it("Item details editable", () => {
    cy.get(".receipt-table", { timeout: 5000 })
      .find(":nth-child(2)")
      .find(":nth-child(1)")
      .find(":nth-child(2)")
      .find("input")
      .type(" ");
    cy.get(".receipt-table", { timeout: 5000 })
      .find(":nth-child(2)")
      .find(":nth-child(1)")
      .find(":nth-child(3)")
      .find("input")
      .type(" ");
    cy.get(".receipt-table", { timeout: 5000 })
      .find(":nth-child(2)")
      .find(":nth-child(1)")
      .find(":nth-child(5)")
      .find("input")
      .type(" ");
  });
});
