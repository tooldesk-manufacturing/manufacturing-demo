/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("account login unit tests", () => {
  before(() => {
    cy.visit(base_url);
    cy.waitUntil(() => cy.url().then($url => $url == base_url));
  });

  it("Should not login for wrong email", () => {
    cy.url().should("eq", base_url + "account");
    cy.get("input[formControlName=email]").type("a@ky.com", { timeout: 4000 });
    cy.get("input[formControlName=password]").type("asdf12", { timeout: 4000 });
    cy.get("form")
      .find("button")
      .contains("Login")
      .click();
    cy.contains("Logging in");
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("be.visible", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("not.be.visible", { timeout: 40000 })
    );
  });

  it("Should not login for wrong password", () => {
    cy.url().should("eq", base_url + "account");
    cy.get("input[formControlName=email]").type(bspace, { timeout: 4000 });
    cy.get("input[formControlName=email]").type("a@c.com", { timeout: 4000 });
    cy.get("input[formControlName=password]").type(bspace, { timeout: 4000 });
    cy.get("input[formControlName=password]").type("avdf12", { timeout: 4000 });
    cy.get("form")
      .find("button")
      .contains("Login")
      .click();
    cy.contains("Logging in");
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("be.visible", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("not.be.visible", { timeout: 40000 })
    );
  });
});
