/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("account login unit tests", () => {
  before(() => {
    cy.visit(base_url);
    cy.waitUntil(() => cy.url().then($url => $url == base_url));
  });

  it("should show headings,input,email button,close button", () => {
    cy.get("mat-card-title")
      .contains("ToolDesk")
      .should("be.visible");
    cy.get("input[formControlName=email").should("be.visible");
    cy.get("input[formControlName=password").should("be.visible");
    cy.get("button")
      .contains("Login")
      .should("be.visible");
  });

  it("email missing", () => {
    cy.get("input[formControlName=email").type(" ");
    cy.get(".submit-button").click();
    cy.get("mat-error").contains("Email is required");
  });

  it("email format", () => {
    cy.get("input[formControlName=email").type("a.com");
    cy.get(".submit-button").click();
    cy.get("mat-error").contains("Please enter a valid email address");
  });

  it("password required", () => {
    cy.get("input[formControlName=email").type(" ");
    cy.get(".submit-button").click();
    cy.get("mat-error").contains("Password is required");
  });

  it("password length.", () => {
    cy.get("input[formControlName=password").type("acom");
    cy.get(".submit-button").click();
    cy.get("mat-error").contains("The password must at least");
  });

  it("background is present", () => {
    cy.get(".login-bg");
  });
  it("button disabled for wrong input", () => {
    cy.get("input[formControlName=password").type(bspace);
    cy.get("input[formControlName=password").type("acom");
    cy.get(".submit-button").click();
    cy.get("mat-error").contains("The password must at least");
  });
  it("button enabled for right input", () => {
    cy.get("input[formControlName=password").type(bspace);
    cy.get("input[formControlName=password").type("abcom");
    cy.get(".submit-button").should("be.visible");
  });
});
