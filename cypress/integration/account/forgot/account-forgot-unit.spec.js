/// <reference types="cypress"/>

let base_url = "http://localhost:4200/account";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

describe("account forgot unit tests", () => {
  before(() => {
    cy.visit(base_url);
    cy.url().should("eq", base_url);
    cy.get("mat-hint")
      .contains("Forgot your password", { timeout: 40000 })
      .click();
    cy.waitUntil(() => cy.url().then($url => $url == base_url + "/forgot"));
  });

  it("should show headings,input,email button,close button", () => {
    cy.get("mat-card-title").contains("Enter email");
    cy.get(".close-button").should("be.visible");
    cy.get(".mat-stroked-button").should("be.visible");
    cy.get("mat-form-field").should("be.visible");
  });

  it("input email format", () => {
    cy.get("input[formControlName=email").type("a.com");
    cy.get("mat-card-title")
      .contains("Enter email")
      .click();
    cy.get("mat-error").contains("Please enter a valid email address");
  });

  it("input required", () => {
    cy.get("input[formControlName=email").type(" ");
    cy.get("input[formControlName=email").type(bspace);
    cy.get("mat-card-title")
      .contains("Enter email")
      .click();
    cy.get("mat-error").contains("Email is required");
  });

  it("button disabled for missing input", () => {
    cy.get("input[formControlName=email").type(bspace);
    cy.get("input[formControlName=email").type(" ");
    cy.get(".mat-stroked-button").should("be.disabled");
  });

  it("button disabled for wrong input", () => {
    cy.get("input[formControlName=email").type(bspace);
    cy.get("input[formControlName=email").type("bsca");
    cy.get(".mat-stroked-button").should("be.disabled");
  });

  it("button enabled for right input", () => {
    cy.get("input[formControlName=email").type(bspace);
    cy.get("input[formControlName=email").type("a@b");
    cy.get(".mat-stroked-button").should("not.be.disabled");
  });

  it("Close button redirects to order", () => {
    cy.get(".close-button", { timeout: 4000 }).click();
    cy.url().should("eq", base_url);
  });
});
