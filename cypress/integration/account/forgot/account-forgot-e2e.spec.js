/// <reference types="cypress"/>

let base_url = "http://localhost:4200/account";

describe("account forgot e2e tests", () => {
  beforeEach(() => {
    cy.visit(base_url);
    cy.url().should("eq", base_url);
    cy.get("mat-hint")
      .contains("Forgot your password")
      .click();
    cy.waitUntil(() => cy.url().then($url => $url == base_url + "/forgot"));
  });

  it("success if email exist", () => {
    cy.get("input[formControlName=email").type("a@c.com");
    cy.get(".mat-stroked-button")
      .should("not.be.disabled")
      .click();
    cy.contains("Sending password reset email");
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy
        .get(".toast-success")
        .should("contain", "Successful", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("not.be.visible", { timeout: 40000 })
    );
    cy.url().should("eq", base_url);
  });

  it("fail if email doesn't exist", () => {
    cy.get("input[formControlName=email").type("a@c");
    cy.get(".mat-stroked-button")
      .should("not.be.disabled")
      .click();
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("be.visible", { timeout: 40000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-error").should("not.be.visible", { timeout: 40000 })
    );
    cy.waitUntil(() => cy.url().then($url => $url == base_url + "/forgot"));
  });
});
