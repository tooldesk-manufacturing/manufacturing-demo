/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";

Cypress.Commands.add("dcNavigation", (email, pass) => {
  cy.get("app-order", { timeout: 5000 })
    .children()
    .first()
    .find("button")
    .contains("more_vert")
    .click();
  cy.get(".mat-menu-panel", { timeout: 5000 })
    .find("button")
    .contains("Generate DC")
    .click();
  cy.url().should("eq", base_url + "delivery-challan/new");
});