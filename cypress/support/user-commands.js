/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
let bspace =
  "{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}";

Cypress.Commands.add("addUser", () => {
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
  cy.get(".themeIcon").click();
  cy.get("button")
    .contains("Register a new user")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.url().should("eq", base_url + "management/register");
  cy.get("mat-radio-button[ng-reflect-value=worker]").click();
  cy.get("input[formControlName=name]").type("Sample123");
  cy.get("input[formControlName=email]").type(
    "Sample@" + Math.random() + ".com"
  );
  cy.get("input[formControlName=password]").type("asdf12");
  cy.get("input[formControlName=confirmPassword]").type("asdf12");
  cy.get(".mat-stroked-button", { timeout: 3000 }).click();
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.get(".toast-success").should("not.be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
});

Cypress.Commands.add("deleteUser", () => {
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("[name=nameFilter]").type("Sample123");
  cy.wait(4000);
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("mat-table")
    .find(":nth-child(2)")
    .should("be.visible")
    .find("button")
    .contains("delete")
    .click();
  cy.get(".enter-pin-card").should("be.visible");
  cy.get("input[formControlName=currentPIN]").type("asdf12");
  cy.get(".mat-stroked-button", { timeout: 3000 }).click();
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.get(".toast-success").should("not.be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
});

Cypress.Commands.add("addCustomer", () => {
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
  cy.get("mat-icon")
    .contains("keyboard_arrow_up")
    .click();
  cy.get("button")
    .contains("Register a new user")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.url().should("eq", base_url + "management/register");
  cy.get(".mat-tab-label-content")
    .contains("Customer")
    .click();
  cy.get("input[formControlName=name]").type("Sample123");
  cy.get("input[formControlName=email]").type(
    "Sample@" + Math.random() + ".com"
  );
  cy.get("input[formControlName=password]").type("asdf12");
  cy.get("input[formControlName=confirmPassword]").type("asdf12");
  cy.get("input[formControlName=gstin]").type(bspace + "123456789012345");
  cy.get(".mat-stroked-button").click({ timeout: 3000 });
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.get(".toast-success").should("not.be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
});

Cypress.Commands.add("deleteCustomer", () => {
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));

  cy.get(".mat-tab-label-content")
    .contains("Customer")
    .click();
  cy.contains("Sample123")
    .parent()
    .children()
    .last()
    .children()
    .last()
    .click();
  cy.get(".enter-pin-card").should("be.visible");
  cy.get("input[formControlName=currentPIN]").type("asdf12");
  cy.get(".mat-stroked-button").click({ timeout: 3000 });
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.get(".toast-success").should("not.be.visible"), {
    timeout: 50000
  });

  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
});

Cypress.Commands.add("addSubvendor", () => {
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
  cy.get("mat-icon")
    .contains("keyboard_arrow_up")
    .click();
  cy.get("button")
    .contains("Register a new user")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.url().should("eq", base_url + "management/register");
  cy.get(".mat-tab-label-content")
    .contains("Subcontractor")
    .click();
  cy.get("input[formControlName=name]").type("Sample123");
  cy.get("input[formControlName=email]").type(
    "Sample@" + Math.random() + ".com"
  );
  cy.get("input[formControlName=password]").type("asdf12");
  cy.get("input[formControlName=confirmPassword]").type("asdf12");
  cy.get("input[formControlName=gstin]").type(bspace + "123456789012345");
  cy.get(".mat-stroked-button").click({ timeout: 3000 });
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.get(".toast-success").should("not.be.visible"), {
    timeout: 50000
  });

  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
});

Cypress.Commands.add("deleteSubvendor", () => {
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
  cy.get(".mat-tab-label-content")
    .contains("Subcontractor")
    .click();
  cy.get("mat-cell")
    .contains("Sample123")
    .parent()
    .children()
    .last()
    .children()
    .last()
    .click();
  cy.get(".enter-pin-card").should("be.visible");
  cy.get("input[formControlName=currentPIN]").type("asdf12");
  cy.get(".mat-stroked-button").click({ timeout: 3000 });
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.get(".toast-success").should("not.be.visible"), {
    timeout: 50000
  });
  cy.waitUntil(() => cy.url().should("eq", base_url + "management"));
});
