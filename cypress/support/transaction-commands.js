/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";

Cypress.Commands.add("deleteTransactions", () => {
  cy.get(".mat-list-item-content", { timeout: 4000 })
    .contains("Transaction")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get(".mat-row")
    .find("button")
    .contains("delete")
    .click({ force: true });
  cy.get(".mat-input-element[formcontrolname=currentPIN]", {
    timeout: 4000
  }).type("asdf12");
  cy.get(".mat-button-wrapper")
    .contains("Delete Transaction")
    .click();
});
Cypress.Commands.add("addTransactions", () => {
  cy.get(".mat-header-row", { timeout: 5000 })
    .find(".mat-primary")
    .click({ force: true });
  cy.get(".mat-dialog-container", { timeout: 5000 }).should("be.visible");

  cy.get("input[formcontrolname=amount]").type("200");
  cy.get("mat-select[formcontrolname=type]", { timeout: 5000 })
    .type("{enter}")
    .type("{downarrow}");
  cy.get(".mat-stroked-button").click();
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() =>
    cy.url().should("eq", base_url + "transactions", { timeout: 40000 })
  );
});
