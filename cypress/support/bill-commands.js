/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";
Cypress.Commands.add("billingNavigation", () => {
  cy.get("app-order", { timeout: 4000 })
    .first()
    .find("button")
    .contains("more_vert")
    .click();
  cy.get("button")
    .contains("Bill Completed Items", { timeout: 40000 })
    .click();
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "billing/new"));
});

Cypress.Commands.add("deleteBill", () => {
  cy.get(".mat-list-item-content", { timeout: 5000 })
    .contains("Billing")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("mat-row", { timeout: 4000 })
    .first()
    .find(":nth-child(5)")
    .find("button")
    .contains("delete", { timeout: 4000 })
    .click();
  cy.get(".enter-pin-card", { timeout: 4000 })
    .should("be.visible")
    .find("input[formcontrolname=currentPIN]")
    .type("asdf12");
  cy.get(".mat-stroked-button", { timeout: 4000 }).click();
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
});
