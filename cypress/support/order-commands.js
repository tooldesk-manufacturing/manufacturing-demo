/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";

Cypress.Commands.add("addOrder", () => {
  cy.get(".themeIcon")
    .should("be.visible", { timeout: 30000 })
    .click();
  cy.get("button")
    .contains("Create a new order")
    .should("be.visible", { timeout: 30000 })
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "order/create"));
  cy.get("input[formControlName=customerName]").type("Sample Industries");
  cy.waitUntil(() =>
    cy
      .get("mat-icon")
      .contains("add_circle")
      .should("be.visible", { timeout: 30000 })
      .click()
  );
  cy.get("input[formControlName=name]").type("Chemical X");
  cy.get("input[formControlName=weight]").type("2");
  cy.get("input[formControlName=quantity]").type("2");
  cy.get("button")
    .contains("Create Order")

    .should("be.visible", { timeout: 30000 })
    .click();
  cy.contains("Creating order");
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("contain", "Successful"));
  cy.url().should("eq", base_url + "order");
});

Cypress.Commands.add("deleteOrder", () => {
  cy.get(".mat-list-item-content")
    .contains("Order")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() =>
    cy.url().should("eq", base_url + "order", { timeout: 40000 })
  );
  cy.get(".ps__thumb-y").then($thumb => {
    $thumb.css("top", "0px");
  });
  cy.get("mat-card")
    .first()
    .find(":nth-child(1)")
    .find("button")
    .find("mat-icon")
    .contains("more_vert")
    .should("be.visible", { timeout: 30000 })
    .click();
  cy.get("button")
    .contains("Delete")
    .click({ force: true });
  cy.get("input[formControlName=currentPIN]").type("asdf12");
  cy.get("button")
    .contains("Delete Order")

    .should("be.visible", { timeout: 30000 })
    .click();
  cy.contains("Deleting");
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("contain", "Successful"));
  cy.waitUntil(() =>
    cy.url().should("eq", base_url + "order", { timeout: 40000 })
  );
});

Cypress.Commands.add("addWorkerToOrder", () => {
  const isVisible = elem =>
    !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("mat-expansion-panel-header")
    .contains("Chemical X")

    .should("be.visible", { timeout: 30000 })
    .click();
  cy.get("button")
    .contains("add_circle")

    .should("be.visible", { timeout: 30000 })
    .click();
  cy.get(".add-process-card > .mat-card-content", { timeout: 4000 })
    .find("mat-form-field")
    .first()
    .find("div.mat-form-field-infix > mat-select")
    .focus()
    .type("{enter}{downarrow}", { force: true });
  cy.get("mat-select[formControlName=department]").then($matsel => {
    if ($matsel.text().includes("punching")) {
      return false;
    } else if ($matsel.text().includes("Department")) {
      cy.get(".add-process-card > .mat-card-content")
        .find("mat-form-field")
        .first()
        .find("div.mat-form-field-infix > mat-select")
        .focus()
        .type("{enter} {downarrow}", { force: true });
      if ($matsel.text().includes("Department")) {
        cy.get(".add-process-card > .mat-card-content")
          .find("mat-form-field")
          .first()
          .find("div.mat-form-field-infix > mat-select")
          .focus()
          .type("{enter} {downarrow}", { force: true });
      } else if ($matsel.text().includes("punching")) {
        return false;
      } else {
        cy.get("mat-option")
          .contains("punching")
          .should($mop3 => {
            expect(isVisible($mop3[0])).to.be.true;
            cy.contains("punching").click();
          });
      }
    } else {
      cy.get("mat-option")
        .contains("punching")
        .should($mop3 => {
          expect(isVisible($mop3[0])).to.be.true;
          cy.contains("punching").click();
        });
    }
    cy.get("button")
      .contains("Add Process")
      .click({ force: true });
    cy.contains("Adding subprocess");
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("contain", "Successful")
    );
  });
});
Cypress.Commands.add("addSubvendorToOrder", () => {
  const isVisible = elem =>
    !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("mat-expansion-panel-header")
    .contains("Chemical X")
    .click();
  cy.get("button")
    .contains("add_circle")
    .click();
  cy.get("mat-radio-button")
    .contains("Subcontractor")
    .click();
  cy.get(".add-process-card > .mat-card-content", { timeout: 4000 })
    .find("mat-form-field")
    .first()
    .find("div.mat-form-field-infix > mat-select")
    .focus()
    .type("{enter}{downarrow}", { force: true });
  cy.get("mat-select[formControlName=subvendorName]").then($matsel => {
    if ($matsel.text().includes("Subcontractoro")) {
      return false;
    } else if ($matsel.text().includes("Subcontractor")) {
      cy.get(".add-process-card > .mat-card-content")
        .find("mat-form-field")
        .first()
        .find("div.mat-form-field-infix > mat-select")
        .focus()
        .type("{enter} {downarrow}", { force: true });
      if ($matsel.text().includes("Subcontractor")) {
        cy.get(".add-process-card > .mat-card-content")
          .find("mat-form-field")
          .first()
          .find("div.mat-form-field-infix > mat-select")
          .focus()
          .type("{enter} {downarrow}", { force: true });
      } else if ($matsel.text().includes("Subcontractoro")) {
        return false;
      } else {
        cy.get("mat-option")
          .contains("Subcontractoro")
          .should($mop3 => {
            expect(isVisible($mop3[0])).to.be.true;
            cy.get("mat-option", { timeout: 4000 })
              .contains("Subcontractoro")
              .click();
          });
      }
    } else {
      cy.get("mat-option")
        .contains("Subcontractoro")
        .should($mop3 => {
          expect(isVisible($mop3[0])).to.be.true;
          cy.contains("Subcontractoro").click();
        });
    }
    cy.get("button")
      .contains("Add Process")
      .click({ force: true });
    cy.contains("Adding subprocess");
    cy.waitUntil(() =>
      cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
    );
    cy.waitUntil(() =>
      cy.get(".toast-success").should("contain", "Successful")
    );
  });
});

Cypress.Commands.add("addOrderWithName", (name, weight) => {
  cy.get(".themeIcon")
    .should("be.visible", { timeout: 30000 })
    .click();
  cy.get("button")
    .contains("Create a new order")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "order/create"));
  cy.get("input[formControlName=customerName]").type(name);
  cy.get("mat-icon")
    .contains("add_circle")
    .click();
  cy.get("input[formControlName=name]").type("Chemical X");
  cy.get("input[formControlName=weight]").type(weight);
  cy.get("input[formControlName=quantity]").type("2");
  cy.get("button")
    .contains("Create Order")
    .click();
  cy.contains("Creating order");
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("contain", "Successful"));

  cy.waitUntil(() => cy.url().should("eq", base_url + "order"));
});

Cypress.Commands.add("addOrderWithQuantity", (name, quantit) => {
  cy.get(".themeIcon")
    .should("be.visible", { timeout: 30000 })
    .click();
  cy.get("button")
    .contains("Create a new order")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.waitUntil(() => cy.url().should("eq", base_url + "order/create"));
  cy.get("input[formControlName=customerName]").type(name);
  cy.get("mat-icon")
    .contains("add_circle")
    .click();
  cy.get("input[formControlName=name]").type("Chemical X");
  cy.get("input[formControlName=weight]").type("2");
  cy.get("input[formControlName=quantity]").type(quantit);
  cy.get("button")
    .contains("Create Order")
    .click();
  cy.contains("Creating order");
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("contain", "Successful"));
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );

  cy.waitUntil(() => cy.url().should("eq", base_url + "order"));
});

Cypress.Commands.add("addAmountToSubvendor", () => {
  cy.wait(60000);
  cy.addOrderWithName("Sample123", "2");
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get(".mat-list-item-content")
    .contains("Order")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("app-order", { timeout: 4000 })
    .first()
    .should("be.visible")
    .find("mat-expansion-panel")
    .find(".mat-expansion-indicator")
    .click();
  cy.get("app-order", { timeout: 4000 })
    .first()
    .should("be.visible")
    .find("mat-expansion-panel")
    .find("button")
    .contains("add_circle")
    .click();
  cy.get(".mat-radio-label-content", { timeout: 4000 })
    .contains("Subcontractor")
    .click();
  cy.get("input[formcontrolname=price]", { timeout: 4000 }).type("3000");
  cy.get(".mat-checkbox-layout", { timeout: 4000 }).click();
  cy.get("mat-select[formcontrolname=subvendorName]", { timeout: 4000 }).type(
    enter
  );
  cy.get(".mat-stroked-button", { timeout: 4000 }).click({ force: true });
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.get("app-order", { timeout: 4000 })
    .first()
    .find("button")
    .contains("more_vert")
    .click();
  cy.get(".mat-menu-item", { timeout: 4000 })
    .contains("View Transactions")
    .click();
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get(".mat-list-item-content")
    .contains("User Management")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.url().should("eq", base_url + "management");
  cy.get(".mat-tab-label-content")
    .contains("Subcontractor", { timeout: 4000 })
    .click();
  cy.get("mat-row")
    .first()
    .find(".mat-accent")
    .click();

  cy.get(".td--5 > .mat-icon-button").should("be.visible");
  cy.get("table")
    .find(":nth-child(2)")
    .contains("3,000", { force: true });
  cy.get(".close-button").click();
  cy.get(".mat-list-item-content")
    .contains("Order")
    .click({ timeout: 10000 });
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
});

Cypress.Commands.add("addSubvendorToOrders", () => {
  const isVisible = elem =>
    !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
  cy.waitUntil(() =>
    cy
      .get("ngx-loading-bar")
      .children()
      .should("have.length", 0, { timeout: 50000 })
  );
  cy.get("mat-expansion-panel-header")
    .contains("Chemical X")
    .click();
  cy.get("button")
    .contains("add_circle")
    .click();
  cy.get("mat-radio-button")
    .contains("Subcontractor")
    .click();
  cy.get(".add-process-card ").should("be.visible");
  cy.get(".mat-select-arrow").then($arrow => {
    $arrow.click();
    $arrow.click();
    $arrow.click();
    $arrow.click();
    $arrow.click();
    $arrow.click();
    $arrow.click();
  });
  cy.get(".mat-option-text")
    .contains("Subcontractoro")
    .click();
  cy.get("button")
    .contains("Add Process")
    .click({ force: true });
  cy.contains("Adding subprocess");
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() => cy.get(".toast-success").should("contain", "Successful"));
});
