/// <reference types="cypress"/>

let base_url = "http://localhost:4200/";

Cypress.Commands.add("login", (email, pass) => {
  cy.visit(base_url);
  cy.waitUntil(() =>
    cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
  );
  cy.waitUntil(() =>
    cy
      .url()
      .then($url => $url == base_url + "account" || $url == base_url + "order")
  );
  cy.url().then($url => {
    if ($url == base_url + "account") {
      cy.get("input[formControlName=email]").type(email);
      cy.get("input[formControlName=password]").type(pass);
      cy.get("form")
        .find("button")
        .contains("Login")
        .should("be.visible", { timeout: 10000 })
        .click();
      cy.contains("Logging in", { timeout: 50000 });
      cy.waitUntil(() =>
        cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
      );
      cy.waitUntil(() => cy.get(".toast-success", { timeout: 30000 }));
      cy.url().should("eq", base_url + "order");
    } else {
      cy.get("button")
        .find("mat-icon")
        .contains("keyboard_arrow_down")
        .should("be.visible", { timeout: 10000 })
        .click();
      cy.get("button")
        .contains("Logout")
        .should("be.visible", { timeout: 10000 })
        .click();
      cy.waitUntil(() => cy.url().should("eq", base_url + "account"));
      cy.get("input[formControlName=email]", { timeout: 40000 })
        .should("be.visible", { timeout: 4000 })
        .type(email);
      cy.get("input[formControlName=password]", { timeout: 40000 })
        .should("be.visible", { timeout: 4000 })
        .type(pass);
      cy.get("form")
        .find("button")
        .contains("Login", { timeout: 40000 })
        .click();
      cy.contains("Logging in", { timeout: 50000 });
      cy.waitUntil(() =>
        cy.get(".shapeshifter").should("not.be.visible", { timeout: 50000 })
      );
      cy.waitUntil(() => cy.get(".toast-success", { timeout: 30000 }));
    }
  });
});

Cypress.Commands.add("loginWithAdmin", () => {
  cy.login("a@c.com", "asdf12");
});

Cypress.Commands.add("loginWithCustomer", () => {
  cy.login("c@b.com", "asdf12");
});

Cypress.Commands.add("loginWithSubvendor", () => {
  cy.login("s@b.com", "asdf12");
});

Cypress.Commands.add("loginWithWorker", () => {
  cy.login("w@b.com", "asdf12");
});

Cypress.Commands.add("loginWithInspector", () => {
  cy.login("i@b.com", "asdf12");
});

Cypress.Commands.add("loginWithReceptionist", () => {
  cy.login("r@b.com", "asdf12");
});

Cypress.Commands.add("loginWithOwner", () => {
  cy.login("o@b.com", "asdf12");
});

Cypress.Commands.add("logout", () => {
  cy.get(".mat-drawer-inner-container").should("be.visible");
  cy.get("button")
    .find("mat-icon")
    .contains("keyboard_arrow_down")
    .should("be.visible", { timeout: 10000 })
    .click();
  cy.get("button")
    .contains("Logout")
    .should("be.visible", { timeout: 10000 })
    .click();
  cy.url().should("eq", base_url + "account");
});
