// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBFkNbSlt-ylfOcDu5PJeB_picX1pzCm4w',
    authDomain: 'manufacturingdemo-b819b.firebaseapp.com',
    databaseURL: 'https://manufacturingdemo-b819b.firebaseio.com',
    projectId: 'manufacturingdemo-b819b',
    storageBucket: 'manufacturingdemo-b819b.appspot.com',
    messagingSenderId: '770811728325'
  }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
