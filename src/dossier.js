var onboardify = {
  obOrg: 'ToolDesk',
  obEmail: 'anonymous'
};
function OBgetCookie(a) {
  a += '=';
  for (var c = document.cookie.split(';'), b = 0; b < c.length; b++) {
    for (var d = c[b]; ' ' == d.charAt(0); ) d = d.substring(1);
    if (-1 != d.indexOf(a)) return d.substring(a.length, d.length);
  }
  return '';
}
function OBloadScript(a, c) {
  var b = document.createElement('SCRIPT');
  b.src = a;
  b.onload = c;
  document.body.appendChild(b);
}
function OBcookieObj() {
  var a = {},
    c = OBgetCookie('obEmail');
  void 0 != c &&
    null != c &&
    ((a.obEmail = c),
    (a.obOrg = OBgetCookie('obOrg')),
    (a.obAccount = OBgetCookie('obAccount')),
    (a.obTimestamp = OBgetCookie('obTimeStamp')),
    (a.obAuthCode = OBgetCookie('obAuthCode')),
    (a.obFname = OBgetCookie('obFname')),
    (a.obLname = OBgetCookie('obLname')),
    (a.obIsCampaign = OBgetCookie('obIsCampaign')));
  return a;
}
function OBLaunch() {
  var a = getParameterByName('utm_org'),
    c = getParameterByName('utm_email'),
    b = getParameterByName('utm_campaign');
  if (a && '' !== a && c && '' !== c && b && '' !== b) {
    var d = getParameterByName('utm_firstname'),
      f = getParameterByName('utm_lastname');
    setCookie('obOrg', a, 1);
    setCookie('obEmail', c, 1);
    a = getParameterByName('utm_custname');
    setCookie('obAccount', a ? a : b, 1);
    setCookie('obFname', d, 1);
    setCookie('obLname', f, 1);
    setCookie('obIsCampaign', !0, 1);
    b = window.location.href.indexOf('?');
    window.location = window.location.href.substr(0, b);
  }
  var e =
    'undefined' == typeof window.onboardify || OBgetCookie('obIsCampaign')
      ? OBcookieObj()
      : window.onboardify;
  e.obEmail &&
    OBloadScript(
      'https://webapp.onboardify.com/js/apploader.min.js',
      function() {
        OBvalidateLaunch(e);
      }
    );
}
document.addEventListener(
  'readystatechange',
  function() {
    'complete' == document.readyState && OBLaunch();
  },
  !1
);
function getParameterByName(a) {
  a = a.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  a = new RegExp('[\\?&]' + a + '=([^&#]*)').exec(location.href);
  return null == a ? '' : decodeURIComponent(a[1].replace(/\+/g, ' '));
}
function setCookie(a, c, b) {
  var d = new Date();
  d.setTime(d.getTime() + 864e5 * b);
  b = 'expires=' + d.toUTCString();
  document.cookie = a + '=' + c + ';' + b + ';path=/';
}
