import {
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef,
  OnDestroy,
  OnInit
} from '@angular/core';
import { CoreService } from '../services/core.service';
import { Subscription } from 'rxjs';

@Directive({ selector: '[appHasRole]' })
export class RoleDirective implements OnInit, OnDestroy {
  private authorities: string[];
  private obs: Subscription;
  constructor(
    private core: CoreService,
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) {}

  @Input()
  set appHasRole(value: string | string[]) {
    this.authorities =
      typeof value === 'string' ? [<string>value] : <string[]>value;
  }

  private _packageName: string;
  @Input('appHasRolePackage')
  set appHasRolePackage(value: string) {
    this._packageName = value;
  }

  ngOnInit() {
    this.obs = this.core.user.subscribe(_ => {
      if (this.core.checkAuthorization(this.authorities)) {
        if (this._packageName) {
          if (this.checkPackage()) {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
          } else {
            this.viewContainerRef.clear();
          }
        } else {
          this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
      } else {
        this.viewContainerRef.clear();
      }
    });
  }

  checkPackage() {
    if (this.core.modules[this._packageName] === true) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy() {
    this.obs.unsubscribe();
  }
}
