import { Component } from '@angular/core';
import { CoreService } from '../../services/core.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {
  constructor(public core: CoreService) {}
}
