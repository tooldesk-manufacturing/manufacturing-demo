import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { shareReplay, switchMap, take } from 'rxjs/operators';
import {
  Bill,
  BillingDetails,
  CustomerSummary,
  Departments,
  Packages,
  SubvendorSummary,
  User,
  WorkerSummary,
  DueDate,
  UsersLimit,
  Receipt,
  SearchOverride
} from '../models/models';
import { SpinnerOverlayService } from '../spinner/spinner-overlay/spinner-overlay.service';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  user: Observable<User>;
  userRef: firebase.User;
  userCustomClaims: firebase.auth.IdTokenResult;
  modules: Packages = {};
  subvendors: Observable<SubvendorSummary[]>;
  customers: Observable<CustomerSummary[]>;
  workers: Observable<WorkerSummary[]>;
  departments: Observable<Departments>;
  billingDetails: Observable<BillingDetails>;
  userLimit: Observable<UsersLimit>;
  dueDate: DueDate;

  initialURL: string;
  URLParams: any;

  pastDueDate = false;
  verificationReminder = false;
  dueDateMessage = '';
  currentBill: Bill;
  currentReceipt: Receipt;
  loadingText = 'ToolDesk';
  resetOrders = false;

  receiptSearchOverrride: SearchOverride = null;
  billingSearchOverrride: SearchOverride = null;
  transactionSearchOverrride: SearchOverride = null;
  managementFilterOverride: SearchOverride = null;
  orderSearchOverride: SearchOverride = null;

  constructor(
    private afAuth: AngularFireAuth,
    private afStore: AngularFirestore,
    private router: Router,
    private snackBar: MatSnackBar,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {
    this.checkDueDate();
    this.checkPackages();
    this.initUser();
    this.initData();
  }

  private async checkPackages() {
    try {
      this.modules = await this.afStore
        .collection('meta')
        .doc<Packages>('modules')
        .valueChanges()
        .pipe(take(1))
        .toPromise();
      if (this.userRef) {
        if (this.initialURL) {
          if (this.URLParams !== undefined) {
            await this.router.navigate([this.initialURL], {
              queryParams: this.URLParams
            });
          } else {
            if (
              this.initialURL === '/account' ||
              this.initialURL === '/noaccess'
            ) {
              await this.router.navigate(['order']);
            } else {
              await this.router.navigate([this.initialURL]);
            }
          }
        } else {
          await this.router.navigate(['order']);
        }
        this.toaster.success('Successful!', 'Login');
        this.spinner.hide();
      } else {
        await this.router.navigate(['account']);
        this.spinner.hide();
      }
    } catch (reason) {
      this.toaster.error(reason, 'Retrieving packages');
      this.spinner.hide();
    }
  }

  private initUser() {
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          this.userRef = <firebase.User>user;
          this.userRef.getIdTokenResult().then(result => {
            this.userCustomClaims = result;
          });
          return this.afStore
            .doc<User>(`users/${this.userRef.uid}`)
            .valueChanges()
            .pipe(shareReplay(1));
        } else {
          return of(null);
        }
      }),
      shareReplay(1)
    );
  }

  private initData() {
    this.subvendors = this.afStore
      .collection<SubvendorSummary>('subvendorSummary')
      .valueChanges()
      .pipe(shareReplay(1));
    this.customers = this.afStore
      .collection<CustomerSummary>('customerSummary')
      .valueChanges()
      .pipe(shareReplay(1));
    this.workers = this.afStore
      .collection<WorkerSummary>('workerSummary')
      .valueChanges()
      .pipe(shareReplay(1));
    this.departments = this.afStore
      .collection('meta')
      .doc<Departments>('departments')
      .valueChanges()
      .pipe(shareReplay(1));
    this.billingDetails = this.afStore
      .collection('meta')
      .doc<BillingDetails>('billingDetails')
      .valueChanges()
      .pipe(shareReplay(1));
    this.userLimit = this.afStore
      .collection('meta')
      .doc<UsersLimit>('usersLimit')
      .valueChanges()
      .pipe(shareReplay(1));
  }

  private async checkDueDate() {
    this.dueDate = await this.afStore
      .collection('meta')
      .doc<DueDate>('dueDate')
      .valueChanges()
      .pipe(take(1))
      .toPromise();
    if (
      this.dueDate === undefined ||
      this.dueDate.date === undefined ||
      this.dueDate.month === undefined ||
      this.dueDate.year === undefined
    ) {
      this.dueDateMessage =
        'Unable to connect to our servers. Check your internet connection.';
      this.pastDueDate = true;
      this.router.navigate(['/noaccess']);
    } else {
      const today = new Date();
      const due = new Date(
        +this.dueDate.year,
        +this.dueDate.month - 1,
        +this.dueDate.date
      );
      const diff = Math.ceil(
        (due.getTime() - today.getTime()) / (1000 * 60 * 60 * 24)
      );
      if (diff === 0) {
        this.dueDateMessage =
          'Today is your billing date.You will have 3 days to renew your subscription.';
      } else if (diff > 0 && diff <= 3) {
        this.dueDateMessage =
          'You have ' + diff + ' days till your next billing date.';
      } else if (diff === -1) {
        this.dueDateMessage = 'You have 2 days to renew your subscription.';
      } else if (diff === -2) {
        this.dueDateMessage = 'This is the last day to renew your subscription';
      } else if (diff <= -3) {
        this.dueDateMessage =
          'Your subscription period has ended. Please renew to continue.';
        this.pastDueDate = true;
        this.router.navigate(['/noaccess']);
      }
    }
    if (this.dueDateMessage !== '') {
      this.snackBar.open(this.dueDateMessage, 'CLOSE');
      this.dueDateMessage = '';
    }
  }

  checkAuthorization(allowedRoles: string[]) {
    if (!this.userRef) {
      return false;
    }
    for (const role of allowedRoles) {
      if (this.userCustomClaims.claims[role] === true) {
        return true;
      }
    }
    return false;
  }

  async logout() {
    await this.afAuth.auth.signOut();
    await this.router.navigate(['account']);
    window.location.reload();
  }
}
