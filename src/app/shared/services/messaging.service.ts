import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import '@firebase/messaging';

@Injectable()
export class MessagingService {
  messaging = firebase.messaging();
  /*   currentMessage = new BehaviorSubject(null); */
  constructor(
    private afStore: AngularFirestore,
    private fireAuth: AngularFireAuth
  ) {}

  private updateToken(token) {
    this.fireAuth.authState.subscribe(user => {
      if (!user) {
        return;
      }
      this.afStore
        .collection('fcmTokens')
        .doc(user.uid)
        .set({ token: token });
    });
  }

  getPermission() {
    this.messaging
      .requestPermission()
      .then(() => {
        return this.messaging.getToken();
      })
      .then(token => {
        this.updateToken(token);
      });
  }

  /*   receiveMessage() {
    this.messaging.onMessage(payload => {
      this.currentMessage.next(payload);
    });
  } */
}
