import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/functions';

@Injectable()
export class FunctionsService {
  constructor(private afFunctions: AngularFireFunctions) {}

  callFunction(name: string, data: any): Promise<void> {
    const firebaseFunctions = this.afFunctions.httpsCallable(name);
    return firebaseFunctions({ data }).toPromise();
  }
}
