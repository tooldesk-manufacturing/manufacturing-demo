import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable()
export class StorageService {
  constructor(private afStorage: AngularFireStorage) {}

  async uploadFile(
    collection: string,
    name: string,
    file: File
  ): Promise<void> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStorage.ref(`${collection}/${name}`).put(file);
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  getFileURL(collection: string, name: string): Promise<any> {
    const storageRef = this.afStorage.ref(`${collection}/${name}`);
    return storageRef.getDownloadURL().toPromise();
  }
}
