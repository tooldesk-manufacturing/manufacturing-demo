import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  Query,
  AngularFirestoreCollection
} from '@angular/fire/firestore';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FirestoreQuery, BatchWrite } from '../../models/models';

@Injectable()
export class FirestoreService {
  constructor(private afStore: AngularFirestore) {}

  getNewDocumentID(): string {
    return this.afStore.createId();
  }

  getDocument<T>(collection: string, uid: string): Observable<T> {
    return this.afStore
      .collection(collection)
      .doc<T>(uid)
      .valueChanges()
      .pipe(shareReplay(1));
  }

  getDocumentRef<T>(
    collection: string,
    uid: string
  ): firebase.firestore.DocumentReference {
    return this.afStore.collection(collection).doc<T>(uid).ref;
  }

  setDocument<T>(collection: string, uid: string, data: T): Promise<void> {
    return this.afStore
      .collection(collection)
      .doc(uid)
      .set(data, { merge: true });
  }

  getCollectionRef<T>(
    collection: string,
    params: FirestoreQuery
  ): AngularFirestoreCollection<T> {
    return this.afStore.collection<T>(collection, ref => {
      let query: Query = ref;
      if (params) {
        if (params.orderBy) {
          query = query.orderBy(params.orderBy.path, params.orderBy.direction);
        }
        if (params.conditions) {
          params.conditions.forEach(quer => {
            query = query.where(quer.path, quer.op, quer.value);
          });
        }
        if (params.limit) {
          query = query.limit(params.limit);
        }
        if (params.startAfter) {
          query = query.startAfter(params.startAfter);
        }
      }
      return query;
    });
  }

  getCollection<T>(
    collection: string,
    params: FirestoreQuery
  ): Observable<T[]> {
    return this.afStore
      .collection<T>(collection, ref => {
        let query: Query = ref;
        if (params) {
          if (params.orderBy) {
            query = query.orderBy(
              params.orderBy.path,
              params.orderBy.direction
            );
          }
          if (params.conditions) {
            params.conditions.forEach(quer => {
              query = query.where(quer.path, quer.op, quer.value);
            });
          }
          if (params.limit) {
            query = query.limit(params.limit);
          }
          if (params.startAfter) {
            query = query.startAfter(params.startAfter);
          }
        }
        return query;
      })
      .valueChanges()
      .pipe(shareReplay(1));
  }

  batchWrite(batchWrites: BatchWrite[]): Promise<void> {
    const batch = this.afStore.firestore.batch();
    batchWrites.forEach(write => {
      switch (write.type) {
        case 'set':
          batch.set(write.docRef, write.setData, write.options);
          break;
        case 'delete':
          batch.delete(write.docRef);
          break;
        case 'update':
          batch.update(write.docRef, write.updateData);
      }
    });
    return batch.commit();
  }
}
