import {
  Timestamp,
  OrderByDirection,
  WhereFilterOp
} from '@firebase/firestore-types';
export interface Order {
  id: string;
  createdAt: Timestamp;
  customerDC?: string;
  customerName: string;
  customerUID: string;
  internalDC: string;
  orderPice?: number;
  estimatedFinishDate?: Date;
  items?: OrderItem[];
  status: string;
  currentSubProcessSubvendorUID?: string[];
  containsMultipleItemBill?: boolean;
}

export interface TimelineItem {
  description: string;
  status: string;
  dateTime?: Date;
}

export interface Transaction {
  type: string;
  id: string;
  cash?: boolean;
  amount: number;
  description?: string;
  billNo?: string;
  orderUID?: string[];
  orderItemIndex?: number | number[];
  customerName?: string;
  customerUID?: string;
  customerDC?: string[];
  internalDC?: string[];
  createdAt: Timestamp;
  paymentReceived?: boolean;
  receivedAt?: Timestamp;
  subvendorUID?: string;
  subvendorName?: string;
}

export interface OrderItemSummary {
  quantity: number;
  name?: string;
  weight?: number;
  price?: number;
  hsn?: string;
  orderUID?: string;
  itemIndex: number;
}

export interface MultipleBillingOrderItemSummary {
  name?: string;
  quantity: number;
  orderUID: string;
  itemIndex: number;
  customerDC?: string;
  weight?: number;
}

export interface Bill {
  billNo: string;
  id: string;
  cash?: boolean;
  netAmount: number;
  grossAmount: number;
  billWeight?: boolean;
  orderUID: string[];
  internalDC: string[];
  customerDC: string[];
  igst?: boolean;
  gstPercentage?: number;
  orderCreatedDate?: Timestamp;
  items: OrderItemSummary[];
  createdAt: Timestamp;
  companyDetails?: BillingDetails;
  customerDetails?: BillingDetails;
  customerName?: string;
  customerGSTIN?: string;
  customerUID: string;
  complete?: boolean;
  multipleOrders?: boolean;
  refPONo?: string;
  refDate?: string;
  eSugam?: string;
  gstRef?: string;
  dispatchMode?: string;
  vehicleNo?: string;
  invoiceRemarks?: string;
}

export interface BillingDetails {
  id?: string;
  name: string;
  companyLogo?: string;
  email: string;
  address: string;
  phoneNumber: string;
  phoneNumber2?: string;
  gstin: string;
  stateCode: string;
  HSNCode?: string;
  SACCode?: string;
  NEFT?: string;
}

export interface SubProcess {
  internalDC?: string;
  orderUID?: string;
  orderItemIndex?: number;
  cash?: boolean;
  subProcessType: string;
  receivedAt: Date;
  description?: string;
  subProcessPrice?: number;
  sentAt?: Date;
  subvendorName?: string;
  subvendorUID?: string;
  departmentName?: string;
}

export interface DueDate {
  date: string;
  month: string;
  year: string;
}

export interface IndividualUserLimit {
  current: number;
  max?: number;
  infinite: boolean;
}

export interface UsersLimit {
  admin: IndividualUserLimit;
  owner: IndividualUserLimit;
  customer: IndividualUserLimit;
  receptionist: IndividualUserLimit;
  worker: IndividualUserLimit;
  inspector: IndividualUserLimit;
  subvendor: IndividualUserLimit;
}

export interface Receipt {
  id: string;
  dcNo: string;
  orderUID?: string;
  internalDC: string;
  customerDC: string;
  orderCreatedDate: Timestamp;
  items: OrderItemSummary[];
  createdAt: Timestamp;
  companyDetails?: BillingDetails;
  customerDetails?: BillingDetails;
  customerUID: string;
  refPONo?: string;
  eSugam?: string;
  gstRef?: string;
  refDate?: string;
  dispatchMode?: string;
  vehicleNo?: string;
  dcRemarks?: string;
}

export interface ClaimStatus {
  name: string;
  claimed: boolean;
}

export interface OrderItem {
  id?: string;
  name?: string;
  weight?: number;
  quantity: number;
  images?: string[];
  remark?: string;
  price?: number;
  hsn?: string;
  currentSubProcess?: SubProcess;
  subProcesses?: SubProcess[];
  timeline: TimelineItem[];
  status: string;
  claimed: ClaimStatus;
  progress?: number;
}

export interface Department {
  name: string;
}

export interface Departments {
  list: Department[];
}

export interface Packages {
  basic?: boolean;
  dashboard?: boolean;
}

export interface WorkerSummary {
  uid: string;
  name: string;
  presentThisMonth: number;
  type?: string;
}

export interface CustomerSummary {
  uid: string;
  name: string;
  creditAmount: number;
}

export interface SubvendorSummary {
  uid: string;
  name: string;
  amountDue: number;
}

export interface Roles {
  admin?: boolean;
  worker?: boolean;
  customer?: boolean;
  owner?: boolean;
  receptionist?: boolean;
  subvendor?: boolean;
  inspector?: boolean;
}
export interface User {
  uid: string;
  displayName: string;
  email: string;
  roles: Roles;
  emailVerified: boolean;
  department?: Department[];
  salary?: number;
  address?: string;
  photoURL?: string;
  phoneNumber?: string;
  landlineNumber?: string;
  gstin?: string;
  tin?: string;
  credit?: boolean;
  esugam?: string;
  stateCode?: string;
  hsnCode?: string;
  SACCode?: string;
}

export interface SearchOverride {
  searchField: string;
  searchTerm: string | Date;
}

export interface FirestoreQuery {
  orderBy?: {
    path: string;
    direction: OrderByDirection;
  };
  limit?: number;
  startAfter?: any;
  conditions?: {
    path: string;
    op: WhereFilterOp;
    value: any;
  }[];
}

export interface BatchWrite {
  type: string;
  docRef: firebase.firestore.DocumentReference;
  setData?: firebase.firestore.DocumentData;
  updateData?: firebase.firestore.UpdateData;
  options?: firebase.firestore.SetOptions;
}
