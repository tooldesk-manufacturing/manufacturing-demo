import { AbstractControl } from '@angular/forms';

export function dateLessThanValidator(control: AbstractControl) {
  const today = new Date();
  if (!control.value) {
    return null;
  } else {
    if (today > control.value) {
      return { dateRange: true };
    }
  }
}
