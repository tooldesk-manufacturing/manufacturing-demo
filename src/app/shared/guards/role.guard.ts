import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  CanLoad,
  Route,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { CoreService } from '../services/core.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate, CanLoad {
  constructor(private core: CoreService, private router: Router) {}

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    if (this.core.checkAuthorization(route.data['expectedRoles'])) {
      return true;
    } else {
      this.router.navigate(['order']);
      return false;
    }
  }

  canActivate(
    next: ActivatedRouteSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.core.checkAuthorization(next.data['expectedRoles'])) {
      return true;
    } else {
      this.router.navigate(['order']);
      return false;
    }
  }
}
