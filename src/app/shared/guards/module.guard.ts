import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router,
  CanLoad,
  Route,
  UrlSegment
} from '@angular/router';
import { Observable } from 'rxjs';
import { CoreService } from '../services/core.service';

@Injectable({
  providedIn: 'root'
})
export class ModuleGuard implements CanActivate, CanLoad {
  constructor(private core: CoreService, private router: Router) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    let url = '';
    segments.forEach(segment => {
      url += '/' + segment.path;
    });
    this.core.initialURL = url;
    if (
      this.core.modules[route.data['moduleName']] === true &&
      !this.core.pastDueDate
    ) {
      return true;
    } else {
      this.router.navigate(['noaccess']);
      return false;
    }
  }

  canActivate(
    next: ActivatedRouteSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (
      this.core.modules[next.data['moduleName']] === true &&
      !this.core.pastDueDate
    ) {
      return true;
    } else {
      this.router.navigate(['noaccess']);
      return false;
    }
  }
}
