import { NgModule } from '@angular/core';
import { NgArrayPipesModule, NgMathPipesModule, NgStringPipesModule } from 'ngx-pipes';

@NgModule({
  imports: [NgArrayPipesModule, NgMathPipesModule, NgStringPipesModule],
  exports: [NgArrayPipesModule, NgMathPipesModule, NgStringPipesModule]
})
export class NgxPipesModule {}
