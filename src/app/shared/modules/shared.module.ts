import { NgModule } from '@angular/core';
import { RoleDirective } from '../directives/role.directive';
import { NumberToWordsPipe } from '../pipes/number-to-words.pipe';
import { HumanizePipe } from '../pipes/humanize.pipe';
import { INRCurrencyPipe } from '../pipes/inr-currency.pipe';

@NgModule({
  declarations: [
    RoleDirective,
    NumberToWordsPipe,
    HumanizePipe,
    INRCurrencyPipe
  ],
  exports: [RoleDirective, NumberToWordsPipe, HumanizePipe, INRCurrencyPipe]
})
export class SharedModule {}
