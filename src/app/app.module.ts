import { BrowserModule } from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';

import { MaterialModule } from './shared/modules/material.module';
import { FireModules } from './shared/modules/fire.module';
import { SharedModule } from './shared/modules/shared.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { NoaccessComponent } from './shared/noaccess/noaccess.component';

import { MessagingService } from './shared/services/messaging.service';
import { CoreService } from './shared/services/core.service';

import { NgxPipesModule } from './shared/modules/ngx-pipes.module';
import { ToastrModule } from 'ngx-toastr';
import { SlideshowModule } from 'ng-simple-slideshow';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { NotyfToastComponent } from './shared/components/notyf.toast';
import { UpdateService } from './shared/services/update-service.service';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { AppNavComponent } from './shared/layout/app-nav/app-nav.component';
import { AppFooterComponent } from './shared/layout/app-footer/app-footer.component';
import { BottomSheetComponent } from './shared/layout/app-footer/bottom-sheet/bottom-sheet.component';
import { SpinnerOverlayComponent } from './shared/spinner/spinner-overlay/spinner-overlay.component';
import { SpinnerOverlayService } from './shared/spinner/spinner-overlay/spinner-overlay.service';
import { SpinnerComponent } from './shared/spinner/spinner/spinner.component';
import { Location } from '@angular/common';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    AppNavComponent,
    AppFooterComponent,
    BottomSheetComponent,
    NoaccessComponent,
    NotyfToastComponent,
    SpinnerOverlayComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    HttpClientModule,
    MaterialModule,
    NgxPipesModule,
    SlideshowModule,
    LoadingBarRouterModule,
    LoadingBarModule,
    PerfectScrollbarModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      positionClass: 'toast-bottom-left',
      toastComponent: NotyfToastComponent
    }),
    SharedModule,
    AngularFireModule.initializeApp(environment.firebase),
    FireModules,
    ServiceWorkerModule.register('/combined-worker.js', {
      enabled: environment.production
    })
  ],
  entryComponents: [
    BottomSheetComponent,
    NotyfToastComponent,
    SpinnerOverlayComponent
  ],
  providers: [
    MessagingService,
    CoreService,
    SpinnerOverlayService,
    Location,
    UpdateService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
