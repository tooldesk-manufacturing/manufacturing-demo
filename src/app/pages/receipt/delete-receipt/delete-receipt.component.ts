import { Component, Inject, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { Bill, Receipt } from 'src/app/shared/models/models';
import { ReceiptService } from '../receipt.service';

@Component({
  selector: 'app-delete-receipt',
  templateUrl: './delete-receipt.component.html',
  styleUrls: ['./delete-receipt.component.scss']
})
export class DeleteReceiptComponent implements OnInit {
  receipt: Receipt;
  enterPINForm: FormGroup = new FormGroup({
    currentPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<DeleteReceiptComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private receiptService: ReceiptService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.receipt = this.data;
  }

  get formControls() {
    return this.enterPINForm.controls;
  }

  async deleteReceipt(receipt: Receipt, pin: string) {
    this.core.loadingText = 'Deleting delivery-challan...';
    this.spinner.show();
    try {
      await this.receiptService.deleteReceipt(receipt, pin);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Delivery-challan deleted');
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Deleting delivery-challan');
      this.dialogRef.close();
    }
  }
}
