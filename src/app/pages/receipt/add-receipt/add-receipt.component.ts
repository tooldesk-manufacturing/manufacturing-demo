import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ReceiptService } from '../receipt.service';

import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Receipt } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { take } from 'rxjs/operators';
import { BillingService } from '../../billing/billing.service';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-add-receipt',
  templateUrl: './add-receipt.component.html',
  styleUrls: ['./add-receipt.component.scss']
})
export class AddReceiptComponent implements OnInit, OnDestroy {
  billService: BillingService;
  receipt: Receipt;
  Math = Math;
  today = Date.now();

  constructor(
    private core: CoreService,
    private receiptService: ReceiptService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initData();
  }

  async initData() {
    this.receipt = this.core.currentReceipt;
    this.receipt.items.forEach(item => {
      if (item.weight === undefined) {
        item.weight = 0;
      }
      if (item.hsn === undefined) {
        item.hsn = '';
      }
    });
    this.receipt.companyDetails = await this.core.billingDetails
      .pipe(take(1))
      .toPromise();
    const customerDetails = await this.receiptService
      .getCustomerDetails(this.receipt.customerUID)
      .pipe(take(1))
      .toPromise();
    this.receipt.customerDetails = {
      name: customerDetails.displayName ? customerDetails.displayName : '',
      email: customerDetails.email ? customerDetails.email : '',
      address: customerDetails.address ? customerDetails.address : '',
      phoneNumber: customerDetails.phoneNumber
        ? customerDetails.phoneNumber
        : '',
      phoneNumber2: customerDetails.landlineNumber
        ? customerDetails.landlineNumber
        : '',
      gstin: customerDetails.gstin ? customerDetails.gstin : '',
      HSNCode: customerDetails.hsnCode ? customerDetails.hsnCode : '',
      stateCode: customerDetails.stateCode ? customerDetails.stateCode : '',
      SACCode: customerDetails.SACCode ? customerDetails.SACCode : ''
    };
    this.spinner.hide();
  }

  isDate(val: any) {
    return val instanceof Date;
  }

  async generateReceipt() {
    this.core.loadingText = 'Generating Receipt...';
    this.spinner.show();
    try {
      await this.receiptService.generateReceipt(this.receipt);
      this.router.navigate(['delivery-challan']);
      this.spinner.hide();
      this.toaster.success('Success', 'Created receipt!');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Receipt creation');
    }
  }

  ngOnDestroy() {
    this.core.currentReceipt = null;
  }
}
