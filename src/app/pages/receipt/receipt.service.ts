import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  Receipt,
  User,
  CustomerSummary,
  Order
} from '../../shared/models/models';
import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFireFunctions } from '@angular/fire/functions';

@Injectable()
export class ReceiptService {
  constructor(
    private afFunctions: AngularFireFunctions,
    private afStore: AngularFirestore
  ) {}

  getReceipts(
    searchField: string,
    searchTerm: string | Date,
    fromDate?: Date,
    toDate?: Date
  ): Observable<Receipt[]> {
    if (searchField === 'createdAt') {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Receipt>('receipts', ref =>
          ref
            .where(searchField, '>', yesterday)
            .where(searchField, '<', tomorrow)
            .orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField === 'customerUID' && fromDate && toDate) {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Receipt>('receipts', ref =>
          ref
            .where(searchField, '==', searchTerm)
            .where('createdAt', '>', yesterday)
            .where('createdAt', '<', tomorrow)
            .orderBy('createdAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField !== 'customerUID') {
      return this.afStore
        .collection<Receipt>('receipts', ref =>
          ref.where(searchField, '==', searchTerm).orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    }
  }

  getCustomerSummary(uid: string): Observable<CustomerSummary> {
    return this.afStore
      .collection('customerSummary')
      .doc<CustomerSummary>(uid)
      .valueChanges();
  }

  getCustomerDetails(uid: string): Observable<User> {
    return this.afStore
      .collection('users')
      .doc<User>(uid)
      .valueChanges();
  }

  getOrder(uid: string): Observable<Order> {
    return this.afStore
      .collection('orders')
      .doc<Order>(uid)
      .valueChanges();
  }

  getReceipt(uid: string): Observable<Receipt> {
    return this.afStore
      .collection('receipts')
      .doc<Receipt>(uid)
      .valueChanges();
  }

  generateReceipt(dc: Receipt): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const dcRef = this.afStore.collection('receipts').doc(dc.id).ref;
        dc.companyDetails = null;
        await dcRef.set(dc);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  deleteReceipt(receipt: Receipt, pin: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const deleteReceiptFunction = this.afFunctions.httpsCallable(
          'deleteReceipt'
        );
        await deleteReceiptFunction({
          pin: pin,
          receiptUID: receipt.id
        }).toPromise();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }
}
