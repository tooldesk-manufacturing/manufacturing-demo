import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleGuard } from 'src/app/shared/guards/role.guard';
import { AddReceiptComponent } from './add-receipt/add-receipt.component';
import { ReceiptComponent } from './receipt.component';

const routes: Routes = [
  {
    path: '',
    component: ReceiptComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'new',
    component: AddReceiptComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptRoutingModule {}
