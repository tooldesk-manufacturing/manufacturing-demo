import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ReceiptService } from './receipt.service';
import { ReceiptComponent } from './receipt.component';
import { ReceiptRoutingModule } from './receipt-routing.module';
import { AddReceiptComponent } from './add-receipt/add-receipt.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { NgxPipesModule } from 'src/app/shared/modules/ngx-pipes.module';
import { DeleteReceiptComponent } from './delete-receipt/delete-receipt.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ReceiptRoutingModule,
    PerfectScrollbarModule,
    NgxPipesModule
  ],
  declarations: [ReceiptComponent, AddReceiptComponent, DeleteReceiptComponent],
  entryComponents: [DeleteReceiptComponent],
  providers: [ReceiptService]
})
export class ReceiptModule {}
