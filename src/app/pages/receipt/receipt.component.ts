import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import {
  MatPaginator,
  MatSort,
  MatDialog,
  MatSnackBar
} from '@angular/material';
import { Subscription, Observable } from 'rxjs';
import { ReceiptService } from './receipt.service';
import { startWith, map, take } from 'rxjs/operators';
import { ReceiptDB, ReceiptDataSource } from './receipt.datasource';
import { LoadingBarService } from '@ngx-loading-bar/core';
import {
  CustomerSummary,
  Receipt,
  BillingDetails
} from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { dateCompareValidator } from 'src/app/shared/validators/date-compare.validator';
import { DeleteReceiptComponent } from './delete-receipt/delete-receipt.component';

import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss']
})
export class ReceiptComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  displayedColumns: string[];
  dataSource: ReceiptDataSource;
  DB: ReceiptDB;

  receipt: Receipt;

  billSum = 0;
  Math = Math;
  companyLogoDataURL = '';
  companyDetails: BillingDetails;

  customers: CustomerSummary[] = [];
  customerOptions: string[] = [];
  customerFilteredOptions: Observable<string[]>;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  searchDCForm: FormGroup = new FormGroup({
    searchTerm: new FormControl('', [Validators.required]),
    searchField: new FormControl('', [Validators.required]),
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });

  constructor(
    private receiptService: ReceiptService,
    private changeDetectorRefs: ChangeDetectorRef,
    private core: CoreService,
    private loadingService: LoadingBarService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.getRouteParams();
    this.getNames();
    this.initialDataSource();
    this.initForm();
    this.initBillingDetails();
  }

  private initForm() {
    const today = new Date();
    this.searchDCForm.controls['fromDate'].setValue(today);
    this.searchDCForm.controls['toDate'].setValue(today);
  }

  private getRouteParams() {
    if (
      this.core.URLParams &&
      !this.isEmpty(this.core.URLParams) &&
      this.core.initialURL.search('delivery-challan') > -1
    ) {
      this.core.receiptSearchOverrride = {
        searchField: this.core.URLParams['searchField'],
        searchTerm: this.core.URLParams['searchTerm']
      };
      this.core.URLParams = null;
      this.core.initialURL = null;
    }
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  private async initBillingDetails() {
    try {
      this.companyDetails = await this.core.billingDetails
        .pipe(take(1))
        .toPromise();
    } catch (reason) {}
  }

  private getNames() {
    this.subscriptions.push(
      this.core.customers.subscribe(res => {
        this.customers = res;
        this.customerOptions = res.map(customer => customer.name);
      })
    );
    this.customerFilteredOptions = this.searchDCForm
      .get('searchTerm')
      .valueChanges.pipe(
        startWith(''),
        map(val => this.filterCustomerNames(val))
      );
  }

  private filterCustomerNames(val: string): string[] {
    if (this.searchDCForm.controls['searchField'].value === 'createdAt') {
      return this.customerOptions;
    }
    return this.customerOptions.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  private selectDatasource(
    overrideSearchField?: string,
    overrideSearchTerm?: Date | string
  ) {
    if (!overrideSearchField || !overrideSearchTerm) {
      if (this.searchDCForm.controls['searchField'].value === 'customerUID') {
        overrideSearchTerm = this.customers.find(
          x => x.name === this.searchDCForm.controls['searchTerm'].value
        ).uid;
        this.DB = new ReceiptDB(
          this.receiptService,
          this.searchDCForm.controls['searchField'].value,
          overrideSearchTerm,
          this.searchDCForm.controls['fromDate'].value,
          this.searchDCForm.controls['toDate'].value
        );
      } else if (
        this.searchDCForm.controls['searchField'].value === 'createdAt'
      ) {
        this.DB = new ReceiptDB(
          this.receiptService,
          this.searchDCForm.controls['searchField'].value,
          null,
          this.searchDCForm.controls['fromDate'].value,
          this.searchDCForm.controls['toDate'].value
        );
      } else {
        this.DB = new ReceiptDB(
          this.receiptService,
          this.searchDCForm.controls['searchField'].value,
          this.searchDCForm.controls['searchTerm'].value
        );
      }
    } else {
      this.DB = new ReceiptDB(
        this.receiptService,
        overrideSearchField,
        overrideSearchTerm
      );
    }
    this.dataSource = new ReceiptDataSource(this.DB, this.paginator, this.sort);
  }

  initialDataSource() {
    if (this.core.receiptSearchOverrride) {
      this.changeDataSource(
        this.core.receiptSearchOverrride.searchField,
        this.core.receiptSearchOverrride.searchTerm
      );
      this.searchDCForm.controls['searchField'].setValue(
        this.core.receiptSearchOverrride.searchField
      );
      this.searchDCForm.controls['searchTerm'].setValue(
        this.core.receiptSearchOverrride.searchTerm
      );
      this.core.receiptSearchOverrride = null;
    } else {
      const today = new Date();
      this.searchDCForm.controls['searchField'].setValue('createdAt');
      this.searchDCForm.controls['searchTerm'].setValue('  ');
      this.searchDCForm.controls['fromDate'].setValue(today);
      this.searchDCForm.controls['toDate'].setValue(today);
      this.changeDataSource();
    }
  }

  get formControls() {
    return this.searchDCForm.controls;
  }

  clearForm() {
    this.searchDCForm.controls['searchTerm'].setValue('');
    if (this.searchDCForm.controls['searchField'].value === 'customerUID') {
      this.searchDCForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchDCForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else if (
      this.searchDCForm.controls['searchField'].value === 'createdAt'
    ) {
      this.searchDCForm.controls['searchTerm'].setValue('  ');
      this.searchDCForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchDCForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else {
      this.searchDCForm.controls['fromDate'].setValidators([]);
      this.searchDCForm.controls['toDate'].setValidators([]);
    }
    this.searchDCForm.updateValueAndValidity();
  }

  checkCustomerName() {
    const val = this.searchDCForm.controls['searchTerm'].value;
    if (!this.customerOptions.includes(val)) {
      this.searchDCForm.controls['searchTerm'].setErrors({
        inList: true
      });
      this.searchDCForm.updateValueAndValidity();
    }
  }

  changeDataSource(
    overrideSearchField?: string,
    overrideSearchTerm?: Date | string
  ) {
    this.loadingService.start();
    this.displayedColumns = ['createdAt', 'dcNo', 'details'];
    this.selectDatasource(overrideSearchField, overrideSearchTerm);
    this.changeDetectorRefs.detectChanges();
    this.loadingService.stop();
  }

  async openReceipt(receipt: Receipt, option: string) {
    try {
      this.receipt = receipt;
      this.receipt.companyDetails = this.companyDetails;
      this.printPDF(option);
      this.receipt = null;
    } catch (reason) {
      this.receipt = null;
    }
  }

  isDate(val: any) {
    return val instanceof Date;
  }

  private getString(
    value: string | string[] | number,
    optional?: string
  ): string | string[] | number {
    if (value && optional) {
      return value + optional;
    } else if (value) {
      return value;
    } else {
      return '';
    }
  }

  private getBase64Image(img) {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, 60, 60);
    const dataURL = canvas.toDataURL('image/png');
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
  }

  onImageLoad() {
    this.companyLogoDataURL = this.getBase64Image(
      document.getElementById('companyLogo')
    );
  }

  copyToClipboard(message: string) {
    const el = document.createElement('textarea');
    el.value = message;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
      document.getSelection().rangeCount > 0
        ? document.getSelection().getRangeAt(0)
        : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
      document.getSelection().removeAllRanges();
      document.getSelection().addRange(selected);
    }
    this.snackbar.open('Receipt No copied to clipboard', 'CLOSE', {
      duration: 1000
    });
  }

  printPDF(option: string) {
    const doc = new jsPDF();
    doc.setFontSize(12);
    doc.setFont('helvetica', 'bold');
    doc.text(this.getString(this.receipt.companyDetails.name), 28, 7);
    if (this.receipt.companyDetails.companyLogo && this.companyLogoDataURL) {
      doc.addImage(this.companyLogoDataURL, 'PNG', 4, 7, 90, 90);
    }
    doc.text('DELIVERY CHALLAN', 163, 7);
    doc.setFont('helvetica', 'normal');
    doc.setFontSize(8);
    const companyDetails = doc.splitTextToSize(
      this.getString(this.receipt.companyDetails.address) +
        '\n' +
        'Contact: ' +
        this.getString(this.receipt.companyDetails.phoneNumber, ',') +
        this.getString(this.receipt.companyDetails.phoneNumber2, ',') +
        this.getString(this.receipt.companyDetails.email) +
        '\n' +
        'State Code: ' +
        this.getString(this.receipt.companyDetails.stateCode) +
        '\n' +
        'GSTIN: ' +
        this.getString(this.receipt.companyDetails.gstin) +
        '\n' +
        'SAC Code: ' +
        this.getString(this.receipt.companyDetails.SACCode),
      100
    );
    doc.text(companyDetails, 28, 12);
    doc.setLineWidth(0.25);
    doc.line(2, 34, 208, 34);
    const customerDetails = doc.splitTextToSize(
      'Details of receiver(Bill to):' +
        '\n' +
        this.getString(this.receipt.customerDetails.name) +
        '\n' +
        this.getString(this.receipt.customerDetails.address, '\n') +
        'Contact: ' +
        this.getString(this.receipt.customerDetails.phoneNumber, ',') +
        this.getString(this.receipt.customerDetails.phoneNumber2) +
        '\n' +
        'State Code: ' +
        this.getString(this.receipt.customerDetails.stateCode) +
        '\n' +
        'GSTIN: ' +
        this.getString(this.receipt.customerDetails.gstin) +
        '\n' +
        'SAC Code: ' +
        this.getString(this.receipt.customerDetails.SACCode) +
        '\n' +
        'Your DC No: ' +
        this.getString(this.receipt.customerDC) +
        '\n' +
        'Your DC Date: ' +
        this.receipt.createdAt.toDate().toLocaleDateString('en-IN') +
        '\n',
      90
    );
    doc.text(customerDetails, 4, 38);
    const billDetails = doc.splitTextToSize(
      'DC No: ' +
        this.getString(this.receipt.dcNo) +
        '\n' +
        'DC Date: ' +
        this.receipt.createdAt.toDate().toLocaleDateString('en-IN') +
        '\n' +
        'Ref PO No: ' +
        this.getString(this.receipt.refPONo) +
        '\n' +
        'Ref Date: ' +
        this.getString(this.receipt.refDate) +
        '\n' +
        'Our DC No: ' +
        this.getString(this.receipt.internalDC) +
        '\n' +
        'Our DC Date: ' +
        this.receipt.orderCreatedDate.toDate().toLocaleDateString('en-IN') +
        '\n' +
        'E-Sugam Details: ' +
        this.getString(this.receipt.eSugam) +
        '\n' +
        'DC Ref No(GST): ' +
        this.getString(this.receipt.gstRef) +
        '\n' +
        'Mode of dispatch: ' +
        this.getString(this.receipt.dispatchMode) +
        '\n' +
        'Vehicle No: ' +
        this.getString(this.receipt.vehicleNo),
      90
    );
    doc.text(billDetails, 115, 38);
    const itemTableHeaders = [
      ['#', 'Desription', 'HSN', 'Quantity(Nos)', 'Weight(Kgs)']
    ];
    const items = [];
    for (let i = 0; i < this.receipt.items.length; i++) {
      items.push([
        i + 1,
        this.getString(this.receipt.items[i].name),
        this.getString(this.receipt.items[i].hsn),
        this.getString(this.receipt.items[i].quantity),
        this.getString(this.receipt.items[i].weight)
      ]);
    }
    (doc as any).autoTable({
      theme: 'striped',
      startY: 70.25,
      styles: {
        font: 'helvetica',
        halign: 'center',
        valign: 'middle',
        fontSize: 10
      },
      headStyles: { overflow: 'linebreak', fontSize: 8 },
      columnStyles: {
        0: { cellWidth: 10 },
        1: { cellWidth: 45 },
        2: { cellWidth: 45 },
        3: { cellWidth: 25 },
        4: { cellWidth: 20 },
        5: { cellWidth: 20 },
        6: { cellWidth: 20, halign: 'right' }
      },
      bodyStyles: {
        fontStyle: 'bold'
      },
      tableWidth: 205.2,
      margin: { top: 2, left: 2.3, bottom: 40 },
      head: itemTableHeaders,
      body: items
    });
    doc.setFontSize(8);
    const footerTable = [
      ['DC Remarks', this.receipt.dcRemarks, ''],
      ['', '', 'For ' + this.getString(this.receipt.companyDetails.name)]
    ];
    (doc as any).autoTable({
      theme: 'striped',
      startY: 260,
      styles: {
        font: 'helvetica',
        halign: 'left',
        valign: 'middle'
      },
      columnStyles: {
        0: { cellWidth: 20 },
        1: { cellWidth: 30 },
        2: { cellWidth: 50, halign: 'right' }
      },
      bodyStyles: {
        cellPadding: 0.5
      },
      tableWidth: 205.2,
      margin: { left: 2.3 },
      body: footerTable
    });

    doc.setFontSize(10);
    doc.text('Receiver\'s Seal and Signature', 3, 294);
    doc.text('Authorised Signature', 174, 294);
    doc.setLineWidth(0.5);
    for (let i = 1; i <= doc.internal.getNumberOfPages(); i++) {
      doc.setPage(i);
      doc.setDrawColor(0);
      doc.rect(2, 2, 206, 293);
    }
    doc.setProperties({
      title: this.receipt.dcNo,
      subject: 'Delivery Challan',
      author: this.core.userRef.displayName,
      creator: this.receipt.companyDetails.name
    });
    if (option === 'view') {
      doc.output('dataurlnewwindow', { title: this.receipt.dcNo });
    } else if (option === 'download') {
      doc.save(this.receipt.dcNo);
    } else if (option === 'print') {
      doc.autoPrint();
      doc.output('dataurlnewwindow', { title: this.receipt.dcNo });
    }
  }

  deleteReceipt(receipt: Receipt) {
    this.dialog.open(DeleteReceiptComponent, {
      data: receipt
    });
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
