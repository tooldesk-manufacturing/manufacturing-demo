import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';

import { ReceiptService } from './receipt.service';
import { Timestamp } from '@firebase/firestore-types';
import { Receipt } from 'src/app/shared/models/models';

export class ReceiptDB {
  receiptObs: Subscription;
  dataChange: BehaviorSubject<Receipt[]> = new BehaviorSubject<Receipt[]>([]);
  get data(): Receipt[] {
    return this.dataChange.value;
  }
  constructor(
    private receiptService: ReceiptService,
    private searchField: string,
    private searchTerm: string | Date,
    private fromDate?: Date,
    private toDate?: Date
  ) {
    this.receiptObs = this.receiptService
      .getReceipts(
        this.searchField,
        this.searchTerm,
        this.fromDate,
        this.toDate
      )
      .subscribe(dc => {
        this.dataChange.next(dc);
      });
  }
}

export class ReceiptDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Receipt[] = [];
  renderedData: Receipt[] = [];

  constructor(
    private receiptDB: ReceiptDB,
    private _paginator: MatPaginator,
    private _sort: MatSort
  ) {
    super();

    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<Receipt[]> {
    const displayDataChanges = [
      this.receiptDB.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.receiptDB.data
          .slice()
          .filter((item: Receipt) => {
            return true;
          });

        const sortedData = this.sortData(this.filteredData.slice());
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    this.receiptDB.receiptObs.unsubscribe();
  }

  sortData(data: Receipt[]): Receipt[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string | Date = '';
      let propertyB: number | string | Date = '';

      switch (this._sort.active) {
        case 'createdAt':
          [propertyA, propertyB] = [
            (a.createdAt as Timestamp).toDate(),
            (b.createdAt as Timestamp).toDate()
          ];
          break;
        case 'dcNo':
          [propertyA, propertyB] = [a.dcNo, b.dcNo];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
