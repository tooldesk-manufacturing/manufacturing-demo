import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CoreService } from 'src/app/shared/services/core.service';

@Injectable()
export class AccountService {
  constructor(
    private afAuth: AngularFireAuth,
    private afStore: AngularFirestore,
    private core: CoreService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.verifyEmail();
  }

  verifyEmail() {
    this.core.user.subscribe(user => {
      if (user !== null) {
        if (
          this.core.userRef &&
          this.core.userRef.emailVerified &&
          !user.emailVerified
        ) {
          this.updateEmailVerified(user.uid);
        }
        if (!user.emailVerified && !this.core.verificationReminder) {
          this.core.verificationReminder = true;
          const snackbarRef = this.snackBar.open(
            'Email not verified.',
            'VERIFY',
            {
              duration: 2000
            }
          );
          snackbarRef.onAction().subscribe(() => {
            this.core.userRef.sendEmailVerification();
          });
        }
      }
    });
  }

  forgotPassword(email: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afAuth.auth.sendPasswordResetEmail(email);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  emailLogin(email: string, password: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afAuth.auth.signInWithEmailAndPassword(email, password);
        await this.router.navigate(['order']);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  updateEmailVerified(uid: string) {
    this.afStore.doc(`users/${uid}`).update({ emailVerified: true });
  }
}
