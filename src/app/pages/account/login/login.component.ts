import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import 'css-doodle';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ])
  });

  constructor(
    private accountService: AccountService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  get formControls() {
    return this.loginForm.controls;
  }

  async login(email: string, password: string) {
    this.core.loadingText = 'Logging in...';
    this.spinner.show();
    try {
      if (this.loginForm.valid) {
        await this.accountService.emailLogin(email, password);
        this.spinner.hide();
        this.toaster.success('Successful!', 'Log In');
      } else {
        this.formControls['email'].markAsTouched();
        this.formControls['password'].markAsTouched();
        this.spinner.hide();
        this.toaster.error('Invalid login credentials', 'Log In');
      }
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Log In');
    }
  }
}
