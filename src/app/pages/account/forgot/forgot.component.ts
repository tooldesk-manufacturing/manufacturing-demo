import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent {
  forgotForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  constructor(
    private accountService: AccountService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private router: Router
  ) {}

  get formControls() {
    return this.forgotForm.controls;
  }

  async forgotPassword(emailID: string) {
    this.core.loadingText = 'Sending password reset email...';
    this.spinner.show();
    try {
      await this.accountService.forgotPassword(emailID);
      await this.router.navigate(['account']);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Sent password reset email.');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Forgot Password');
    }
  }
}
