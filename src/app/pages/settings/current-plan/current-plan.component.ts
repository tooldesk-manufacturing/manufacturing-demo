import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/shared/services/core.service';
import { Packages, UsersLimit, DueDate } from 'src/app/shared/models/models';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-current-plan',
  templateUrl: './current-plan.component.html',
  styleUrls: ['./current-plan.component.scss']
})
export class CurrentPlanComponent implements OnInit {
  packages: Packages;
  userLimit: UsersLimit;
  dueDate: DueDate;
  dueDateString: string;
  dueDateFormat: Date;

  constructor(private core: CoreService) {}

  ngOnInit() {
    this.initData();
  }

  private async initData() {
    this.packages = this.core.modules;
    this.userLimit = await this.core.userLimit.pipe(take(1)).toPromise();
    delete this.userLimit.admin;
    this.dueDate = this.core.dueDate;
    this.dueDateString =
      this.dueDate.year +
      '-' +
      this.dueDate.month.padStart(2, '0') +
      '-' +
      this.dueDate.date.padStart(2, '0') +
      'T00:00:00';
    this.dueDateFormat = new Date(this.dueDateString);
  }
}
