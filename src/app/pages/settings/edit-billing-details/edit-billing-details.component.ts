import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingBarService } from '@ngx-loading-bar/core';

import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { BillingDetails } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { take } from 'rxjs/operators';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-edit-billing-details',
  templateUrl: './edit-billing-details.component.html',
  styleUrls: ['./edit-billing-details.component.scss']
})
export class EditBillingDetailsComponent implements OnInit {
  subscriptions: Subscription[] = [];
  billingDetails: BillingDetails;
  billingDetailsForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.email]),
    address: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(11)
    ]),
    phoneNumber2: new FormControl('', [
      Validators.minLength(8),
      Validators.maxLength(11)
    ]),
    gstin: new FormControl('', [
      Validators.minLength(15),
      Validators.maxLength(15)
    ]),
    stateCode: new FormControl('', [
      Validators.minLength(2),
      Validators.maxLength(2)
    ]),
    hsnCode: new FormControl(''),
    sacCode: new FormControl(''),
    neft: new FormControl(''),
    companyLogo: new FormControl('')
  });
  uploadingImages = false;

  constructor(
    private settingsService: SettingsService,
    private core: CoreService,
    private loadingService: LoadingBarService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private async initData() {
    this.loadingService.start();
    this.billingDetails = await this.core.billingDetails
      .pipe(take(1))
      .toPromise();
    this.setFormData();
    this.loadingService.stop();
  }

  private setFormData() {
    this.billingDetailsForm.controls['name'].setValue(
      this.billingDetails.name ? this.billingDetails.name : ''
    );
    this.billingDetailsForm.controls['email'].setValue(
      this.billingDetails.email ? this.billingDetails.email : ''
    );
    this.billingDetailsForm.controls['address'].setValue(
      this.billingDetails.address ? this.billingDetails.address : ''
    );
    this.billingDetailsForm.controls['phoneNumber'].setValue(
      this.billingDetails.phoneNumber ? this.billingDetails.phoneNumber : ''
    );
    this.billingDetailsForm.controls['gstin'].setValue(
      this.billingDetails.gstin ? this.billingDetails.gstin : ''
    );
    this.billingDetailsForm.controls['stateCode'].setValue(
      this.billingDetails.stateCode ? this.billingDetails.stateCode : ''
    );
    this.billingDetailsForm.controls['neft'].setValue(
      this.billingDetails.NEFT ? this.billingDetails.NEFT : ''
    );
    this.billingDetailsForm.controls['phoneNumber2'].setValue(
      this.billingDetails.phoneNumber2 ? this.billingDetails.phoneNumber2 : ''
    );
    this.billingDetailsForm.controls['hsnCode'].setValue(
      this.billingDetails.HSNCode ? this.billingDetails.HSNCode : ''
    );
    this.billingDetailsForm.controls['sacCode'].setValue(
      this.billingDetails.SACCode ? this.billingDetails.SACCode : ''
    );
    this.billingDetailsForm.controls['companyLogo'].setValue(
      this.billingDetails.companyLogo ? this.billingDetails.companyLogo : ''
    );
  }

  get formControls() {
    return this.billingDetailsForm.controls;
  }

  private async readImageFile(file: File): Promise<boolean> {
    return new Promise(resolve => {
      const image = new Image();
      image.src = URL.createObjectURL(file);
      image.onload = () => {
        if (image.naturalHeight === 256 && image.naturalWidth === 256) {
          resolve(true);
        } else {
          resolve(false);
        }
      };
    });
  }

  async uploadPhoto(event: any) {
    this.uploadingImages = true;
    const file = event.target.files.item(0);
    const correctSize = await this.readImageFile(file);
    if (!correctSize) {
      this.toaster.info('The Logo must be of the size 256x256');
      this.uploadingImages = false;
      return;
    }
    const today = new Date();
    const name =
      today.getFullYear().toString() +
      '-' +
      today.getMonth().toString() +
      '-' +
      today.getDate().toString() +
      '-' +
      today.getHours().toString() +
      ':' +
      today.getMinutes().toString() +
      '-' +
      today.getSeconds().toString() +
      file.name;
    try {
      await this.settingsService.uploadPhoto(file, name);
      const path = await this.settingsService.getFileURL(name);
      this.billingDetailsForm.controls['companyLogo'].setValue(path);
      this.billingDetailsForm.controls['companyLogo'].markAsDirty();
      this.toaster.success('Successful!', 'Image upload');
      this.billingDetails.companyLogo = this.billingDetailsForm.controls[
        'companyLogo'
      ].value;
      this.uploadingImages = false;
    } catch (reason) {
      this.toaster.error(reason, 'Image upload');
      this.uploadingImages = false;
    }
  }

  async updateDetails() {
    this.core.loadingText = 'Updating billing details...';
    this.spinner.show();
    try {
      const temp: BillingDetails = {
        name: this.billingDetailsForm.controls['name'].value,
        email: this.billingDetailsForm.controls['email'].value,
        address: this.billingDetailsForm.controls['address'].value,
        phoneNumber: this.billingDetailsForm.controls['phoneNumber'].value,
        gstin: this.billingDetailsForm.controls['gstin'].value,
        stateCode: this.billingDetailsForm.controls['stateCode'].value,
        NEFT: this.billingDetailsForm.controls['neft'].value,
        phoneNumber2: this.billingDetailsForm.controls['phoneNumber2'].value,
        HSNCode: this.billingDetailsForm.controls['hsnCode'].value,
        SACCode: this.billingDetailsForm.controls['sacCode'].value,
        companyLogo: this.billingDetailsForm.controls['companyLogo'].value
      };
      await this.settingsService.updateBillingDetails(temp);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Update billing details');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Update Billing Details');
    }
  }
}
