import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from '../../account/account.service';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-change-pin',
  templateUrl: './change-pin.component.html',
  styleUrls: ['./change-pin.component.scss']
})
export class ChangePinComponent {
  changePinForm: FormGroup = new FormGroup({
    currentPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ]),
    newPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ])
  });

  constructor(
    private settingsService: SettingsService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  get formControls() {
    return this.changePinForm.controls;
  }

  async changePIN(currentPIN: string, newPIN: string) {
    this.core.loadingText = 'Updating PIN...';
    this.spinner.show();
    try {
      await this.settingsService.updatePin(currentPIN, newPIN);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Update PIN');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Update PIN');
    }
  }
}
