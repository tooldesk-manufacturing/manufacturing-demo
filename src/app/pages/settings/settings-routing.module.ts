import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleGuard } from 'src/app/shared/guards/role.guard';
import { SettingsComponent } from './settings.component';
import { ChangePinComponent } from './change-pin/change-pin.component';
import { ChangeEmailComponent } from './change-email/change-email.component';
import { EditDepartmentsComponent } from './edit-departments/edit-departments.component';
import { EditBillingDetailsComponent } from './edit-billing-details/edit-billing-details.component';
import { CurrentPlanComponent } from './current-plan/current-plan.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent
  },
  {
    path: 'pin',
    component: ChangePinComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['owner']
    }
  },
  {
    path: 'email',
    component: ChangeEmailComponent
  },
  {
    path: 'departments',
    component: EditDepartmentsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'billing-details',
    component: EditBillingDetailsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'current-plan',
    component: CurrentPlanComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {}
