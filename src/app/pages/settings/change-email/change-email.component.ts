import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CoreService } from 'src/app/shared/services/core.service';
import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent {
  changeEmailForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  constructor(
    private settingsService: SettingsService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  get formControls() {
    return this.changeEmailForm.controls;
  }

  async changeEmail(newEmail: string) {
    this.core.loadingText = 'Updating email...';
    this.spinner.show();
    try {
      await this.settingsService.updateEmail(newEmail);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Update email');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Update email');
    }
  }
}
