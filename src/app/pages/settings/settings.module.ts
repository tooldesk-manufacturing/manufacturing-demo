import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { SettingsComponent } from './settings.component';
import { SettingsService } from './settings.service';
import { ChangeEmailComponent } from './change-email/change-email.component';
import { ChangePinComponent } from './change-pin/change-pin.component';
import { CurrentPlanComponent } from './current-plan/current-plan.component';
import { EditBillingDetailsComponent } from './edit-billing-details/edit-billing-details.component';
import { EditDepartmentsComponent } from './edit-departments/edit-departments.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SettingsRoutingModule,
    PerfectScrollbarModule
  ],
  declarations: [
    SettingsComponent,
    ChangeEmailComponent,
    ChangePinComponent,
    CurrentPlanComponent,
    EditBillingDetailsComponent,
    EditDepartmentsComponent
  ],
  providers: [SettingsService]
})
export class SettingsModule {}
