import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/shared/services/core.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  userRef: firebase.User;
  constructor(private core: CoreService) {}

  ngOnInit() {
    this.userRef = this.core.userRef;
  }
}
