import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl
} from '@angular/forms';
import { LoadingBarService } from '@ngx-loading-bar/core';

import { ToastrService } from 'ngx-toastr';
import { Department } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { take } from 'rxjs/operators';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-edit-departments',
  templateUrl: './edit-departments.component.html',
  styleUrls: ['./edit-departments.component.scss']
})
export class EditDepartmentsComponent implements OnInit {
  editDepartmentsForm: FormGroup;
  departments = new Array<Department>();

  constructor(
    private formBuilder: FormBuilder,
    private settingsService: SettingsService,
    private core: CoreService,
    private loadingService: LoadingBarService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private async initData() {
    this.loadingService.start();
    this.editDepartmentsForm = this.formBuilder.group({
      departments: this.formBuilder.array([])
    });
    const currentDepartments = await this.core.departments
      .pipe(take(1))
      .toPromise();
    this.departments = new Array<Department>();
    this.purgeFormControlsArray(this.departmentForms);
    currentDepartments.list.forEach(department => {
      this.departments.push(department);
      this.addExistingDepartment(department.name);
    });
    this.loadingService.stop();
  }

  private purgeFormControlsArray(formArray: FormArray) {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  private addExistingDepartment(value: string) {
    const item = this.formBuilder.group({
      name: new FormControl(value, [Validators.required])
    });
    item.markAsPristine();
    this.departmentForms.push(item);
  }

  get formControls() {
    return this.editDepartmentsForm.controls;
  }

  get departmentForms() {
    return this.editDepartmentsForm.get('departments') as FormArray;
  }

  addDepartment() {
    const item = this.formBuilder.group({
      name: new FormControl('', [Validators.required])
    });
    item.markAsPristine();
    this.departmentForms.push(item);
    this.departmentForms.updateValueAndValidity();
  }

  getValidity(i: number) {
    return (<FormArray>this.editDepartmentsForm.get('departments')).controls[i]
      .invalid;
  }

  deleteDepartment(i: number) {
    this.departmentForms.removeAt(i);
    this.departmentForms.markAsDirty();
  }

  async updateDepartments() {
    this.core.loadingText = 'Updating departments...';
    this.spinner.show();
    try {
      await this.settingsService.updateDepartments(this.departmentForms);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Update departments');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Update departments');
    }
  }
}
