import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormArray } from '@angular/forms';
import { BillingDetails, Department } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable()
export class SettingsService {
  constructor(
    private afFunctions: AngularFireFunctions,
    private afStorage: AngularFireStorage,
    private afStore: AngularFirestore,
    private core: CoreService
  ) {}

  getFileURL(file: string): Promise<any> {
    const storageRef = this.afStorage.ref(`profiles/${file}`);
    return storageRef.getDownloadURL().toPromise();
  }

  uploadPhoto(file: File, name: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStorage.ref(`profiles/${name}`).put(file);
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  updateBillingDetails(billingDetail: BillingDetails): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStore
          .collection('meta')
          .doc('billingDetails')
          .set(billingDetail, { merge: true });
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  updateDepartments(departmentForms: FormArray): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        let departments = new Array<Department>();
        departments = departmentForms.value;
        await this.afStore
          .collection('meta')
          .doc('departments')
          .set({
            list: departments
          });
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  updatePin(current: string, newPin: string): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try {
        const updatePinRef = this.afFunctions.httpsCallable('updatePin');
        await updatePinRef({ currentPIN: current, newPIN: newPin }).toPromise();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  updateEmail(newEmail: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.core.userRef.updateEmail(newEmail);
        await this.afStore
          .collection('users')
          .doc(this.core.userRef.uid)
          .set({ email: newEmail, emailVerified: false }, { merge: true });
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }
}
