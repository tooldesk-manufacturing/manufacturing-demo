import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../order.service';
import { CoreService } from 'src/app/shared/services/core.service';
import { ToastrService } from 'ngx-toastr';

import { Order } from 'src/app/shared/models/models';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-update-progress',
  templateUrl: './update-progress.component.html',
  styleUrls: ['./update-progress.component.scss']
})
export class UpdateprogressComponent implements OnInit {
  order: Order;
  itemIndex: number;
  progressForm: FormGroup = new FormGroup({
    progress: new FormControl('')
  });

  constructor(
    public dialogRef: MatDialogRef<UpdateprogressComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService: OrderService,
    private core: CoreService,
    private toaster: ToastrService,
    private spinner: SpinnerOverlayService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.order = this.data.order;
    this.itemIndex = this.data.itemIndex;
    this.progressForm.controls['progress'].setValidators([
      Validators.min(this.order.items[this.itemIndex].progress),
      Validators.max(this.order.items[this.itemIndex].quantity)
    ]);
  }

  get formControls() {
    return this.progressForm.controls;
  }

  async unclaimItem() {
    this.core.loadingText = 'Unclaiming item...';
    this.spinner.show();
    try {
      this.order.items[this.itemIndex].progress = this.progressForm.controls[
        'progress'
      ].value;
      await this.orderService.unclaimItem(this.order, this.itemIndex);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Item unclaimed');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Unclaim item');
    }
  }
}
