import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { OrderService } from '../order.service';
import { Observable, Subscription, of } from 'rxjs';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { PaginationService } from './pagination-service.service';
import { tap, take, startWith, map } from 'rxjs/operators';
import { LoadingBarService } from '@ngx-loading-bar/core';
import {
  User,
  Order,
  OrderItem,
  CustomerSummary
} from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { MessagingService } from 'src/app/shared/services/messaging.service';
import { dateCompareValidator } from 'src/app/shared/validators/date-compare.validator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  @ViewChild('filter', { static: false })
  filter: ElementRef;
  user: User;

  orderType = 'pending';
  searchedOrders: Observable<Order[]>;
  dataChanged: Subscription;

  predicate: any;
  tabIndex: number;
  searchedOrdersLength: number;

  customers: CustomerSummary[] = [];
  customerOptions: string[] = [];
  customerFilteredOptions: Observable<string[]>;

  departmentName: string[];
  userName: string;

  searchOrderForm: FormGroup = new FormGroup({
    searchField: new FormControl('', [Validators.required]),
    searchTerm: new FormControl('', [Validators.required]),
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });

  isCustomer = false;
  isSubvendor = false;

  constructor(
    private core: CoreService,
    private messaging: MessagingService,
    private orderService: OrderService,
    public page: PaginationService,
    private loadingService: LoadingBarService
  ) {}

  ngOnInit() {
    this.messaging.getPermission();
    this.initData();
    this.subscriptions.push(
      (this.dataChanged = this.orderService.dataChanged$.subscribe(res => {
        if (res === 1) {
          this.page.reloadData();
          this.initData();
          this.orderService.resetChanged();
        }
      }))
    );
  }

  private async initData() {
    if (this.core.resetOrders) {
      this.page.reloadData();
      this.core.resetOrders = false;
    }
    this.initRoleWiseOrders();
  }

  private async initRoleWiseOrders() {
    try {
      this.user = await this.core.user.pipe(take(1)).toPromise();
      this.isCustomer = await this.core.checkAuthorization(['customer']);
      this.isSubvendor = await this.core.checkAuthorization(['subvendor']);
      this.getNames();
      if (this.isCustomer) {
        this.page.init('orders', 'createdAt', { reverse: true }, this.user.uid);
      } else if (this.isSubvendor) {
        this.initSubvendorPredicate();
        this.page.init(
          'orders',
          'createdAt',
          { reverse: true },
          null,
          true,
          this.user.uid
        );
      } else if (this.isWorker()) {
        if (this.user.department) {
          this.departmentName = this.user.department.map(x => x.name);
        }
        this.page.init('orders', 'createdAt', { reverse: true }, null);
        this.initWorkerPredicate();
      } else {
        this.getRouteParams();
        this.page.init('orders', 'createdAt', { reverse: true }, null);
      }
      if (this.user) {
        this.userName = this.user.displayName;
      }
    } catch (reason) {
      console.error(reason);
    }
  }

  private getRouteParams() {
    if (
      this.core.URLParams &&
      !this.isEmpty(this.core.URLParams) &&
      this.core.initialURL.search('order') > -1
    ) {
      this.core.orderSearchOverride = {
        searchField: this.core.URLParams['searchField'],
        searchTerm: this.core.URLParams['searchTerm']
      };
      this.initOrderSearch();
      this.core.URLParams = null;
      this.core.initialURL = null;
    }
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  private initOrderSearch() {
    this.orderType = 'search';
    this.tabIndex = 1;
    this.searchOrderForm.controls['searchField'].setValue(
      this.core.orderSearchOverride.searchField
    );
    this.searchOrderForm.controls['searchTerm'].setValue(
      this.core.orderSearchOverride.searchTerm
    );
    this.searchOrders();
  }

  private initWorkerPredicate() {
    this.predicate = (value: any, index: number, array: any[]): boolean => {
      for (let i = 0; i < array.length; i++) {
        if (
          (array[i] as OrderItem).currentSubProcess &&
          (array[i] as OrderItem).currentSubProcess.subProcessType ===
            'department' &&
          this.departmentName &&
          this.departmentName.includes(
            (array[i] as OrderItem).currentSubProcess.departmentName
          )
        ) {
          return true;
        }
      }
      return false;
    };
  }

  private initSubvendorPredicate() {
    this.predicate = (value: any, index: number, array: any[]): boolean => {
      for (let i = 0; i < array.length; i++) {
        if (
          (array[i] as OrderItem).currentSubProcess &&
          (array[i] as OrderItem).currentSubProcess.subProcessType ===
            'subvendor' &&
          (array[i] as OrderItem).currentSubProcess.subvendorUID ===
            this.user.uid
        ) {
          return true;
        }
      }
      return false;
    };
  }

  private getNames() {
    if (!this.isCustomer && !this.isSubvendor) {
      this.subscriptions.push(
        this.core.customers.subscribe(res => {
          this.customers = res;
          this.customerOptions = res.map(customer => customer.name);
        })
      );
      this.customerFilteredOptions = this.searchOrderForm
        .get('searchTerm')
        .valueChanges.pipe(
          startWith(''),
          map(val => this.filterCustomerNames(val))
        );
    }
  }

  private filterCustomerNames(val: string): string[] {
    if (this.searchOrderForm.controls['searchField'].value === 'createdAt') {
      return this.customerOptions;
    }
    return this.customerOptions.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  isWorker() {
    if (this.user) {
      return (
        this.core.checkAuthorization(['worker']) &&
        !this.core.checkAuthorization(['receptionist', 'inspector'])
      );
    } else {
      return false;
    }
  }

  loadMore() {
    this.page.paginate();
  }

  get formControls() {
    return this.searchOrderForm.controls;
  }

  clearForm() {
    this.searchedOrdersLength = undefined;
    this.searchOrderForm.controls['searchTerm'].setValue('');
    if (this.searchOrderForm.controls['searchField'].value === 'customerUID') {
      this.searchOrderForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchOrderForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else if (
      this.searchOrderForm.controls['searchField'].value === 'createdAt'
    ) {
      this.searchOrderForm.controls['searchTerm'].setValue('  ');
      this.searchOrderForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchOrderForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else {
      this.searchOrderForm.controls['fromDate'].setValidators([]);
      this.searchOrderForm.controls['toDate'].setValidators([]);
    }
    this.searchOrderForm.updateValueAndValidity();
  }

  checkCustomerName() {
    const val = this.searchOrderForm.controls['searchTerm'].value;
    if (!this.customerOptions.includes(val)) {
      this.searchOrderForm.controls['searchTerm'].setErrors({
        inList: true
      });
      this.searchOrderForm.updateValueAndValidity();
    }
  }

  onLinkClick(event: MatTabChangeEvent) {
    if (event.tab.textLabel === 'Pending') {
      this.orderType = 'pending';
    } else if (event.tab.textLabel === 'Search') {
      this.orderType = 'search';
      this.initForm();
    }
  }

  private initForm() {
    const today = new Date();
    this.searchOrderForm.controls['fromDate'].setValue(today);
    this.searchOrderForm.controls['toDate'].setValue(today);
  }

  searchOrders() {
    this.loadingService.start();
    if (this.searchOrderForm.controls['searchField'].value === 'customerUID') {
      this.searchedOrders = this.orderService
        .getOrders(
          this.searchOrderForm.controls['searchField'].value,
          this.customers.find(
            x => x.name === this.searchOrderForm.controls['searchTerm'].value
          ).uid,
          this.searchOrderForm.controls['fromDate'].value,
          this.searchOrderForm.controls['toDate'].value
        )
        .pipe(
          tap(orders => {
            this.searchedOrdersLength = orders.length;
            this.loadingService.stop();
          })
        );
    } else if (
      this.searchOrderForm.controls['searchField'].value === 'createdAt'
    ) {
      this.searchedOrders = this.orderService
        .getOrders(
          this.searchOrderForm.controls['searchField'].value,
          null,
          this.searchOrderForm.controls['fromDate'].value,
          this.searchOrderForm.controls['toDate'].value
        )
        .pipe(
          tap(orders => {
            this.searchedOrdersLength = orders.length;
            this.loadingService.stop();
          })
        );
    } else {
      this.searchedOrders = this.orderService
        .getOrders(
          this.searchOrderForm.controls['searchField'].value,
          this.searchOrderForm.controls['searchTerm'].value
        )
        .pipe(
          tap(orders => {
            this.searchedOrdersLength = orders.length;
            this.loadingService.stop();
          })
        );
    }
  }

  resetSearchOrders() {
    this.searchedOrdersLength = undefined;
    this.loadingService.start();
    this.searchOrderForm.reset();
    this.searchOrderForm.markAsPristine();
    this.searchedOrders = of([]);
    this.loadingService.stop();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
