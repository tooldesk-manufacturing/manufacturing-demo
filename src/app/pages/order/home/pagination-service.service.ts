import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from '@angular/fire/firestore';
import { BehaviorSubject, of } from 'rxjs';
import { Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { LoadingBarService } from '@ngx-loading-bar/core';

export interface QueryConfig {
  path: string;
  field: string;
  limit: number;
  reverse: boolean;
  customerUID?: string;
  subvendor?: boolean;
  subvendorUID?: string;
}
@Injectable()
export class PaginationService {
  private _loading = new BehaviorSubject(false);
  private _data = new BehaviorSubject([]);
  private _done = new BehaviorSubject(false);

  private query: QueryConfig;

  data: Observable<any>;
  loading: Observable<boolean> = this._loading.asObservable();
  done: Observable<boolean> = this._done.asObservable();

  constructor(
    private afs: AngularFirestore,
    private loadingService: LoadingBarService
  ) {}

  reloadData() {
    this._data.next([]);
    this.data = of(null);
  }

  init(
    path: string,
    field: string,
    opts?: any,
    customerUID?: string,
    subvendor?: boolean,
    subvendorUID?: string
  ) {
    if (this.getCursor() === null) {
      this.query = {
        path,
        field,
        limit: 4,
        reverse: false,
        customerUID,
        subvendor,
        subvendorUID,
        ...opts
      };
      let first: any;
      if (this.query.customerUID) {
        first = this.afs.collection(this.query.path, ref => {
          return ref
            .where('customerUID', '==', this.query.customerUID)
            .orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
            .limit(this.query.limit);
        });
      } else if (this.query.subvendor && this.query.subvendorUID) {
        first = this.afs.collection(this.query.path, ref => {
          return ref
            .where('status', '==', 'pending')
            .where(
              'currentSubProcessSubvendorUID',
              'array-contains',
              this.query.subvendorUID
            )
            .orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
            .limit(this.query.limit);
        });
      } else {
        first = this.afs.collection(this.query.path, ref => {
          return ref
            .where('status', '==', 'pending')
            .orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
            .limit(this.query.limit);
        });
      }
      this.mapAndUpdate(first);

      this.data = this._data.asObservable();
    }
  }

  paginate() {
    const cursor = this.getCursor();
    let more: any;
    if (this.query.customerUID) {
      more = this.afs.collection(this.query.path, ref => {
        return ref
          .where('customerUID', '==', this.query.customerUID)
          .orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .startAfter(cursor)
          .limit(this.query.limit);
      });
    } else if (this.query.subvendor && this.query.subvendorUID) {
      more = this.afs.collection(this.query.path, ref => {
        return ref
          .where('status', '==', 'pending')
          .where(
            'currentSubProcessSubvendorUID',
            'array-contains',
            this.query.subvendorUID
          )
          .orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .startAfter(cursor)
          .limit(this.query.limit);
      });
    } else {
      more = this.afs.collection(this.query.path, ref => {
        return ref
          .where('status', '==', 'pending')
          .orderBy(this.query.field, this.query.reverse ? 'desc' : 'asc')
          .startAfter(cursor)
          .limit(this.query.limit);
      });
    }
    this.mapAndUpdate(more);
  }

  private getCursor() {
    const current = this._data.value;
    if (current.length) {
      return current[current.length - 1].doc;
    }
    return null;
  }

  private mapAndUpdate(col: AngularFirestoreCollection<any>) {
    if (this._loading.value) {
      return;
    }
    this._loading.next(true);
    this.loadingService.start();
    return col
      .snapshotChanges()
      .pipe(
        tap(arr => {
          const values = arr.map(snap => {
            const data = snap.payload.doc.data();
            const doc = snap.payload.doc;
            return { ...data, doc };
          });
          if (!!values.length) {
            this._data.next(this._data.getValue().concat(values));
          } else {
            this._done.next(true);
          }
          this._loading.next(false);
          this.loadingService.stop();
        }),
        take(1)
      )
      .subscribe();
  }
}
