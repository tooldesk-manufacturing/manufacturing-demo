import { Component, Inject, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {
  orderUID: string;
  containsMultipleItemBill: boolean;
  enterPINForm: FormGroup = new FormGroup({
    currentPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<DeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService: OrderService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.orderUID = this.data.id;
    this.containsMultipleItemBill = this.data.containsMultipleItemBill;
  }

  get formControls() {
    return this.enterPINForm.controls;
  }

  async deleteOrder(orderUID: string, pin: string) {
    this.core.loadingText = 'Deleting order...';
    this.spinner.show();
    try {
      await this.orderService.deleteOrder(orderUID, pin);
      this.core.resetOrders = true;
      this.spinner.hide();
      this.toaster.success('Successful!', 'Order deleted');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Order deletion');
    }
  }
}
