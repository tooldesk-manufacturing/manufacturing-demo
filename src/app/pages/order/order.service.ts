import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { shareReplay, take } from 'rxjs/operators';
import { Timestamp } from '@firebase/firestore-types';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  BillingDetails,
  Order,
  OrderItemSummary,
  Bill,
  SubProcess,
  SubvendorSummary,
  Transaction,
  Receipt,
  User
} from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AngularFireStorage } from '@angular/fire/storage';
import * as firebase from 'firebase/app';

@Injectable()
export class OrderService {
  private _dataChanged = new BehaviorSubject<number>(0);
  dataChanged$ = this._dataChanged.asObservable();

  constructor(
    private afFunctions: AngularFireFunctions,
    private afStorage: AngularFireStorage,
    private afStore: AngularFirestore,
    private core: CoreService,
    private router: Router
  ) {}

  reloadData() {
    this._dataChanged.next(1);
  }

  resetChanged() {
    this._dataChanged.next(0);
  }

  async uploadPhoto(file: File, name: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStorage.ref(`items/${name}`).put(file);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  billItems(order: Order): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const items: OrderItemSummary[] = [];
        for (let i = 0; i < order.items.length; i++) {
          if (order.items[i].status === 'completed') {
            items.push({
              quantity: order.items[i].quantity,
              name: order.items[i].name,
              weight: order.items[i].weight,
              price: order.items[i].price,
              hsn: order.items[i].hsn,
              itemIndex: i
            });
          }
        }
        if (items.length > 0) {
          const today = new Date();
          const billNo =
            'B' +
            order.customerName.substring(0, 2).toUpperCase() +
            today.getDate().toString() +
            (today.getMonth() + 1).toString() +
            today.getFullYear().toString() +
            today.getHours().toString() +
            today.getMinutes().toString();
          const id = this.afStore.createId();
          const tempDoc = this.afStore.collection('bills').doc(id).ref;
          const temp: Bill = {
            id: tempDoc.id,
            billNo: billNo,
            grossAmount: 0,
            netAmount: 0,
            orderUID: [order.id],
            createdAt: firebase.firestore.FieldValue.serverTimestamp() as Timestamp,
            items: items,
            internalDC: [order.internalDC],
            orderCreatedDate: order.createdAt,
            customerDC: [order.customerDC],
            customerUID: order.customerUID,
            refDate: '',
            refPONo: '',
            gstRef: '',
            vehicleNo: '',
            dispatchMode: '',
            eSugam: ''
          };
          temp.complete = true;
          order.items.forEach(item => {
            if (
              item.status === 'pending' ||
              item.status === 'claimed' ||
              item.status === 'ongoing'
            ) {
              temp.complete = false;
            }
          });
          this.core.currentBill = temp;
          resolve();
        } else {
          reject('No completed items');
        }
      } catch (reason) {
        reject(reason);
      }
    });
  }

  dcItems(order: Order): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const items: OrderItemSummary[] = [];
        for (let i = 0; i < order.items.length; i++) {
          items.push({
            quantity: order.items[i].quantity,
            name: order.items[i].name,
            weight: order.items[i].weight,
            hsn: order.items[i].hsn,
            itemIndex: i
          });
        }
        if (items.length > 0) {
          const today = new Date();
          const dcNo =
            'DC' +
            order.customerName.substring(0, 2).toUpperCase() +
            today.getDate().toString() +
            (today.getMonth() + 1).toString() +
            today.getFullYear().toString() +
            today.getHours().toString() +
            today.getMinutes().toString();
          const id = this.afStore.createId();
          const tempDoc = this.afStore.collection('receipts').doc(id).ref;
          const temp: Receipt = {
            id: tempDoc.id,
            dcNo: dcNo,
            orderUID: order.id,
            createdAt: firebase.firestore.FieldValue.serverTimestamp() as Timestamp,
            items: items,
            internalDC: order.internalDC,
            orderCreatedDate: order.createdAt,
            customerDC: order.customerDC,
            customerUID: order.customerUID,
            refDate: '',
            refPONo: '',
            gstRef: '',
            vehicleNo: '',
            dispatchMode: '',
            dcRemarks: '',
            eSugam: ''
          };
          this.core.currentReceipt = temp;
          resolve();
        } else {
          reject('No completed items');
        }
      } catch (reason) {
        reject(reason);
      }
    });
  }

  getCustomerDetails(uid: string): Observable<User> {
    return this.afStore
      .collection('users')
      .doc<User>(uid)
      .valueChanges();
  }

  getBillingDetails(): Observable<BillingDetails> {
    return this.core.billingDetails;
  }

  getFileURL(file: string): Promise<any> {
    const storageRef = this.afStorage.ref(`items/${file}`);
    return storageRef.getDownloadURL().toPromise();
  }

  createOrder(order: Order): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const orders = await this.afStore
        .collection<Order>('orders', ref =>
          ref.where('internalDC', '==', order.internalDC)
        )
        .valueChanges()
        .pipe(take(1))
        .toPromise();
      if (orders.length > 0) {
        reject('Internal DC already exists.');
      } else {
        try {
          await this.afStore
            .collection('orders')
            .doc(order.id)
            .set(order);
          await this.router.navigate(['order']);
          this.reloadData();
          resolve();
        } catch (reason) {
          reject(reason);
        }
      }
    });
  }

  addSubProcess(
    order: Order,
    itemIndex: number,
    subProcess: SubProcess,
    subvendorSummary?: SubvendorSummary
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const batch = this.afStore.firestore.batch();
      try {
        subProcess.orderUID = order.id;
        subProcess.orderItemIndex = itemIndex;
        subProcess.internalDC = order.internalDC;
        if (order.items[itemIndex].subProcesses) {
          order.items[itemIndex].subProcesses.push(subProcess);
        } else {
          order.items[itemIndex].subProcesses = [subProcess];
        }
        order.items[itemIndex].currentSubProcess = subProcess;
        order.items[itemIndex].status = 'ongoing';
        const orderRef = this.afStore.collection('orders').doc(order.id).ref;
        batch.set(orderRef, order, { merge: true });
        if (
          subProcess.subProcessType === 'subvendor' &&
          subProcess.subProcessPrice
        ) {
          const id = this.afStore.createId();
          const tempRef = this.afStore.collection('transactions').doc(id).ref;
          const temp: Transaction = {
            id: tempRef.id,
            amount: subProcess.subProcessPrice,
            subvendorName: subProcess.subvendorName,
            subvendorUID: subProcess.subvendorUID,
            type: 'expenditure',
            createdAt: firebase.firestore.FieldValue.serverTimestamp() as Timestamp,
            orderUID: [subProcess.orderUID],
            orderItemIndex: subProcess.orderItemIndex
          };
          if (subProcess.cash) {
            temp.cash = true;
            temp.paymentReceived = true;
            temp.receivedAt = temp.createdAt;
          } else {
            temp.cash = false;
            temp.paymentReceived = false;
            const summaryRef = this.afStore
              .collection('subvendorSummary')
              .doc(subProcess.subvendorUID).ref;
            batch.set(
              summaryRef,
              {
                amountDue:
                  subvendorSummary.amountDue + subProcess.subProcessPrice
              },
              { merge: true }
            );
          }
          temp.internalDC = [order.internalDC];
          temp.customerDC = [order.customerDC];
          batch.set(tempRef, temp);
        }
        await batch.commit();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  deleteOrder(orderUID: string, pin: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const deleteOrderFunction = this.afFunctions.httpsCallable(
          'deleteOrder'
        );
        await deleteOrderFunction({ pin: pin, orderUID: orderUID }).toPromise();
        this.reloadData();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  getOrders(
    searchField: string,
    searchTerm: string | Date,
    fromDate?: Date,
    toDate?: Date
  ): Observable<Order[]> {
    if (searchField === 'createdAt') {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Order>('orders', ref =>
          ref
            .where('createdAt', '>', yesterday)
            .where('createdAt', '<', tomorrow)
            .orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField === 'customerUID' && fromDate && toDate) {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Order>('orders', ref =>
          ref
            .where(searchField, '==', searchTerm)
            .where('createdAt', '>', yesterday)
            .where('createdAt', '<', tomorrow)
            .orderBy('createdAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else {
      return this.afStore
        .collection<Order>('orders', ref =>
          ref.where(searchField, '==', searchTerm).orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    }
  }

  claimItem(
    order: Order,
    itemIndex: number,
    userName: string
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        order.items[itemIndex].claimed = { claimed: true, name: userName };
        const entry = this.core.userRef.displayName + ' claimed item';
        order.items[itemIndex].timeline.push({
          description: entry,
          status: 'ongoing',
          dateTime: new Date()
        });
        order.items[itemIndex].status = 'claimed';
        await this.afStore
          .collection('orders')
          .doc(order.id)
          .set(order, { merge: true });
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  finishSubvendorSubprocess(
    order: Order,
    itemIndex: number,
    subvendor?: boolean
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        let entry: string;
        if (subvendor) {
          entry =
            order.items[itemIndex].currentSubProcess.subvendorName +
            ' finished process';
        } else {
          entry =
            this.core.userRef.displayName +
            ' finished ' +
            order.items[itemIndex].currentSubProcess.subvendorName +
            ' process';
        }
        order.items[itemIndex].timeline.push({
          description: entry,
          status: 'ongoing',
          dateTime: new Date()
        });
        order.items[itemIndex].subProcesses[
          order.items[itemIndex].subProcesses.length - 1
        ].sentAt = new Date();
        order.currentSubProcessSubvendorUID = order.currentSubProcessSubvendorUID.filter(
          id => id !== order.items[itemIndex].currentSubProcess.subvendorUID
        );
        order.items[itemIndex].currentSubProcess = null;
        order.items[itemIndex].status = 'pending';
        order.items[itemIndex].progress = 0;
        await this.afStore
          .collection('orders')
          .doc(order.id)
          .set(order, { merge: true });
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  unclaimItem(order: Order, itemIndex: number): Promise<string> {
    return new Promise(async (resolve, reject) => {
      if (order.items[itemIndex].progress === order.items[itemIndex].quantity) {
        try {
          const entry = this.core.userRef.displayName + ' unclaimed item';
          order.items[itemIndex].timeline.push({
            description: entry,
            status: 'ongoing',
            dateTime: new Date()
          });
          await this.finishSubProcess(order, itemIndex);
          resolve();
        } catch (reason) {
          reject(reason);
        }
      } else {
        try {
          order.items[itemIndex].claimed = { claimed: false, name: '' };
          const entry = this.core.userRef.displayName + ' unclaimed item';
          order.items[itemIndex].timeline.push({
            description: entry,
            status: 'ongoing',
            dateTime: new Date()
          });
          order.items[itemIndex].status = 'ongoing';
          await this.afStore
            .collection('orders')
            .doc(order.id)
            .set(order, { merge: true });
          resolve();
        } catch (reason) {
          reject(reason);
        }
      }
    });
  }

  finishSubProcess(order: Order, itemIndex: number): Promise<string> {
    return new Promise(async (resolve, reject) => {
      if (order.items[itemIndex].progress === order.items[itemIndex].quantity) {
        try {
          order.items[itemIndex].claimed = { claimed: false, name: '' };
          const entry =
            this.core.userRef.displayName +
            ' finished ' +
            order.items[itemIndex].currentSubProcess.departmentName +
            ' process';
          order.items[itemIndex].timeline.push({
            description: entry,
            status: 'ongoing',
            dateTime: new Date()
          });
          order.items[itemIndex].subProcesses[
            order.items[itemIndex].subProcesses.length - 1
          ].sentAt = new Date();
          order.items[itemIndex].currentSubProcess = null;
          order.items[itemIndex].status = 'pending';
          order.items[itemIndex].progress = 0;
          await this.afStore
            .collection('orders')
            .doc(order.id)
            .set(order, { merge: true });
          resolve();
        } catch (reason) {
          reject(reason);
        }
      } else {
        reject('Item progress is lesser than Quantity');
      }
    });
  }

  async finishItem(order: Order, itemIndex: number): Promise<string> {
    return new Promise(async (resolve, reject) => {
      if (
        order.items[itemIndex].progress === order.items[itemIndex].quantity ||
        order.items[itemIndex].progress === 0
      ) {
        try {
          order.items[itemIndex].claimed = { claimed: false, name: '' };
          const entry = this.core.userRef.displayName + ' finished the item';
          order.items[itemIndex].timeline.push({
            description: entry,
            status: 'completed',
            dateTime: new Date()
          });
          order.items[itemIndex].currentSubProcess = null;
          order.items[itemIndex].status = 'completed';
          await this.afStore
            .collection('orders')
            .doc(order.id)
            .set(order, { merge: true });
          resolve();
        } catch (reason) {
          reject(reason);
        }
      } else {
        reject('Item progress is lesser than Quantity');
      }
    });
  }
}
