import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
  FormArray
} from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { OrderService } from '../order.service';
import { startWith, map } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import { Timestamp } from '@firebase/firestore-types';
import { ToastrService } from 'ngx-toastr';
import {
  CustomerSummary,
  Order,
  OrderItem
} from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { dateLessThanValidator } from 'src/app/shared/validators/date-less-than.validator';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit, OnDestroy {
  createOrderForm: FormGroup;
  customers: CustomerSummary[] = [];
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  subscriptions: Subscription[] = [];
  uploadingImages = false;

  constructor(
    private core: CoreService,
    private formBuilder: FormBuilder,
    private orderService: OrderService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private afStore: AngularFirestore
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.createOrderForm = this.formBuilder.group({
      customerName: new FormControl('', [Validators.required]),
      customerDC: new FormControl(''),
      status: new FormControl('pending'),
      estimatedFinishDate: new FormControl('', [dateLessThanValidator]),
      items: this.formBuilder.array([])
    });
    this.subscriptions.push(
      this.core.customers.subscribe(res => {
        this.customers = res;
        this.options = res.map(customer => customer.name);
      })
    );
    this.filteredOptions = this.createOrderForm
      .get('customerName')
      .valueChanges.pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }

  private getOrderDetails() {
    const today = new Date();
    const order: Order = this.createOrderForm.value;
    const id = this.afStore.createId();
    const temp = this.afStore.collection('orders').doc(id).ref;
    order.id = temp.id;
    const tempUID = this.customers.find(
      x => x.name === this.createOrderForm.value.customerName
    );
    order.customerUID = tempUID.uid;
    order.status = 'pending';
    if (this.createOrderForm.controls['estimatedFinishDate'].dirty) {
      order.estimatedFinishDate = this.createOrderForm.value.estimatedFinishDate;
    } else {
      delete order.estimatedFinishDate;
    }
    order.createdAt = firebase.firestore.FieldValue.serverTimestamp() as Timestamp;
    order.internalDC =
      order.customerName.substring(0, 2).toUpperCase() +
      today.getDate().toString() +
      (today.getMonth() + 1).toString() +
      today.getFullYear().toString() +
      today.getHours().toString() +
      today.getMinutes().toString();
    Array.from<OrderItem>(order.items).forEach(item => {
      if (item.images === null) {
        delete item.images;
      }
      if (item.name === null) {
        delete item.name;
      }
      if (item.weight === null) {
        delete item.weight;
      }
      if (item.hsn === null) {
        delete item.hsn;
      }
      if (item.remark === null) {
        delete item.remark;
      }
      item.progress = 0;
      item.status = 'pending';
      const entry = this.core.userRef.displayName + ' created the order';
      item.timeline = [
        {
          description: entry,
          status: 'pending',
          dateTime: today
        }
      ];
    });
    return order;
  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  checkName() {
    const val = this.createOrderForm.controls['customerName'].value;
    if (!this.options.includes(val)) {
      this.createOrderForm.controls['customerName'].setErrors({
        inList: true
      });
      this.createOrderForm.updateValueAndValidity();
    }
  }

  get formControls() {
    return this.createOrderForm.controls;
  }

  get itemForms() {
    return this.createOrderForm.get('items') as FormArray;
  }

  addItem() {
    const item = this.formBuilder.group({
      name: new FormControl(null),
      weight: new FormControl(null),
      quantity: new FormControl(null, [Validators.required]),
      images: new FormControl(null),
      hsn: new FormControl(null),
      remark: new FormControl(null)
    });
    item.markAsPristine();
    this.itemForms.push(item);
  }

  deleteItem(i: number) {
    this.itemForms.removeAt(i);
  }

  async fileChangeEvent(fileInput: any, i: number) {
    try {
      const filesToUpload = <Array<File>>fileInput.target.files;
      const filePaths = new Array<String>();
      this.uploadingImages = true;
      for (const file of Array.from(filesToUpload)) {
        const today = new Date();
        const name =
          today.getFullYear().toString() +
          '-' +
          today.getMonth().toString() +
          '-' +
          today.getDate().toString() +
          '-' +
          today.getHours().toString() +
          ':' +
          today.getMinutes().toString() +
          '-' +
          today.getSeconds().toString();
        await this.orderService.uploadPhoto(file, name + file.name);
        const path = await this.orderService.getFileURL(name + file.name);
        filePaths.push(path.toString());
        this.itemForms
          .at(i)
          .get('images')
          .setValue(filePaths);
      }
      this.uploadingImages = false;
    } catch (reason) {
      this.uploadingImages = false;
      this.toaster.error('Try again', 'Image upload');
    }
  }

  async createOrder() {
    this.core.loadingText = 'Creating order...';
    this.spinner.show();
    try {
      this.checkName();
      await this.orderService.createOrder(this.getOrderDetails());
      this.core.resetOrders = true;
      this.spinner.hide();
      this.toaster.success('Successful!', 'Order creation');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Order creation');
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
