import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { OrderService } from '../order.service';

import { ToastrService } from 'ngx-toastr';

import { CoreService } from 'src/app/shared/services/core.service';
import {
  Department,
  Order,
  SubvendorSummary,
  SubProcess
} from 'src/app/shared/models/models';
import { take } from 'rxjs/operators';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
@Component({
  selector: 'app-add-process',
  templateUrl: './add-process.component.html',
  styleUrls: ['./add-process.component.scss']
})
export class AddprocessComponent implements OnInit {
  order: Order;
  itemIndex: number;
  subvendors: SubvendorSummary[] = [];
  departments: Department[] = [];
  subProcessType = 'department';

  addDepartmentProcessForm: FormGroup = new FormGroup({
    department: new FormControl('', [Validators.required]),
    description: new FormControl('')
  });

  addSubvendorProcessForm: FormGroup = new FormGroup({
    subvendorName: new FormControl('', [Validators.required]),
    description: new FormControl(''),
    price: new FormControl(''),
    cash: new FormControl(true)
  });

  constructor(
    public dialogRef: MatDialogRef<AddprocessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private orderService: OrderService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private async initData() {
    this.order = this.data.order;
    this.itemIndex = this.data.itemIndex;
    const currentDepartments = await this.core.departments
      .pipe(take(1))
      .toPromise();
    currentDepartments.list.forEach(department => {
      this.departments.push(department);
    });
    const currentSubvendors = await this.core.subvendors
      .pipe(take(1))
      .toPromise();
    currentSubvendors.forEach(subvendor => {
      this.subvendors.push(subvendor);
    });
  }

  get formControls() {
    if (this.subProcessType === 'department') {
      return this.addDepartmentProcessForm.controls;
    } else if (this.subProcessType === 'subvendor') {
      return this.addSubvendorProcessForm.controls;
    }
  }

  private getDepartmentSubprocessDetails() {
    const subprocess: SubProcess = {
      subProcessType: 'department',
      receivedAt: new Date()
    };
    subprocess.departmentName = this.addDepartmentProcessForm.controls[
      'department'
    ].value.name;
    if (this.addDepartmentProcessForm.controls['description'].dirty) {
      subprocess.description = this.addDepartmentProcessForm.controls[
        'description'
      ].value;
    } else {
      delete subprocess.description;
    }
    const entry =
      this.core.userRef.displayName +
      ' sent item to: ' +
      subprocess.departmentName;
    this.order.items[this.itemIndex].timeline.push({
      description: entry,
      status: 'ongoing',
      dateTime: new Date()
    });
    return subprocess;
  }

  private getSubvendorSubprocessDetails() {
    const subprocess: SubProcess = {
      subProcessType: 'subvendor',
      receivedAt: new Date()
    };
    subprocess.subvendorName = this.addSubvendorProcessForm.controls[
      'subvendorName'
    ].value;
    subprocess.subvendorUID = this.subvendors.find(
      sub => sub.name === subprocess.subvendorName
    ).uid;
    if (this.addSubvendorProcessForm.controls['description'].dirty) {
      subprocess.description = this.addSubvendorProcessForm.controls[
        'description'
      ].value;
      subprocess.subProcessPrice = this.addSubvendorProcessForm.controls[
        'price'
      ].value;
    } else {
      delete subprocess.description;
    }
    if (this.addSubvendorProcessForm.controls['price'].dirty) {
      subprocess.subProcessPrice = this.addSubvendorProcessForm.controls[
        'price'
      ].value;
    }
    subprocess.cash = this.addSubvendorProcessForm.controls['cash'].value;
    const entry =
      this.core.userRef.displayName +
      ' sent item to: ' +
      subprocess.subvendorName;
    this.order.items[this.itemIndex].timeline.push({
      description: entry,
      status: 'ongoing',
      dateTime: new Date()
    });
    if (this.order.currentSubProcessSubvendorUID) {
      this.order.currentSubProcessSubvendorUID.push(subprocess.subvendorUID);
    } else {
      this.order.currentSubProcessSubvendorUID = [subprocess.subvendorUID];
    }
    this.order.items[this.itemIndex].progress = 0;
    return subprocess;
  }

  async addSubprocess() {
    this.core.loadingText = 'Adding subprocess...';
    this.spinner.show();
    try {
      if (this.subProcessType === 'department') {
        const subprocess = this.getDepartmentSubprocessDetails();
        await this.orderService.addSubProcess(
          this.order,
          this.itemIndex,
          subprocess
        );
        this.spinner.hide();
        this.toaster.success('Successful!', 'Added subprocess');
      } else if (this.subProcessType === 'subvendor') {
        const subprocess = this.getSubvendorSubprocessDetails();
        await this.orderService.addSubProcess(
          this.order,
          this.itemIndex,
          subprocess,
          this.subvendors.find(
            summary => summary.uid === subprocess.subvendorUID
          )
        );
        this.spinner.hide();
        this.toaster.success('Successful!', 'Added subprocess');
      }
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Subprocess addition');
    }
  }
}
