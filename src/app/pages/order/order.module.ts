import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { CreateOrderComponent } from './create/create-order.component';
import { OrderRoutingModule } from './order-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OrderService } from './order.service';

import { DeleteComponent } from './delete/delete.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddprocessComponent } from './add-process/add-process.component';

import { TimelineComponent } from './timeline/timeline.component';
import { UpdateprogressComponent } from './update-progress/update-progress.component';
import { PaginationService } from './home/pagination-service.service';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ItemImagesComponent } from './item-images/item-images.component';
import { SlideshowModule } from 'ng-simple-slideshow';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { NgxPipesModule } from 'src/app/shared/modules/ngx-pipes.module';
import { OrderComponent } from './order/order.component';

@NgModule({
  imports: [
    CommonModule,
    OrderRoutingModule,
    MaterialModule,
    NgxPipesModule,
    SharedModule,
    PerfectScrollbarModule,
    SlideshowModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    CreateOrderComponent,
    DeleteComponent,
    AddprocessComponent,
    TimelineComponent,
    UpdateprogressComponent,
    ItemImagesComponent
  ],
  declarations: [
    HomeComponent,
    CreateOrderComponent,
    DeleteComponent,
    AddprocessComponent,
    TimelineComponent,
    UpdateprogressComponent,
    ItemImagesComponent,
    OrderComponent
  ],
  providers: [
    OrderService,
    PaginationService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
  ]
})
export class OrderModule {}
