import { Component, OnInit, Input } from '@angular/core';
import { OrderService } from '../order.service';
import { MatDialog } from '@angular/material';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import {
  Order,
  OrderItem,
  User,
  SearchOverride
} from 'src/app/shared/models/models';
import { AddprocessComponent } from '../add-process/add-process.component';
import { UpdateprogressComponent } from '../update-progress/update-progress.component';
import { DeleteComponent } from '../delete/delete.component';
import { TimelineComponent } from '../timeline/timeline.component';
import { ItemImagesComponent } from '../item-images/item-images.component';
import { Router } from '@angular/router';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent {
  @Input() order: Order;
  @Input() user: User;
  @Input() departmentName: string[];
  @Input() userName: string;
  @Input() predicate: any;

  constructor(
    private orderService: OrderService,
    private dialog: MatDialog,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private router: Router
  ) {
    if (this.order && !this.order.containsMultipleItemBill) {
      this.order.containsMultipleItemBill = false;
    }
  }

  isDate(val: any) {
    return val instanceof Date;
  }

  isWorker() {
    if (this.user) {
      return (
        this.core.checkAuthorization(['worker']) &&
        !this.core.checkAuthorization(['receptionist', 'inspector'])
      );
    } else {
      return false;
    }
  }

  isWorkerOrSubvendor() {
    if (this.user) {
      return this.isWorker() || this.isSubvendor();
    } else {
      return false;
    }
  }

  isSubvendor() {
    if (this.user) {
      return this.core.checkAuthorization(['subvendor']);
    } else {
      return false;
    }
  }

  shouldShowItem(item: OrderItem) {
    if (this.isWorker()) {
      if (
        item.currentSubProcess &&
        item.currentSubProcess.subProcessType === 'department' &&
        this.departmentName &&
        this.departmentName.includes(item.currentSubProcess.departmentName)
      ) {
        return true;
      }
    } else if (this.isSubvendor()) {
      if (
        item.currentSubProcess &&
        item.currentSubProcess.subProcessType === 'subvendor' &&
        item.currentSubProcess.subvendorUID === this.user.uid
      ) {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  orderBills(orderUID: string) {
    const temp: SearchOverride = {
      searchField: 'orderUID',
      searchTerm: orderUID
    };
    this.core.billingSearchOverrride = temp;
    this.router.navigate(['billing']);
  }

  orderDCs(orderUID: string) {
    const temp: SearchOverride = {
      searchField: 'orderUID',
      searchTerm: orderUID
    };
    this.core.receiptSearchOverrride = temp;
    this.router.navigate(['delivery-challan']);
  }

  orderTransactions(orderUID: string) {
    const temp: SearchOverride = {
      searchField: 'orderUID',
      searchTerm: orderUID
    };
    this.core.transactionSearchOverrride = temp;
    this.router.navigate(['transactions']);
  }

  addProcess(order: Order, j: number) {
    delete (order as any).doc;
    this.dialog.open(AddprocessComponent, {
      data: { order: order, itemIndex: j }
    });
  }

  deleteOrder(id: string, containsMultipleItemBill: boolean) {
    this.dialog.open(DeleteComponent, {
      data: { id: id, containsMultipleItemBill }
    });
  }

  openImages(images: string[]) {
    this.dialog.open(ItemImagesComponent, {
      data: images,
      height: '80vh',
      width: '80vh',
      panelClass: 'dialog-no-padding'
    });
  }

  openTimeline(order: Order, itemIndex: number) {
    delete (order as any).doc;
    this.dialog.open(TimelineComponent, {
      data: order.items[itemIndex].timeline
    });
  }

  unclaimItem(order: Order, itemIndex: number) {
    delete (order as any).doc;
    this.dialog.open(UpdateprogressComponent, {
      data: { order: order, itemIndex: itemIndex }
    });
  }

  async claimItem(order: Order, itemIndex: number) {
    this.core.loadingText = 'Claiming item...';
    this.spinner.show();
    delete (order as any).doc;
    try {
      await this.orderService.claimItem(order, itemIndex, this.userName);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Item claimed');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Claim item');
    }
  }

  async finishSubvendorProcess(
    order: Order,
    itemIndex: number,
    subvendor?: boolean
  ) {
    this.core.loadingText = 'Finishing subprocess...';
    this.spinner.show();
    delete (order as any).doc;
    try {
      await this.orderService.finishSubvendorSubprocess(
        order,
        itemIndex,
        subvendor
      );
      await this.router.navigate(['order']);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Finished subprocess');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Finishing subprocess');
    }
  }

  async finishSubProcess(order: Order, itemIndex: number) {
    this.core.loadingText = 'Finishing subprocess...';
    this.spinner.show();
    delete (order as any).doc;
    try {
      await this.orderService.finishSubProcess(order, itemIndex);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Finished subprocess');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Finishing subprocess');
    }
  }

  async finishItem(order: Order, itemIndex: number) {
    delete (order as any).doc;
    this.core.loadingText = 'Finishing item...';
    this.spinner.show();
    try {
      await this.orderService.finishItem(order, itemIndex);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Finished item');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Finishing item');
    }
  }

  async billItems(order: Order) {
    this.core.loadingText = 'Billing Items...';
    this.spinner.show();
    delete (order as any).doc;
    try {
      await this.orderService.billItems(order);
      await this.router.navigate(['billing/new']);
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Billing');
    }
  }

  async dcItems(order: Order) {
    this.core.loadingText = 'Generating DC...';
    this.spinner.show();
    delete (order as any).doc;
    try {
      await this.orderService.dcItems(order);
      await this.router.navigate(['delivery-challan/new']);
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'DC Generation');
    }
  }
}
