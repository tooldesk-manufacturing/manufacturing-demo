import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  Bill,
  User,
  Transaction,
  CustomerSummary,
  Order,
  MultipleBillingOrderItemSummary,
  OrderItemSummary
} from '../../shared/models/models';
import { shareReplay, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CoreService } from 'src/app/shared/services/core.service';
import { Timestamp } from '@firebase/firestore-types';
import { AngularFireFunctions } from '@angular/fire/functions';
import * as firebase from 'firebase/app';
import { ExcelService } from './excel-service.service';

@Injectable()
export class BillingService {
  constructor(
    private afFunctions: AngularFireFunctions,
    private afStore: AngularFirestore,
    private core: CoreService,
    private excel: ExcelService
  ) {}

  getBills(
    searchField: string,
    searchTerm: string | Date,
    fromDate?: Date,
    toDate?: Date
  ): Observable<Bill[]> {
    if (searchField === 'createdAt') {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Bill>('bills', ref =>
          ref
            .where(searchField, '>', yesterday)
            .where(searchField, '<', tomorrow)
            .orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField === 'customerUID' && fromDate && toDate) {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Bill>('bills', ref =>
          ref
            .where(searchField, '==', searchTerm)
            .where('createdAt', '>', yesterday)
            .where('createdAt', '<', tomorrow)
            .orderBy('createdAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (
      searchField === 'internalDC' ||
      searchField === 'customerDC' ||
      searchField === 'orderUID'
    ) {
      return this.afStore
        .collection<Bill>('bills', ref =>
          ref
            .where(searchField, 'array-contains', searchTerm)
            .orderBy('createdAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField !== 'customerUID') {
      return this.afStore
        .collection<Bill>('bills', ref =>
          ref.where(searchField, '==', searchTerm).orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    }
  }

  getCustomerSummary(uid: string): Observable<CustomerSummary> {
    return this.afStore
      .collection('customerSummary')
      .doc<CustomerSummary>(uid)
      .valueChanges();
  }

  getCustomerDetails(uid: string): Observable<User> {
    return this.afStore
      .collection('users')
      .doc<User>(uid)
      .valueChanges();
  }

  getOrder(uid: string): Observable<Order> {
    return this.afStore
      .collection('orders')
      .doc<Order>(uid)
      .valueChanges();
  }

  getBill(uid: string): Observable<Bill> {
    return this.afStore
      .collection('bills')
      .doc<Bill>(uid)
      .valueChanges();
  }

  async createMultipleOrdersBill(
    selectedItems: MultipleBillingOrderItemSummary[]
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        const itemList: Map<string, number[]> = new Map<string, number[]>();
        const orderUID: string[] = [];
        const internalDC: string[] = [];
        const customerDC: string[] = [];
        let customerName: string;
        let customerUID: string;
        selectedItems.forEach(item => {
          if (itemList.has(item.orderUID)) {
            const tempi = itemList.get(item.orderUID);
            tempi.push(item.itemIndex);
            itemList.set(item.orderUID, tempi);
          } else {
            itemList.set(item.orderUID, [item.itemIndex]);
          }
        });
        const items: OrderItemSummary[] = [];
        for (const key of Array.from(itemList.keys())) {
          const order = await this.afStore
            .collection('orders')
            .doc<Order>(key)
            .valueChanges()
            .pipe(take(1))
            .toPromise();
          internalDC.push(order.internalDC);
          customerDC.push(order.customerDC);
          orderUID.push(order.id);
          if (!customerName) {
            customerName = order.customerName;
          }
          if (!customerUID) {
            customerUID = order.customerUID;
          }
          itemList.get(key).forEach(i => {
            items.push({
              quantity: order.items[i].quantity,
              name: order.items[i].name,
              weight: order.items[i].weight,
              price: order.items[i].price,
              itemIndex: i,
              orderUID: order.id,
              hsn: order.items[i].hsn
            });
          });
        }
        const today = new Date();
        const billNo =
          'B' +
          customerName.substring(0, 2).toUpperCase() +
          today.getDate().toString() +
          (today.getMonth() + 1).toString() +
          today.getFullYear().toString() +
          today.getHours().toString() +
          today.getMinutes().toString();
        const id = this.afStore.createId();
        const tempDoc = this.afStore.collection('bills').doc(id).ref;
        const bill: Bill = {
          id: tempDoc.id,
          billNo: billNo,
          grossAmount: 0,
          netAmount: 0,
          orderUID: orderUID,
          createdAt: firebase.firestore.FieldValue.serverTimestamp() as Timestamp,
          items: items,
          internalDC: internalDC,
          customerDC: customerDC,
          customerUID: customerUID,
          multipleOrders: true,
          refDate: '',
          refPONo: '',
          gstRef: '',
          vehicleNo: '',
          dispatchMode: ''
        };
        this.core.currentBill = bill;
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  generateBill(
    bill: Bill,
    customerSummary: CustomerSummary,
    order: Order
  ): Promise<string> {
    const temp = this.core.userRef.displayName;
    return new Promise(async (resolve, reject) => {
      try {
        const batch = this.afStore.firestore.batch();
        const billRef = this.afStore.collection('bills').doc(bill.id).ref;
        const id = this.afStore.createId();
        const tRef = this.afStore.collection('transactions').doc(id).ref;
        const tempTransaction: Transaction = this.getTransaction(tRef, bill);
        const oRef = this.afStore
          .collection('orders')
          .doc(bill.orderUID[0] as string).ref;
        order.items.forEach(item => {
          if (item.status === 'completed') {
            item.status = 'billed';
          }
        });
        bill.complete = true;
        order.items.forEach(item => {
          if (item.status === 'billed') {
            const entry = temp + ' billed item';
            item.timeline.push({
              description: entry,
              status: 'billed',
              dateTime: new Date()
            });
          }
        });
        order.items.forEach(item => {
          if (item.status !== 'billed') {
            bill.complete = false;
          }
        });
        if (bill.complete) {
          order.status = 'completed';
        }
        batch.set(oRef, order, { merge: true });
        tempTransaction.paymentReceived = true;
        if (bill.cash === false) {
          tempTransaction.paymentReceived = false;
        }
        if (tempTransaction.paymentReceived) {
          tempTransaction.receivedAt = tempTransaction.createdAt;
        }
        batch.set(tRef, tempTransaction);
        if (bill.cash === false) {
          const sumRef = this.afStore
            .collection('customerSummary')
            .doc(bill.customerUID).ref;
          batch.set(
            sumRef,
            { creditAmount: customerSummary.creditAmount + bill.grossAmount },
            { merge: true }
          );
        }
        delete bill.customerDetails;
        delete bill.companyDetails;
        batch.set(billRef, bill);
        await batch.commit();
        this.core.currentBill = bill;
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  private getTransaction(
    tRef: firebase.firestore.DocumentReference,
    bill: Bill
  ): Transaction {
    return {
      id: tRef.id,
      amount: bill.grossAmount,
      billNo: bill.billNo,
      createdAt: bill.createdAt,
      customerDC: bill.customerDC,
      internalDC: bill.internalDC,
      customerName: bill.customerDetails.name,
      customerUID: bill.customerUID,
      type: 'income',
      cash: bill.cash,
      orderUID: bill.orderUID
    };
  }

  generateMultipleOrdersBill(
    bill: Bill,
    customerSummary: CustomerSummary
  ): Promise<string> {
    const temp = this.core.userRef.displayName;
    return new Promise(async (resolve, reject) => {
      try {
        const batch = this.afStore.firestore.batch();
        const billRef = this.afStore.collection('bills').doc(bill.id).ref;
        const id = this.afStore.createId();
        const tRef = this.afStore.collection('transactions').doc(id).ref;
        const tempTransaction: Transaction = this.getTransaction(tRef, bill);
        const itemList: Map<string, number[]> = new Map<string, number[]>();
        bill.items.forEach(item => {
          if (itemList.has(item.orderUID)) {
            const tempi = itemList.get(item.orderUID);
            tempi.push(item.itemIndex);
            itemList.set(item.orderUID, tempi);
          } else {
            itemList.set(item.orderUID, [item.itemIndex]);
          }
        });
        for (const key of Array.from(itemList.keys())) {
          const orderRef = this.afStore.collection('orders').doc(key).ref;
          const order = await this.afStore
            .collection('orders')
            .doc<Order>(key)
            .valueChanges()
            .pipe(take(1))
            .toPromise();
          itemList.get(key).forEach(i => {
            order.items[i].status = 'billed';
            const entry = temp + ' billed item';
            order.items[i].timeline.push({
              description: entry,
              status: 'billed',
              dateTime: new Date()
            });
          });
          order.containsMultipleItemBill = true;
          batch.set(orderRef, order, { merge: true });
        }
        tempTransaction.paymentReceived = true;
        if (bill.cash === false) {
          tempTransaction.paymentReceived = false;
        }
        if (tempTransaction.paymentReceived) {
          tempTransaction.receivedAt = tempTransaction.createdAt;
        }
        batch.set(tRef, tempTransaction);
        if (bill.cash === false) {
          const sumRef = this.afStore
            .collection('customerSummary')
            .doc(bill.customerUID).ref;
          batch.set(
            sumRef,
            {
              creditAmount: customerSummary.creditAmount + bill.grossAmount
            },
            { merge: true }
          );
        }
        delete bill.customerDetails;
        delete bill.companyDetails;
        batch.set(billRef, bill);
        await batch.commit();
        const batch2 = this.afStore.firestore.batch();
        for (const key of Array.from(itemList.keys())) {
          const orderRef = this.afStore.collection('orders').doc(key).ref;
          const order = await this.afStore
            .collection('orders')
            .doc<Order>(key)
            .valueChanges()
            .pipe(take(1))
            .toPromise();
          let completed = true;
          order.items.forEach(item => {
            if (item.status !== 'billed') {
              completed = false;
            }
          });
          if (completed) {
            order.status = 'completed';
            batch2.set(orderRef, order, { merge: true });
          }
        }
        await batch2.commit();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  getOrders(
    customerUID: string,
    fromDate: string | Date,
    toDate: string | Date
  ): Observable<Order[]> {
    const yesterday = new Date(fromDate as Date);
    yesterday.setHours(0, 0, 0);
    const tomorrow = new Date(toDate as Date);
    tomorrow.setHours(23, 59, 59);
    return this.afStore
      .collection<Order>('orders', ref =>
        ref
          .where('createdAt', '>', yesterday)
          .where('createdAt', '<', tomorrow)
          .where('customerUID', '==', customerUID)
          .where('status', '==', 'pending')
          .orderBy('createdAt', 'asc')
      )
      .valueChanges()
      .pipe(shareReplay(1));
  }

  downloadGSTReport(date: Date): Promise<string> {
    return new Promise(async (resolve, reject) => {
      const yesterday = new Date(date);
      yesterday.setHours(0, 0, 0);
      yesterday.setDate(1);
      yesterday.setMonth(date.getMonth());
      const tomorrow = new Date(date);
      tomorrow.setHours(0, 0, 0);
      tomorrow.setDate(1);
      tomorrow.setMonth(yesterday.getMonth() + 1);
      const bills = await this.afStore
        .collection<Bill>('bills', ref =>
          ref
            .where('createdAt', '>', yesterday)
            .where('createdAt', '<', tomorrow)
            .orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(take(1))
        .toPromise();
      if (bills.length === 0) {
        reject('No bills for selected month');
        return;
      }
      const data = [];
      let invoiceValue = 0;
      let cgstValue = 0;
      let sgstValue = 0;
      let igstValue = 0;
      let basicValue = 0;
      let customerDetails;
      let entry;
      for (const bill of bills) {
        if (bill.gstPercentage < 1) {
          continue;
        }
        entry = {
          'Invoice Date':
            bill.createdAt.toDate().getDate() +
            '/' +
            (bill.createdAt.toDate().getMonth() + 1) +
            '/' +
            bill.createdAt.toDate().getFullYear(),
          'Customer Name': '',
          GSTIN: '',
          'Invoice No': bill.billNo,
          'Invoice Value': bill.grossAmount,
          'CGST Percentage': bill.igst ? 0 : bill.gstPercentage,
          'CGST Value': bill.igst
            ? 0
            : +((bill.netAmount * bill.gstPercentage) / 100).toFixed(2),
          'SGST Percentage': bill.igst ? 0 : bill.gstPercentage,
          'SGST Value': bill.igst
            ? 0
            : +((bill.netAmount * bill.gstPercentage) / 100).toFixed(2),
          'IGST Percentage': bill.igst ? bill.gstPercentage : 0,
          'IGST Value': bill.igst
            ? +((bill.netAmount * bill.gstPercentage) / 100).toFixed(2)
            : 0,
          'Basic Value': bill.netAmount
        };
        if (!bill.customerName) {
          customerDetails = await this.getCustomerDetails(bill.customerUID)
            .pipe(take(1))
            .toPromise();
          entry['Customer Name'] = customerDetails.displayName;
          entry['GSTIN'] = customerDetails.gstin;
        } else {
          entry['Customer Name'] = bill.customerName;
          entry['GSTIN'] = bill.customerGSTIN;
        }
        basicValue += entry['Basic Value'];
        invoiceValue += entry['Invoice Value'];
        if (entry['CGST Value'] > 0) {
          cgstValue += entry['CGST Value'];
          sgstValue += entry['SGST Value'];
        } else {
          igstValue += entry['IGST Value'];
        }
        data.push(entry);
      }
      const finalEntry = {
        'Invoice Date': '',
        'Customer Name': '',
        GSTIN: '',
        'Invoice No': '',
        'Invoice Value': invoiceValue,
        'CGST Percentage': '',
        'CGST Value': cgstValue,
        'SGST Percentage': '',
        'SGST Value': sgstValue,
        'IGST Percentage': '',
        'IGST Value': igstValue,
        'Basic Value': basicValue
      };
      data.push(finalEntry);
      const fileName =
        'GST_INVOICES_' +
        (date.getMonth() + 1).toString() +
        '_' +
        date.getFullYear().toString();
      this.excel.exportAsExcelFile(data, fileName);
      resolve();
    });
  }

  deleteBill(bill: Bill, pin: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const deleteBillFunction = this.afFunctions.httpsCallable('deleteBill');
        await deleteBillFunction({
          pin: pin,
          billUID: bill.id,
          billNo: bill.billNo
        }).toPromise();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }
}
