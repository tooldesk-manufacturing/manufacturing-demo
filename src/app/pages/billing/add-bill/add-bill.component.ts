import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BillingService } from '../billing.service';

import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Bill } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-add-bill',
  templateUrl: './add-bill.component.html',
  styleUrls: ['./add-bill.component.scss']
})
export class AddBillComponent implements OnInit, OnDestroy {
  bill: Bill;
  Math = Math;
  today = Date.now();

  constructor(
    private core: CoreService,
    private billingService: BillingService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    if (!this.core.currentBill) {
      this.router.navigate(['order']);
    }
    this.initData();
  }

  private async initData() {
    this.bill = this.core.currentBill;
    if (this.bill.gstPercentage === undefined) {
      this.bill.gstPercentage = 9;
    }
    this.bill.items.forEach(item => {
      if (item.hsn === undefined) {
        item.hsn = '';
      }
      if (item.weight === undefined) {
        item.weight = 0;
      }
    });
    this.updateNetAmount();
    this.bill.companyDetails = await this.core.billingDetails
      .pipe(take(1))
      .toPromise();
    const customerDetails = await this.billingService
      .getCustomerDetails(this.bill.customerUID)
      .pipe(take(1))
      .toPromise();
    if (customerDetails) {
      this.bill.customerDetails = {
        name: customerDetails.displayName ? customerDetails.displayName : '',
        email: customerDetails.email ? customerDetails.email : '',
        address: customerDetails.address ? customerDetails.address : '',
        phoneNumber: customerDetails.phoneNumber
          ? customerDetails.phoneNumber
          : '',
        phoneNumber2: customerDetails.landlineNumber
          ? customerDetails.landlineNumber
          : '',
        gstin: customerDetails.gstin ? customerDetails.gstin : '',
        HSNCode: customerDetails.hsnCode ? customerDetails.hsnCode : '',
        stateCode: customerDetails.stateCode ? customerDetails.stateCode : ''
      };
      this.bill.customerName = this.bill.customerDetails.name;
      this.bill.customerGSTIN = this.bill.customerDetails.gstin;
    } else {
      this.toaster.error('This customer has been deleted.', 'Billing');
      this.spinner.hide();
      this.router.navigate(['order']);
    }
    if (customerDetails.credit === true) {
      this.bill.cash = false;
    } else {
      this.bill.cash = true;
    }
    this.spinner.hide();
  }

  updateNetAmount() {
    let temp = 0;
    this.bill.items.forEach(item => {
      if (this.bill.billWeight) {
        temp = temp + item.price * item.weight;
      } else {
        temp = temp + item.price * item.quantity;
      }
    });
    this.bill.netAmount = temp;
    if (!this.bill.igst) {
      this.bill.grossAmount = Math.round(
        temp * (1 + (this.bill.gstPercentage * 2) / 100)
      );
    } else {
      this.bill.grossAmount = Math.round(
        temp * (1 + this.bill.gstPercentage / 100)
      );
    }
  }

  isDate(val: any) {
    return val ? val instanceof Date : false;
  }

  async generateBill() {
    this.core.loadingText = 'Billing items...';
    this.spinner.show();
    try {
      let allItemsHavePrice = true;
      this.bill.items.forEach(item => {
        if (!item.price || item.price <= 0) {
          allItemsHavePrice = false;
        }
      });
      if (allItemsHavePrice) {
        if (this.bill.multipleOrders) {
          const customerSummary = await this.billingService
            .getCustomerSummary(this.bill.customerUID)
            .pipe(take(1))
            .toPromise();
          await this.billingService.generateMultipleOrdersBill(
            this.bill,
            customerSummary
          );
          this.core.resetOrders = true;
          this.router.navigate(['billing']);
        } else {
          const customerSummary = await this.billingService
            .getCustomerSummary(this.bill.customerUID)
            .pipe(take(1))
            .toPromise();
          const order = await this.billingService
            .getOrder(this.bill.orderUID[0] as string)
            .pipe(take(1))
            .toPromise();
          await this.billingService.generateBill(
            this.bill,
            customerSummary,
            order
          );
          this.core.resetOrders = true;
          this.router.navigate(['billing']);
        }
        this.spinner.hide();
        this.toaster.success('Success', 'Created bill!');
        this.toaster.success('Success', 'Created transaction!');
        this.toaster.success('Success', 'Updated order!');
      } else {
        this.spinner.hide();
        this.toaster.info('Please enter the rate for the items.', 'Billing');
      }
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Bill creation');
    }
  }

  ngOnDestroy() {
    this.core.currentBill = null;
  }
}
