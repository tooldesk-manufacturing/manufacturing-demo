import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillingComponent } from './billing.component';
import { AddBillComponent } from './add-bill/add-bill.component';
import { RoleGuard } from 'src/app/shared/guards/role.guard';
import { MultipleBillsComponent } from './multiple-bills/multiple-bills.component';

const routes: Routes = [
  {
    path: '',
    component: BillingComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'new',
    component: AddBillComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'multiple-billing',
    component: MultipleBillsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingRoutingModule {}
