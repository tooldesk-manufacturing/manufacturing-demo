import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';

import { BillingService } from './billing.service';
import { Timestamp } from '@firebase/firestore-types';
import { Bill } from 'src/app/shared/models/models';

export class BillingDB {
  billingObs: Subscription;
  dataChange: BehaviorSubject<Bill[]> = new BehaviorSubject<Bill[]>([]);
  get data(): Bill[] {
    return this.dataChange.value;
  }
  constructor(
    private billingService: BillingService,
    private searchField: string,
    private searchTerm: string | Date,
    fromDate?: Date,
    toDate?: Date
  ) {
    this.billingObs = this.billingService
      .getBills(this.searchField, this.searchTerm, fromDate, toDate)
      .subscribe(bill => {
        this.dataChange.next(bill);
      });
  }
}

export class BillingDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Bill[] = [];
  renderedData: Bill[] = [];

  constructor(
    private billingDB: BillingDB,
    private _paginator: MatPaginator,
    private _sort: MatSort
  ) {
    super();

    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<Bill[]> {
    const displayDataChanges = [
      this.billingDB.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.billingDB.data.slice().filter((item: Bill) => {
          return true;
        });

        const sortedData = this.sortData(this.filteredData.slice());
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    this.billingDB.billingObs.unsubscribe();
  }

  sortData(data: Bill[]): Bill[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string | Date = '';
      let propertyB: number | string | Date = '';

      switch (this._sort.active) {
        case 'cash':
          [propertyA, propertyB] = [+a.grossAmount, +b.grossAmount];
          break;
        case 'credit':
          [propertyA, propertyB] = [+a.grossAmount, +b.grossAmount];
          break;
        case 'createdAt':
          [propertyA, propertyB] = [
            (a.createdAt as Timestamp).toDate(),
            (b.createdAt as Timestamp).toDate()
          ];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
