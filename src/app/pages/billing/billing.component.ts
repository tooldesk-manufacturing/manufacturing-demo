import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Subscription, Observable, from } from 'rxjs';
import { BillingService } from './billing.service';
import { startWith, map, take } from 'rxjs/operators';
import { BillingDB, BillingDataSource } from './billing.datasource';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { CustomerSummary, BillingDetails } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { Bill } from 'src/app/shared/models/models';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { dateCompareValidator } from 'src/app/shared/validators/date-compare.validator';
import { NumberToWordsPipe } from 'src/app/shared/pipes/number-to-words.pipe';
import { CurrencyPipe } from '@angular/common';
import { TitleCasePipe } from '@angular/common';
import { DeleteBillComponent } from './delete-bill/delete-bill.component';

import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { INRCurrencyPipe } from 'src/app/shared/pipes/inr-currency.pipe';
import { GstReportComponent } from './gst-report/gst-report.component';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  displayedColumns: string[];
  dataSource: BillingDataSource;
  DB: BillingDB;

  customers: CustomerSummary[] = [];
  customerOptions: string[] = [];
  customerFilteredOptions: Observable<string[]>;

  bill: Bill;

  billSum = 0;
  Math = Math;
  companyLogoDataURL = '';
  companyDetails: BillingDetails;

  searchBillForm: FormGroup = new FormGroup({
    searchTerm: new FormControl('', [Validators.required]),
    searchField: new FormControl('', [Validators.required]),
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private billingService: BillingService,
    private changeDetectorRefs: ChangeDetectorRef,
    private core: CoreService,
    private loadingService: LoadingBarService,
    private numberToWords: NumberToWordsPipe,
    private currency: INRCurrencyPipe,
    private titleCase: TitleCasePipe,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.getRouteParams();
    this.getNames();
    this.initialDataSource();
    this.initForm();
    this.initBillingDetails();
  }

  private initColumns() {
    this.isHandset$.subscribe(isHandset => {
      if (isHandset) {
        this.displayedColumns = ['createdAt', 'cash', 'credit', 'details'];
      } else {
        this.displayedColumns = [
          'createdAt',
          'customerName',
          'billNo',
          'cash',
          'credit',
          'details'
        ];
      }
    });
  }

  private getRouteParams() {
    if (
      this.core.URLParams &&
      !this.isEmpty(this.core.URLParams) &&
      this.core.initialURL.search('billing') > -1
    ) {
      this.core.billingSearchOverrride = {
        searchField: this.core.URLParams['searchField'],
        searchTerm: this.core.URLParams['searchTerm']
      };
      this.core.URLParams = null;
      this.core.initialURL = null;
    }
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  initForm() {
    const today = new Date();
    this.searchBillForm.controls['fromDate'].setValue(today);
    this.searchBillForm.controls['toDate'].setValue(today);
  }

  async initBillingDetails() {
    try {
      this.companyDetails = await this.core.billingDetails
        .pipe(take(1))
        .toPromise();
    } catch (reason) {}
  }

  initialDataSource() {
    if (this.core.billingSearchOverrride) {
      this.changeDataSource(
        this.core.billingSearchOverrride.searchField,
        this.core.billingSearchOverrride.searchTerm
      );
      this.searchBillForm.controls['searchField'].setValue(
        this.core.billingSearchOverrride.searchField
      );
      this.searchBillForm.controls['searchTerm'].setValue(
        this.core.billingSearchOverrride.searchTerm
      );
      this.core.billingSearchOverrride = null;
    } else {
      const today = new Date();
      this.searchBillForm.controls['searchField'].setValue('createdAt');
      this.searchBillForm.controls['searchTerm'].setValue('  ');
      this.searchBillForm.controls['fromDate'].setValue(today);
      this.searchBillForm.controls['toDate'].setValue(today);
      this.changeDataSource();
    }
  }

  private getNames() {
    this.subscriptions.push(
      this.core.customers.subscribe(res => {
        this.customers = res;
        this.customerOptions = res.map(customer => customer.name);
      })
    );
    this.customerFilteredOptions = this.searchBillForm
      .get('searchTerm')
      .valueChanges.pipe(
        startWith(''),
        map(val => this.filterCustomerNames(val))
      );
  }

  private filterCustomerNames(val: string): string[] {
    if (this.searchBillForm.controls['searchField'].value === 'createdAt') {
      return this.customerOptions;
    }
    return this.customerOptions.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  get formControls() {
    return this.searchBillForm.controls;
  }

  clearForm() {
    this.searchBillForm.controls['searchTerm'].setValue('');

    if (this.searchBillForm.controls['searchField'].value === 'customerUID') {
      this.searchBillForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchBillForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else if (
      this.searchBillForm.controls['searchField'].value === 'createdAt'
    ) {
      this.searchBillForm.controls['searchTerm'].setValue('  ');
      this.searchBillForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchBillForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else {
      this.searchBillForm.controls['fromDate'].setValidators([]);
      this.searchBillForm.controls['toDate'].setValidators([]);
    }
    this.searchBillForm.updateValueAndValidity();
  }

  checkCustomerName() {
    const val = this.searchBillForm.controls['searchTerm'].value;
    if (!this.customerOptions.includes(val)) {
      this.searchBillForm.controls['searchTerm'].setErrors({
        inList: true
      });
      this.searchBillForm.updateValueAndValidity();
    }
  }

  changeDataSource(
    overrideSearchField?: string,
    overrideSearchTerm?: Date | string
  ) {
    this.loadingService.start();
    this.initColumns();
    if (!overrideSearchField || !overrideSearchTerm) {
      if (this.searchBillForm.controls['searchField'].value === 'customerUID') {
        overrideSearchTerm = this.customers.find(
          x => x.name === this.searchBillForm.controls['searchTerm'].value
        ).uid;
        this.DB = new BillingDB(
          this.billingService,
          this.searchBillForm.controls['searchField'].value,
          overrideSearchTerm,
          this.searchBillForm.controls['fromDate'].value,
          this.searchBillForm.controls['toDate'].value
        );
      } else if (
        this.searchBillForm.controls['searchField'].value === 'createdAt'
      ) {
        this.DB = new BillingDB(
          this.billingService,
          this.searchBillForm.controls['searchField'].value,
          null,
          this.searchBillForm.controls['fromDate'].value,
          this.searchBillForm.controls['toDate'].value
        );
      } else {
        this.DB = new BillingDB(
          this.billingService,
          this.searchBillForm.controls['searchField'].value,
          this.searchBillForm.controls['searchTerm'].value
        );
      }
    } else {
      this.DB = new BillingDB(
        this.billingService,
        overrideSearchField,
        overrideSearchTerm
      );
    }
    this.dataSource = new BillingDataSource(this.DB, this.paginator, this.sort);
    this.changeDetectorRefs.detectChanges();
    this.loadingService.stop();
  }

  isDate(val: any) {
    return val instanceof Date;
  }

  copyToClipboard(message: string) {
    const el = document.createElement('textarea');
    el.value = message;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    const selected =
      document.getSelection().rangeCount > 0
        ? document.getSelection().getRangeAt(0)
        : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
      document.getSelection().removeAllRanges();
      document.getSelection().addRange(selected);
    }
    this.snackbar.open('Bill No copied to clipboard', 'CLOSE', {
      duration: 1000
    });
  }

  private getString(
    value: string | string[] | number,
    optional?: string
  ): string | string[] | number {
    if (value && optional) {
      return value + optional;
    } else if (value) {
      return value;
    } else {
      return '';
    }
  }

  private getBase64Image(img) {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, 60, 60);
    const dataURL = canvas.toDataURL('image/png');
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
  }

  onImageLoad() {
    this.companyLogoDataURL = this.getBase64Image(
      document.getElementById('companyLogo')
    );
  }

  private getDCDate(ours: boolean) {
    if (this.bill.multipleOrders) {
      return '';
    } else {
      if (ours) {
        return (
          'Our DC Date: ' +
          this.bill.orderCreatedDate.toDate().toLocaleDateString('en-IN') +
          '\n'
        );
      } else {
        return (
          'Your DC Date: ' +
          this.bill.orderCreatedDate.toDate().toLocaleDateString('en-IN') +
          '\n'
        );
      }
    }
  }

  private getDCNo() {
    if (this.bill.multipleOrders) {
      return '';
    } else {
      return 'Our DC No: ' + this.getString(this.bill.internalDC) + '\n';
    }
  }

  private getCurrencyString(amount: any): string {
    return this.currency.transform(amount, false).toString();
  }

  printPDF(option: string) {
    const doc = new jsPDF();
    doc.setFontSize(12);
    doc.setFont('helvetica', 'bold');
    doc.text(this.getString(this.bill.companyDetails.name), 28, 7);
    if (this.bill.companyDetails.companyLogo && this.companyLogoDataURL) {
      doc.addImage(this.companyLogoDataURL, 'PNG', 4, 7, 90, 90);
    }
    doc.text('TAX INVOICE', 179, 7);
    doc.setFont('helvetica', 'normal');
    doc.setFontSize(8);
    const companyDetails = doc.splitTextToSize(
      this.getString(this.bill.companyDetails.address) +
        '\n' +
        'Contact: ' +
        this.getString(this.bill.companyDetails.phoneNumber, ',') +
        this.getString(this.bill.companyDetails.phoneNumber2, ',') +
        this.getString(this.bill.companyDetails.email) +
        '\n' +
        'State Code: ' +
        this.getString(this.bill.companyDetails.stateCode) +
        '\n' +
        'GSTIN: ' +
        this.getString(this.bill.companyDetails.gstin) +
        '\n' +
        'SAC Code: ' +
        this.getString(this.bill.companyDetails.SACCode),
      100
    );
    doc.text(companyDetails, 28, 12);
    doc.setLineWidth(0.25);
    doc.line(2, 32, 208, 32);
    const customerDetails = doc.splitTextToSize(
      'Details of receiver(Bill to):' +
        '\n' +
        this.getString(this.bill.customerDetails.name) +
        '\n' +
        this.getString(this.bill.customerDetails.address, '\n') +
        'Contact: ' +
        this.getString(this.bill.customerDetails.phoneNumber, ',') +
        this.getString(this.bill.customerDetails.phoneNumber2) +
        '\n' +
        'State Code: ' +
        this.getString(this.bill.customerDetails.stateCode) +
        '\n' +
        'GSTIN: ' +
        this.getString(this.bill.customerDetails.gstin) +
        '\n' +
        'SAC Code: ' +
        this.getString(this.bill.customerDetails.SACCode) +
        '\n' +
        'Your DC No: ' +
        this.getString(this.bill.customerDC) +
        '\n' +
        this.getDCDate(false),
      90
    );
    doc.text(customerDetails, 4, 38);
    const billDetails = doc.splitTextToSize(
      'Invoice No: ' +
        this.getString(this.bill.billNo) +
        '\n' +
        'Invoice Date: ' +
        this.bill.createdAt.toDate().toLocaleDateString('en-IN') +
        '\n' +
        'Ref PO No: ' +
        this.getString(this.bill.refPONo) +
        '\n' +
        'Ref Date: ' +
        this.getString(this.bill.refDate) +
        '\n' +
        this.getDCNo() +
        this.getDCDate(true) +
        'E-Sugam Details: ' +
        this.getString(this.bill.eSugam) +
        '\n' +
        'Invoice Ref No(GST): ' +
        this.getString(this.bill.gstRef) +
        '\n' +
        'Mode of dispatch: ' +
        this.getString(this.bill.dispatchMode) +
        '\n' +
        'Vehicle No: ' +
        this.getString(this.bill.vehicleNo),
      90
    );
    doc.text(billDetails, 115, 38);
    const itemTableHeaders = [
      [
        '#',
        'Desription',
        'HSN',
        'Quantity(Nos)',
        'Weight(Kgs)',
        'Rate(Rs/Kg Rs/Nos)',
        'Taxable Value(Rs)'
      ]
    ];
    const items = [];
    for (let i = 0; i < this.bill.items.length; i++) {
      items.push([
        i + 1,
        this.getString(this.bill.items[i].name),
        this.getString(this.bill.items[i].hsn),
        this.getString(this.bill.items[i].quantity),
        this.getString(this.bill.items[i].weight),
        this.getString(this.bill.items[i].price),
        this.bill.billWeight
          ? this.getCurrencyString(
              (this.bill.items[i].weight * this.bill.items[i].price).toFixed(2)
            )
          : this.getCurrencyString(
              (this.bill.items[i].quantity * this.bill.items[i].price).toFixed(
                2
              )
            )
      ]);
    }
    (doc as any).autoTable({
      theme: 'striped',
      startY: 73.25,
      styles: {
        font: 'helvetica',
        halign: 'center',
        valign: 'middle',
        fontSize: 10
      },
      headStyles: { overflow: 'linebreak', fontSize: 8 },
      columnStyles: {
        0: { cellWidth: 10 },
        1: { cellWidth: 45 },
        2: { cellWidth: 45 },
        3: { cellWidth: 25 },
        4: { cellWidth: 20 },
        5: { cellWidth: 20 },
        6: { cellWidth: 20, halign: 'right' }
      },
      bodyStyles: {
        fontStyle: 'bold'
      },
      tableWidth: 205.2,
      margin: { top: 2, left: 2.3, bottom: 80 },
      head: itemTableHeaders,
      body: items
    });
    doc.setFontSize(8);
    let footerTable;
    if (!this.bill.igst) {
      footerTable = [
        [
          'Payment Terms',
          'Basic Value',
          this.getCurrencyString(this.bill.netAmount.toFixed(2))
        ],
        [
          '',
          'CGST @ ' + this.bill.gstPercentage + '%',
          this.getCurrencyString(
            ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
          )
        ],
        [
          '',
          'SGST @ ' + this.bill.gstPercentage + '%',
          this.getCurrencyString(
            ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
          )
        ],
        [
          '',
          'Sub Total',
          this.getCurrencyString(
            (
              ((this.bill.netAmount * this.bill.gstPercentage) / 100) *
              2
            ).toFixed(2)
          )
        ],
        [
          '',
          'Round Off',
          this.Math.abs(
            this.bill.netAmount * (1 + (this.bill.gstPercentage * 2) / 100) -
              this.Math.round(
                this.bill.netAmount * (1 + (this.bill.gstPercentage * 2) / 100)
              )
          ).toFixed(2)
        ],
        ['Invoice Remarks', this.bill.invoiceRemarks],
        [
          'Total Invoice value in words',
          this.titleCase.transform(
            this.numberToWords.transform(this.bill.grossAmount)
          )
        ],
        [
          'Total Invoice value in figures',
          '',
          this.getCurrencyString(this.bill.grossAmount.toFixed(2))
        ],
        [
          'Amount of Tax subject to Reverse Charges',
          'CGST: ' +
            this.getCurrencyString(
              ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
            ) +
            ' SGST: ' +
            this.getCurrencyString(
              ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
            ),
          ''
        ],
        ['', '', 'For ' + this.getString(this.bill.companyDetails.name)]
      ];
    } else {
      footerTable = [
        ['Payment Terms', 'Basic Value', this.bill.netAmount],
        [
          '',
          'IGST @ ' + this.bill.gstPercentage + '%',
          this.getCurrencyString(
            ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
          )
        ],
        [
          '',
          'Sub Total',
          this.getCurrencyString(
            ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
          )
        ],
        [
          '',
          'Round Off',
          this.Math.abs(
            this.bill.netAmount * (1 + this.bill.gstPercentage / 100) -
              this.Math.round(
                this.bill.netAmount * (1 + this.bill.gstPercentage / 100)
              )
          ).toFixed(2)
        ],
        ['Invoice Remarks', this.bill.invoiceRemarks],
        [
          'Total Invoice value in words',
          this.titleCase.transform(
            this.numberToWords.transform(this.bill.grossAmount)
          )
        ],
        [
          'Total Invoice value in figures',
          '',
          this.getCurrencyString(this.bill.grossAmount.toFixed(2))
        ],
        [
          'Amount of Tax subject to Reverse Charges',
          'IGST: ' +
            this.getCurrencyString(
              ((this.bill.netAmount * this.bill.gstPercentage) / 100).toFixed(2)
            ) +
            ''
        ],
        ['', '', 'For ' + this.getString(this.bill.companyDetails.name)]
      ];
    }

    (doc as any).autoTable({
      theme: 'striped',
      startY: 220,
      styles: {
        font: 'helvetica',
        halign: 'left',
        valign: 'middle'
      },
      columnStyles: {
        0: { cellWidth: 40 },
        1: { cellWidth: 20 },
        2: { cellWidth: 40, halign: 'right' }
      },
      bodyStyles: {
        cellPadding: 0.5
      },
      tableWidth: 205.2,
      margin: { left: 2.3 },
      body: footerTable
    });

    doc.setFontSize(10);
    doc.text('Receiver\'s Seal and Signature', 3, 294);
    doc.text('Authorised Signature', 174, 294);
    doc.setLineWidth(0.5);
    for (let i = 1; i <= doc.internal.getNumberOfPages(); i++) {
      doc.setPage(i);
      doc.setDrawColor(0);
      doc.rect(2, 2, 206, 293);
    }
    doc.setProperties({
      title: this.bill.billNo,
      subject: 'Invoice',
      author: this.core.userRef.displayName,
      creator: this.bill.companyDetails.name
    });
    if (option === 'view') {
      doc.output('dataurlnewwindow', { title: this.bill.billNo });
    } else if (option === 'download') {
      doc.save(this.bill.billNo);
    } else if (option === 'print') {
      doc.autoPrint();
      doc.output('dataurlnewwindow', { title: this.bill.billNo });
    }
  }

  deleteBill(bill: Bill) {
    this.dialog.open(DeleteBillComponent, {
      data: bill
    });
  }

  openGSTReport() {
    this.dialog.open(GstReportComponent);
  }

  async openBill(bill: Bill, option: string) {
    try {
      this.bill = bill;
      this.bill.companyDetails = this.companyDetails;
      const customerDetails = await this.billingService
        .getCustomerDetails(this.bill.customerUID)
        .pipe(take(1))
        .toPromise();
      this.bill.customerDetails = {
        name: customerDetails.displayName,
        email: customerDetails.email,
        address: customerDetails.address,
        phoneNumber: customerDetails.phoneNumber,
        phoneNumber2: customerDetails.landlineNumber,
        gstin: customerDetails.gstin,
        HSNCode: customerDetails.hsnCode,
        stateCode: customerDetails.stateCode
      };
      this.printPDF(option);
      this.bill = null;
    } catch (reason) {
      this.bill = null;
    }
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
