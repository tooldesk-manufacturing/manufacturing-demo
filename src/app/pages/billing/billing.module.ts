import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BillingService } from './billing.service';
import { BillingComponent } from './billing.component';
import { BillingRoutingModule } from './billing-routing.module';
import { AddBillComponent } from './add-bill/add-bill.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { NgxPipesModule } from 'src/app/shared/modules/ngx-pipes.module';
import { MultipleBillsComponent } from './multiple-bills/multiple-bills.component';
import { NumberToWordsPipe } from 'src/app/shared/pipes/number-to-words.pipe';
import { DeleteBillComponent } from './delete-bill/delete-bill.component';
import { INRCurrencyPipe } from 'src/app/shared/pipes/inr-currency.pipe';
import { GstReportComponent } from './gst-report/gst-report.component';
import { ExcelService } from './excel-service.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    BillingRoutingModule,
    PerfectScrollbarModule,
    NgxPipesModule
  ],
  declarations: [
    BillingComponent,
    AddBillComponent,
    MultipleBillsComponent,
    DeleteBillComponent,
    GstReportComponent
  ],
  entryComponents: [DeleteBillComponent, GstReportComponent],
  providers: [
    BillingService,
    NumberToWordsPipe,
    TitleCasePipe,
    INRCurrencyPipe,
    ExcelService
  ]
})
export class BillingModule {}
