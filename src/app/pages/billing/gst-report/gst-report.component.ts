import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDatepicker
} from '@angular/material';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material';
import * as moment from 'moment';
import { Moment } from 'moment';
import { CoreService } from 'src/app/shared/services/core.service';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { ToastrService } from 'ngx-toastr';
import { BillingService } from '../billing.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY'
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'app-gst-report',
  templateUrl: './gst-report.component.html',
  styleUrls: ['./gst-report.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class GstReportComponent {
  searchDateForm: FormGroup = new FormGroup({
    fromDate: new FormControl(moment(), [Validators.required])
  });

  constructor(
    public dialogRef: MatDialogRef<GstReportComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private billingService: BillingService
  ) {}

  get formControls() {
    return this.searchDateForm.controls;
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.searchDateForm.controls['fromDate'].value;
    ctrlValue.year(normalizedYear.year());
    this.searchDateForm.controls['fromDate'].setValue(ctrlValue);
  }

  chosenMonthHandler(
    normalizedMonth: Moment,
    datepicker: MatDatepicker<Moment>
  ) {
    const ctrlValue = this.searchDateForm.controls['fromDate'].value;
    ctrlValue.month(normalizedMonth.month());
    this.searchDateForm.controls['fromDate'].setValue(ctrlValue);
    datepicker.close();
  }

  async downloadReport() {
    const date = new Date();
    date.setMonth(
      (this.searchDateForm.controls['fromDate'].value as Moment).month()
    );
    date.setFullYear(
      (this.searchDateForm.controls['fromDate'].value as Moment).year()
    );
    this.core.loadingText = 'Generating report...';
    this.spinner.show();
    try {
      await this.billingService.downloadGSTReport(date);
      this.spinner.hide();
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'GST Report');
      this.dialogRef.close();
    }
  }
}
