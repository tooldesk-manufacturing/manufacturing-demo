import { Component, Inject, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { Bill } from 'src/app/shared/models/models';
import { BillingService } from '../billing.service';

@Component({
  selector: 'app-delete-bill',
  templateUrl: './delete-bill.component.html',
  styleUrls: ['./delete-bill.component.scss']
})
export class DeleteBillComponent implements OnInit {
  bill: Bill;
  enterPINForm: FormGroup = new FormGroup({
    currentPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<DeleteBillComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private billingService: BillingService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.bill = this.data;
  }

  get formControls() {
    return this.enterPINForm.controls;
  }

  async deleteBill(bill: Bill, pin: string) {
    this.core.loadingText = 'Deleting bill...';
    this.spinner.show();
    try {
      await this.billingService.deleteBill(bill, pin);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Bill deleted');
      this.toaster.success('Successful!', 'Transaction deleted');
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Deleting bill');
      this.dialogRef.close();
    }
  }
}
