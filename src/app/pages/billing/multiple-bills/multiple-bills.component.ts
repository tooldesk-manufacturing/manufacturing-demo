import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
  ElementRef
} from '@angular/core';
import { CustomerSummary } from 'functions/src/models';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { CoreService } from 'src/app/shared/services/core.service';
import {
  MultipleBillingOrderItemSummaryDataSource,
  MultipleBillingItemsDB
} from './multiple-bills.datasource';
import { BillingService } from '../billing.service';
import { dateCompareValidator } from 'src/app/shared/validators/date-compare.validator';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MultipleBillingOrderItemSummary } from 'src/app/shared/models/models';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-multiple-bills',
  templateUrl: './multiple-bills.component.html',
  styleUrls: ['./multiple-bills.component.scss']
})
export class MultipleBillsComponent implements OnInit, OnDestroy {
  customers: CustomerSummary[] = [];
  customerOptions: string[] = [];
  customerFilteredOptions: Observable<string[]>;

  subscriptions: Subscription[] = [];

  searchOrdersForm: FormGroup = new FormGroup({
    customerUID: new FormControl('', [Validators.required]),
    fromDate: new FormControl('', [Validators.required]),
    toDate: new FormControl('', [dateCompareValidator('fromDate')])
  });

  dataSource: MultipleBillingOrderItemSummaryDataSource;
  selection = new SelectionModel<MultipleBillingOrderItemSummary>(true, []);
  DB: MultipleBillingItemsDB;
  displayedColumns: string[];
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  constructor(
    private core: CoreService,
    private billingService: BillingService,
    private loadingService: LoadingBarService,
    private changeDetectorRefs: ChangeDetectorRef,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getNames();
    this.initialDataSource();
  }

  private getNames() {
    this.subscriptions.push(
      this.core.customers.subscribe(res => {
        this.customers = res;
        this.customerOptions = res.map(customer => customer.name);
      })
    );
    this.customerFilteredOptions = this.searchOrdersForm
      .get('customerUID')
      .valueChanges.pipe(
        startWith(''),
        map(val => this.filterCustomerNames(val))
      );
  }

  private filterCustomerNames(val: string): string[] {
    return this.customerOptions.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  get formControls() {
    return this.searchOrdersForm.controls;
  }

  initialDataSource() {
    const firstDay = new Date();
    firstDay.setDate(1);
    this.searchOrdersForm.controls['fromDate'].setValue(firstDay);
    const today = new Date();
    this.searchOrdersForm.controls['toDate'].setValue(today);
    this.searchOrdersForm.controls['customerUID'].setValue('');
  }

  clearForm() {
    this.searchOrdersForm.controls['customerUID'].setValue('');
    this.searchOrdersForm.controls['fromDate'].setValue('');
    this.searchOrdersForm.controls['toDate'].setValue('');
    this.searchOrdersForm.updateValueAndValidity();
  }

  isAllSelected(): boolean {
    if (!this.dataSource) {
      return false;
    }
    if (this.selection.isEmpty()) {
      return false;
    }

    return this.selection.selected.length === this.DB.data.length;
  }

  masterToggle() {
    if (!this.dataSource) {
      return;
    }

    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.DB.data.forEach(data => {
        this.selection.select(data);
      });
    }
  }

  checkCustomerName() {
    const val = this.searchOrdersForm.controls['customerUID'].value;
    if (!this.customerOptions.includes(val)) {
      this.searchOrdersForm.controls['customerUID'].setErrors({
        inList: true
      });
      this.searchOrdersForm.updateValueAndValidity();
    }
  }

  changeDataSource() {
    this.loadingService.start();
    this.selection.clear();
    this.displayedColumns = [
      'select',
      'name',
      'quantity',
      'weight',
      'customerDC',
      'createBill'
    ];
    this.DB = new MultipleBillingItemsDB(
      this.billingService,
      this.customers.find(
        x => x.name === this.searchOrdersForm.controls['customerUID'].value
      ).uid,
      this.searchOrdersForm.controls['fromDate'].value,
      this.searchOrdersForm.controls['toDate'].value
    );
    this.dataSource = new MultipleBillingOrderItemSummaryDataSource(
      this.DB,
      this.paginator,
      this.sort
    );
    this.changeDetectorRefs.detectChanges();
    this.loadingService.stop();
  }

  async createBill() {
    try {
      await this.billingService.createMultipleOrdersBill(
        this.selection.selected
      );
      await this.router.navigate(['billing/new']);
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Billing');
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
