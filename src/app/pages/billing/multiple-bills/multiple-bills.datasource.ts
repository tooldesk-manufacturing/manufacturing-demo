import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map, take } from 'rxjs/operators';
import { MultipleBillingOrderItemSummary } from 'src/app/shared/models/models';
import { BillingService } from '../billing.service';

export class MultipleBillingItemsDB {
  dataChange: BehaviorSubject<
    MultipleBillingOrderItemSummary[]
  > = new BehaviorSubject<MultipleBillingOrderItemSummary[]>([]);

  get data(): MultipleBillingOrderItemSummary[] {
    return this.dataChange.value;
  }

  constructor(
    private billingService: BillingService,
    customerName: string,
    fromDate: Date,
    toDate: Date
  ) {
    this.getData(customerName, fromDate, toDate);
  }

  async getData(customerName: string, fromDate: Date, toDate: Date) {
    const orders = await this.billingService
      .getOrders(customerName, fromDate, toDate)
      .pipe(take(1))
      .toPromise();
    const filteredItems: MultipleBillingOrderItemSummary[] = [];
    orders.forEach(order => {
      order.items.forEach(item => {
        if (item.status === 'completed') {
          filteredItems.push({
            name: item.name,
            orderUID: order.id,
            quantity: item.quantity,
            weight: item.weight,
            itemIndex: order.items.findIndex(itemToFind => itemToFind === item),
            customerDC: order.customerDC
          });
        }
      });
    });
    this.dataChange.next(filteredItems);
  }
}

export class MultipleBillingOrderItemSummaryDataSource extends DataSource<any> {
  renderedData: MultipleBillingOrderItemSummary[] = [];
  retrievedData: MultipleBillingOrderItemSummary[] = [];

  constructor(
    private MultipleBillingOrderItemSummarysDB: MultipleBillingItemsDB,
    private _paginator: MatPaginator,
    private _sort: MatSort
  ) {
    super();
  }

  connect(): Observable<MultipleBillingOrderItemSummary[]> {
    const displayDataChanges = [
      this.MultipleBillingOrderItemSummarysDB.dataChange,
      this._sort.sortChange,
      this._paginator.page
    ];

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.retrievedData = this.MultipleBillingOrderItemSummarysDB.data.slice();
        const sortedData = this.sortData(this.retrievedData.slice());
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {}

  sortData(
    data: MultipleBillingOrderItemSummary[]
  ): MultipleBillingOrderItemSummary[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'name':
          [propertyA, propertyB] = [a.name.toLowerCase(), b.name.toLowerCase()];
          break;
        case 'customerDC':
          [propertyA, propertyB] = [
            a.customerDC.toLowerCase(),
            b.customerDC.toLowerCase()
          ];
          break;
        case 'quantity':
          [propertyA, propertyB] = [+a.quantity, +b.quantity];
          break;
        case 'weight':
          [propertyA, propertyB] = [+a.weight, +b.weight];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
