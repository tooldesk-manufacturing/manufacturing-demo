import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import {
  SubvendorSummary,
  WorkerSummary,
  CustomerSummary
} from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';

export class SubvendorDB {
  subvendorObs: Subscription;
  dataChange: BehaviorSubject<SubvendorSummary[]> = new BehaviorSubject<
    SubvendorSummary[]
  >([]);
  get data(): SubvendorSummary[] {
    return this.dataChange.value;
  }

  constructor(private core: CoreService) {
    this.subvendorObs = this.core.subvendors.subscribe(subvendor => {
      this.dataChange.next(subvendor);
    });
  }
}

export class SubvendorDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: SubvendorSummary[] = [];
  renderedData: SubvendorSummary[] = [];

  constructor(
    private subvendorDB: SubvendorDB,
    private _paginator: MatPaginator,
    private _sort: MatSort
  ) {
    super();

    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<SubvendorSummary[]> {
    const displayDataChanges = [
      this.subvendorDB.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.subvendorDB.data
          .slice()
          .filter((item: SubvendorSummary) => {
            return item.name.toLowerCase().includes(this.filter.toLowerCase());
          });

        const sortedData = this.sortData(this.filteredData.slice());
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    this.subvendorDB.subvendorObs.unsubscribe();
  }

  sortData(data: SubvendorSummary[]): SubvendorSummary[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'subvendorName':
          [propertyA, propertyB] = [a.name.toLowerCase(), b.name.toLowerCase()];
          break;
        case 'amountDue':
          [propertyA, propertyB] = [+a.amountDue, +b.amountDue];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}

export class WorkerDB {
  workerObs: Subscription;
  dataChange: BehaviorSubject<WorkerSummary[]> = new BehaviorSubject<
    WorkerSummary[]
  >([]);
  get data(): WorkerSummary[] {
    return this.dataChange.value;
  }

  constructor(private core: CoreService) {
    this.workerObs = this.core.workers.subscribe(worker => {
      this.dataChange.next(worker);
    });
  }
}

export class WorkerDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: WorkerSummary[] = [];
  renderedData: WorkerSummary[] = [];

  constructor(
    private workerDB: WorkerDB,
    private _paginator: MatPaginator,
    private _sort: MatSort
  ) {
    super();

    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<WorkerSummary[]> {
    const displayDataChanges = [
      this.workerDB.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.workerDB.data
          .slice()
          .filter((item: WorkerSummary) => {
            return item.name.toLowerCase().includes(this.filter.toLowerCase());
          });

        const sortedData = this.sortData(this.filteredData.slice());
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    this.workerDB.workerObs.unsubscribe();
  }

  sortData(data: WorkerSummary[]): WorkerSummary[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'employeeName':
          [propertyA, propertyB] = [a.name.toLowerCase(), b.name.toLowerCase()];
          break;
        case 'presentThisMonth':
          [propertyA, propertyB] = [+a.presentThisMonth, +b.presentThisMonth];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}

export class CustomerDB {
  customerObs: Subscription;
  dataChange: BehaviorSubject<CustomerSummary[]> = new BehaviorSubject<
    CustomerSummary[]
  >([]);
  get data(): CustomerSummary[] {
    return this.dataChange.value;
  }

  constructor(private core: CoreService) {
    this.customerObs = this.core.customers.subscribe(customer => {
      this.dataChange.next(customer);
    });
  }
}

export class CustomerDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');
  get filter(): string {
    return this._filterChange.value;
  }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: CustomerSummary[] = [];
  renderedData: CustomerSummary[] = [];

  constructor(
    private customerDB: CustomerDB,
    private _paginator: MatPaginator,
    private _sort: MatSort
  ) {
    super();

    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  connect(): Observable<CustomerSummary[]> {
    const displayDataChanges = [
      this.customerDB.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.customerDB.data
          .slice()
          .filter((item: CustomerSummary) => {
            return item.name.toLowerCase().includes(this.filter.toLowerCase());
          });

        const sortedData = this.sortData(this.filteredData.slice());
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {
    this.customerDB.customerObs.unsubscribe();
  }

  sortData(data: CustomerSummary[]): CustomerSummary[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'customerName':
          [propertyA, propertyB] = [a.name.toLowerCase(), b.name.toLowerCase()];
          break;
        case 'creditAmount':
          [propertyA, propertyB] = [+a.creditAmount, +b.creditAmount];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
