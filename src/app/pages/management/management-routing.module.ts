import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementComponent } from './management.component';
import { EditDetailsComponent } from './edit-details/edit-details.component';
import { RoleGuard } from 'src/app/shared/guards/role.guard';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: '',
    component: ManagementComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'edit',
    component: EditDetailsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['admin', 'owner', 'receptionist']
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule {}
