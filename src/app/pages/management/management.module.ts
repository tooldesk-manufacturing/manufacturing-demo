import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ManagementComponent } from './management.component';
import { ManagementService } from './management.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserProfileComponent } from './profile/profile.component';
import { ManagementRoutingModule } from './management-routing.module';
import { EditDetailsComponent } from './edit-details/edit-details.component';
import { MonthlyAttendanceComponent } from './monthly-attendance/monthly-attendance.component';
import { AmountDueComponent } from './amount-due/amount-due.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { DeleteUserComponent } from './delete/delete.component';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    PerfectScrollbarModule,
    ManagementRoutingModule
  ],
  declarations: [
    ManagementComponent,
    UserProfileComponent,
    EditDetailsComponent,
    MonthlyAttendanceComponent,
    AmountDueComponent,
    DeleteUserComponent,
    RegisterComponent
  ],
  entryComponents: [
    UserProfileComponent,
    EditDetailsComponent,
    MonthlyAttendanceComponent,
    AmountDueComponent,
    DeleteUserComponent
  ],
  providers: [
    ManagementService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
  ]
})
export class ManagementModule {}
