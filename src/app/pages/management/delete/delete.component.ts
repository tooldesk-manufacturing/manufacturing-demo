import { Component, Inject } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ManagementService } from '../management.service';

import { ToastrService } from 'ngx-toastr';
import { CoreService } from 'src/app/shared/services/core.service';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteUserComponent {
  uid: string;
  roleType: string;

  enterPINForm: FormGroup = new FormGroup({
    currentPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<DeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mService: ManagementService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private core: CoreService
  ) {
    this.initData();
  }

  private initData() {
    this.uid = this.data.uid;
    this.roleType = this.data.roleType;
  }

  get formControls() {
    return this.enterPINForm.controls;
  }

  async deleteUser(uid: string, pin: string) {
    this.core.loadingText = 'Deleting user...';
    this.spinner.show();
    try {
      await this.mService.deleteUser(uid, this.roleType, pin);
      this.spinner.hide();
      this.toaster.success('Successful!', 'User deleted');
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Deleting user');
      this.dialogRef.close();
    }
  }
}
