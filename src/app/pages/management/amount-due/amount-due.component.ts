import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ManagementService } from '../management.service';
import { Observable } from 'rxjs';
import { Transaction, SearchOverride } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-amount-due',
  templateUrl: './amount-due.component.html',
  styleUrls: ['./amount-due.component.scss']
})
export class AmountDueComponent implements OnInit {
  uid: string;
  transactions: Observable<Transaction[]>;

  constructor(
    public dialogRef: MatDialogRef<AmountDueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mService: ManagementService,
    private core: CoreService,
    private router: Router
  ) {}

  ngOnInit() {
    this.intitData();
  }

  private intitData() {
    this.uid = this.data.uid;
    if (this.data.type === 'subvendor') {
      this.transactions = this.mService.getSubvendorAmountDue(this.uid);
    } else if (this.data.type === 'customer') {
      this.transactions = this.mService.getCustomerCreditAmount(this.uid);
    }
  }

  transactionInternalDC(internalDC: string) {
    const temp: SearchOverride = {
      searchField: 'internalDC',
      searchTerm: internalDC
    };
    this.core.transactionSearchOverrride = temp;
    this.router.navigate(['transactions']);
    this.dialogRef.close();
  }

  transactionBillNo(billNo: string) {
    const temp: SearchOverride = {
      searchField: 'billNo',
      searchTerm: billNo
    };
    this.core.transactionSearchOverrride = temp;
    this.router.navigate(['transactions']);
    this.dialogRef.close();
  }

  billBillNo(billNo: string) {
    const temp: SearchOverride = {
      searchField: 'billNo',
      searchTerm: billNo
    };
    this.core.billingSearchOverrride = temp;
    this.router.navigate(['billing']);
    this.dialogRef.close();
  }

  /*   orderOrderUID(orderUID: string) {
    const temp: SearchOverride = {
      searchField: 'orderUID',
      searchTerm: orderUID
    };
    this.core.orderSearchOverrride = temp;
    this.router.navigate(['order']);
    this.dialogRef.close();
  } */
}
