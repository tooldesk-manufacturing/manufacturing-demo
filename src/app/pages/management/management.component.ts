import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { MatDialog, MatTabChangeEvent } from '@angular/material';
import { ManagementService } from './management.service';
import { MatPaginator, MatSort } from '@angular/material';
import {
  SubvendorDataSource,
  SubvendorDB,
  CustomerDB,
  WorkerDB,
  WorkerDataSource,
  CustomerDataSource
} from './management.datasource';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { UserProfileComponent } from './profile/profile.component';
import { EditDetailsComponent } from './edit-details/edit-details.component';
import { MonthlyAttendanceComponent } from './monthly-attendance/monthly-attendance.component';

import { AmountDueComponent } from './amount-due/amount-due.component';
import { DeleteUserComponent } from './delete/delete.component';
import { LoadingBarService } from '@ngx-loading-bar/core';

import { ToastrService } from 'ngx-toastr';
import { CoreService } from 'src/app/shared/services/core.service';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import {
  CustomerSummary,
  WorkerSummary,
  SubvendorSummary
} from 'src/app/shared/models/models';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit, OnDestroy {
  userType: string;

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  @ViewChild('filter', { static: true })
  filter: ElementRef;

  displayedColumns: string[];
  selection = new SelectionModel<string>(true, []);
  dataSource: any;
  DB: any;

  tabIndex: number;
  filterValue = '';

  workerDB: WorkerDB;
  customerDB: CustomerDB;
  subvendorDB: SubvendorDB;
  filterObs: Subscription;

  constructor(
    private core: CoreService,
    private mService: ManagementService,
    private changeDetectorRefs: ChangeDetectorRef,
    private dialog: MatDialog,
    private loadingService: LoadingBarService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.getRouteParams();
    this.initData();
  }

  private initData() {
    this.filterObs = fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
    if (this.userType === undefined) {
      this.userType = 'worker';
      this.tabIndex = 0;
    }
    this.changeDataSource();
  }

  private getRouteParams() {
    if (
      this.core.URLParams &&
      !this.isEmpty(this.core.URLParams) &&
      this.core.initialURL.search('management') > -1
    ) {
      this.core.managementFilterOverride = {
        searchField: this.core.URLParams['searchField'],
        searchTerm: this.core.URLParams['searchTerm']
      };
      this.initUserFilter();
      this.core.URLParams = null;
      this.core.initialURL = null;
    }
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  private initUserFilter() {
    switch (this.core.managementFilterOverride.searchField) {
      case 'worker':
        this.userType = 'worker';
        this.tabIndex = 0;
        break;
      case 'customer':
        this.userType = 'customer';
        this.tabIndex = 1;
        break;
      case 'subvendor':
        this.userType = 'subvendor';
        this.tabIndex = 2;
        break;
    }
    if (this.core.managementFilterOverride.searchTerm.toString().length > 0) {
      this.filterValue = this.core.managementFilterOverride.searchTerm.toString();
    }
  }

  private selectDatasource() {
    if (this.userType === 'worker') {
      this.displayedColumns = [
        'select',
        'employeeName',
        'presentThisMonth',
        'employeeEdit'
      ];
      if (!this.workerDB) {
        this.workerDB = new WorkerDB(this.core);
      }
      this.DB = this.workerDB;
      this.dataSource = new WorkerDataSource(
        this.DB,
        this.paginator,
        this.sort
      );
    } else if (this.userType === 'customer') {
      this.displayedColumns = ['customerName', 'creditAmount', 'customerEdit'];
      if (!this.customerDB) {
        this.customerDB = new CustomerDB(this.core);
      }
      this.DB = this.customerDB;
      this.dataSource = new CustomerDataSource(
        this.DB,
        this.paginator,
        this.sort
      );
    } else if (this.userType === 'subvendor') {
      this.displayedColumns = ['subvendorName', 'amountDue', 'subvendorEdit'];
      if (!this.subvendorDB) {
        this.subvendorDB = new SubvendorDB(this.core);
      }
      this.DB = this.subvendorDB;
      this.dataSource = new SubvendorDataSource(
        this.DB,
        this.paginator,
        this.sort
      );
    }
    if (this.filterValue) {
      this.dataSource.filter = this.filterValue;
    }
    this.changeDetectorRefs.detectChanges();
  }

  setUser(uid: string) {
    const obs = this.mService.getSelectedUser(uid).subscribe(res => {
      res.uid = uid;
      this.dialog.open(EditDetailsComponent, {
        data: res
      });
      obs.unsubscribe();
    });
  }

  async deleteUser(summary: any) {
    let summaryRole: string;
    if (summary['type']) {
      summaryRole = summary['type'];
    } else {
      summaryRole = this.userType;
    }
    this.dialog.open(DeleteUserComponent, {
      data: { uid: summary.uid, roleType: summaryRole }
    });
  }

  isAllSelected(): boolean {
    if (!this.dataSource) {
      return false;
    }
    if (this.selection.isEmpty()) {
      return false;
    }

    if (this.filter.nativeElement.value) {
      return (
        this.selection.selected.length === this.dataSource.renderedData.length
      );
    } else {
      return this.selection.selected.length === this.DB.data.length;
    }
  }

  onLinkClick(event: MatTabChangeEvent) {
    if (event.tab.textLabel === 'Employees') {
      this.userType = 'worker';
      this.tabIndex = 0;
    } else if (event.tab.textLabel === 'Customers') {
      this.userType = 'customer';
      this.tabIndex = 1;
    } else if (event.tab.textLabel === 'Subcontractors') {
      this.userType = 'subvendor';
      this.tabIndex = 2;
    }
    this.changeDataSource();
  }

  masterToggle() {
    if (!this.dataSource) {
      return;
    }

    if (this.isAllSelected()) {
      this.selection.clear();
    } else if (this.filter.nativeElement.value) {
      this.dataSource.renderedData.forEach(data => {
        this.selection.select(data.uid);
      });
    } else {
      this.DB.data.forEach(data => {
        this.selection.select(data.uid);
      });
    }
  }

  changeDataSource() {
    this.loadingService.start();
    this.filter.nativeElement.value = '';
    this.selectDatasource();
    this.loadingService.stop();
  }

  openSubvendorList(uid: string) {
    this.dialog.open(AmountDueComponent, {
      data: { uid: uid, type: 'subvendor' }
    });
  }
  openCustomerList(uid: string) {
    this.dialog.open(AmountDueComponent, {
      data: { uid: uid, type: 'customer' }
    });
  }

  openUserProfile(uid: string) {
    const obs = this.mService.getUser(uid).subscribe(res => {
      this.dialog.open(UserProfileComponent, {
        data: res
      });
      obs.unsubscribe();
    });
  }

  openMonthlyAttendance(uid: string) {
    this.dialog.open(MonthlyAttendanceComponent, {
      data: uid
    });
  }

  async setAttendance() {
    this.core.loadingText = 'Marking attendance...';
    this.spinner.show();
    try {
      await this.mService.markAttendance(this.selection.selected);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Attendance marking');
      this.selection.clear();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Attendance marking');
      this.selection.clear();
    }
  }

  async setOutTime() {
    this.core.loadingText = 'Marking out time...';
    this.spinner.show();
    try {
      await this.mService.markOutTime(this.selection.selected);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Marking out time');
      this.selection.clear();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Marking out time');
      this.selection.clear();
    }
  }

  ngOnDestroy() {
    this.filterObs.unsubscribe();
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }
}
