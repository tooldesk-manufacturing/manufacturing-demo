import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ToastrService } from 'ngx-toastr';
import {
  Department,
  UsersLimit,
  IndividualUserLimit
} from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { matchOtherValidator } from 'src/app/shared/validators/match-other.validator';
import { take } from 'rxjs/operators';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { ManagementService } from '../management.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  userType: string;
  subType: string;
  departments = new Array<Department>();
  usersLimit: UsersLimit;
  uploadingImages = false;

  registerForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6)
    ]),
    confirmPassword: new FormControl('', [
      matchOtherValidator('password'),
      Validators.required
    ]),
    photoURL: new FormControl(),
    address: new FormControl(''),
    phoneNumber: new FormControl('', [
      Validators.minLength(10),
      Validators.maxLength(10)
    ]),
    landlineNumber: new FormControl('', [
      Validators.minLength(8),
      Validators.maxLength(11)
    ]),
    department: new FormControl(''),
    gstin: new FormControl('', [
      Validators.minLength(15),
      Validators.maxLength(15),
      Validators.required
    ]),
    tin: new FormControl('', [
      Validators.minLength(11),
      Validators.maxLength(11)
    ]),
    esugam: new FormControl(''),
    stateCode: new FormControl('', [
      Validators.minLength(2),
      Validators.maxLength(2)
    ]),
    credit: new FormControl(false),
    creditPeriod: new FormControl(''),
    salary: new FormControl()
  });

  constructor(
    private managementService: ManagementService,
    private spinner: SpinnerOverlayService,
    private core: CoreService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
    this.initUserLimit();
  }

  private async initData() {
    this.userType = 'worker';
    const departments = await this.core.departments.pipe(take(1)).toPromise();
    departments.list.forEach(department => {
      this.departments.push(department);
    });
    this.registerForm.controls['gstin'].setValue('               ');
  }

  private async initUserLimit() {
    this.usersLimit = await this.core.userLimit.pipe(take(1)).toPromise();
  }

  private getUserDetails() {
    const userDetails: any = {
      displayName: this.registerForm.controls['name'].value,
      email: this.registerForm.controls['email'].value,
      address: this.registerForm.controls['address'].value,
      password: this.registerForm.controls['password'].value,
      phoneNumber: this.registerForm.controls['phoneNumber'].value,
      landlineNumber: this.registerForm.controls['landlineNumber'].value,
      photoURL: this.registerForm.controls['photoURL'].value
    };
    switch (this.userType) {
      case 'customer':
        userDetails.role = { customer: true };
        userDetails.creditPeriod = this.registerForm.controls[
          'creditPeriod'
        ].value;
        userDetails.credit = this.registerForm.controls['credit'].value;
        userDetails.gstin = this.registerForm.controls['gstin'].value;
        userDetails.tin = this.registerForm.controls['tin'].value;
        userDetails.esugam = this.registerForm.controls['esugam'].value;
        userDetails.stateCode = this.registerForm.controls['stateCode'].value;
        break;
      case 'subvendor':
        userDetails.role = { subvendor: true };
        userDetails.creditPeriod = this.registerForm.controls[
          'creditPeriod'
        ].value;
        userDetails.credit = this.registerForm.controls['credit'].value;
        userDetails.gstin = this.registerForm.controls['gstin'].value;
        userDetails.tin = this.registerForm.controls['tin'].value;
        userDetails.esugam = this.registerForm.controls['esugam'].value;
        userDetails.stateCode = this.registerForm.controls['stateCode'].value;
        break;
      case 'owner':
        userDetails.role = { owner: true };
        delete userDetails.gstin;
        break;
      case 'worker':
        userDetails.role = { worker: true };
        userDetails.salary = this.registerForm.controls['salary'].value;
        userDetails.department = this.registerForm.controls['department'].value;
        delete userDetails.gstin;
    }
    if (this.subType) {
      switch (this.subType) {
        case 'receptionist':
          userDetails.role = { worker: true, receptionist: true };
          break;
        case 'inspector':
          userDetails.role = { worker: true, inspector: true };
      }
    }
    return userDetails;
  }

  get formControls() {
    return this.registerForm.controls;
  }

  clearForm() {
    this.registerForm.reset();
    this.registerForm.controls['gstin'].setValue('               ');
  }

  onChangeCredit(event: any) {
    if (event.checked === true) {
      this.registerForm.controls['credit'].setValue(true);
    } else {
      this.registerForm.controls['credit'].setValue(false);
    }
  }

  checkLimit(): boolean {
    if (this.usersLimit) {
      if (this.userType === 'worker') {
        if (this.subType === 'receptionist') {
          return this.checkRoleLimit(this.usersLimit.receptionist);
        } else if (this.subType === 'inspector') {
          return this.checkRoleLimit(this.usersLimit.inspector);
        } else if (this.subType === 'worker') {
          return this.checkRoleLimit(this.usersLimit.worker);
        }
      } else if (this.userType === 'customer') {
        return this.checkRoleLimit(this.usersLimit.customer);
      } else if (this.userType === 'subvendor') {
        return this.checkRoleLimit(this.usersLimit.subvendor);
      } else if (this.userType === 'owner') {
        return this.checkRoleLimit(this.usersLimit.owner);
      } else if (this.userType === 'admin') {
        return this.checkRoleLimit(this.usersLimit.admin);
      }
    }
  }

  checkRoleLimit(userLimit: IndividualUserLimit): boolean {
    if (userLimit.infinite) {
      return true;
    } else if (userLimit.current < userLimit.max) {
      return true;
    }
    return false;
  }

  onLinkClick(event: MatTabChangeEvent) {
    if (event.tab.textLabel === 'Employee') {
      this.userType = 'worker';
    } else if (event.tab.textLabel === 'Customer') {
      this.userType = 'customer';
    } else if (event.tab.textLabel === 'Subcontractor') {
      this.userType = 'subvendor';
    } else if (event.tab.textLabel === 'Owner') {
      this.userType = 'owner';
    }
    this.clearForm();
    if (this.userType === 'customer') {
      this.registerForm.controls['gstin'].setValue(null);
      this.registerForm.updateValueAndValidity();
    }
  }

  async uploadPhoto(event: any) {
    this.uploadingImages = true;
    const file = event.target.files.item(0);
    const today = new Date();
    const name =
      today.getFullYear().toString() +
      '-' +
      today.getMonth().toString() +
      '-' +
      today.getDate().toString() +
      '-' +
      today.getHours().toString() +
      ':' +
      today.getMinutes().toString() +
      '-' +
      today.getSeconds().toString() +
      file.name;
    try {
      await this.managementService.uploadPhoto(file, name);
      const path = await this.managementService.getFileURL(name);
      this.registerForm.controls['photoURL'].setValue(path);
      this.registerForm.controls['photoURL'].markAsDirty();
      this.toaster.success('Successful!', 'Image upload');
      this.uploadingImages = false;
    } catch (reason) {
      this.toaster.error(reason, 'Image upload');
      this.uploadingImages = false;
    }
  }

  async createUser() {
    this.core.loadingText = 'Registering new user...';
    this.spinner.show();
    try {
      const userDetails = this.getUserDetails();
      await this.managementService.registerUser(userDetails);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Registered a new user');
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Register new user');
    }
  }
}
