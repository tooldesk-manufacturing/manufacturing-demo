import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserProfileComponent } from '../profile/profile.component';
import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  MatDatepicker
} from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';
import { ManagementService } from '../management.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY'
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};
@Component({
  selector: 'app-monthly-attendance',
  templateUrl: './monthly-attendance.component.html',
  styleUrls: ['./monthly-attendance.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class MonthlyAttendanceComponent implements OnInit {
  userID: string;
  attendance: Observable<{}>;
  presentDays: number;
  searchDateForm: FormGroup = new FormGroup({
    fromDate: new FormControl(moment(), [Validators.required])
  });

  constructor(
    public dialogRef: MatDialogRef<MonthlyAttendanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private managementService: ManagementService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.userID = this.data;
    this.getAttendance();
  }

  initialDataSource() {
    const today = moment();
    const ctrlValue = this.searchDateForm.controls['fromDate'].value;
    ctrlValue.year(today.year());
    ctrlValue.month(today.month());
    this.searchDateForm.controls['fromDate'].setValue(ctrlValue);
    this.getAttendance();
  }

  get formControls() {
    return this.searchDateForm.controls;
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.searchDateForm.controls['fromDate'].value;
    ctrlValue.year(normalizedYear.year());
    this.searchDateForm.controls['fromDate'].setValue(ctrlValue);
  }

  chosenMonthHandler(
    normalizedMonth: Moment,
    datepicker: MatDatepicker<Moment>
  ) {
    const ctrlValue = this.searchDateForm.controls['fromDate'].value;
    ctrlValue.month(normalizedMonth.month());
    this.searchDateForm.controls['fromDate'].setValue(ctrlValue);
    datepicker.close();
  }

  getAttendance() {
    this.presentDays = 0;
    const date = new Date();
    date.setMonth(
      (this.searchDateForm.controls['fromDate'].value as Moment).month()
    );
    date.setFullYear(
      (this.searchDateForm.controls['fromDate'].value as Moment).year()
    );
    this.attendance = this.managementService
      .getMonthlyAttendance(this.userID, date)
      .pipe(
        tap(list => {
          if (list) {
            this.presentDays = Object.keys(list).length;
          }
        })
      );
  }
}
