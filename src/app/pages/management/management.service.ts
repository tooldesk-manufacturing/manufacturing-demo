import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import {
  User,
  Transaction,
  Roles,
  IndividualUserLimit
} from 'src/app/shared/models/models';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AngularFireStorage } from '@angular/fire/storage';
import { take, shareReplay } from 'rxjs/operators';
import { CoreService } from 'src/app/shared/services/core.service';
import { Router } from '@angular/router';
@Injectable()
export class ManagementService {
  selectedUser: User;
  today = new Date();
  attendance = '';
  updateData = '';

  constructor(
    private afFunctions: AngularFireFunctions,
    private afStorage: AngularFireStorage,
    private afStore: AngularFirestore,
    private core: CoreService,
    private router: Router
  ) {}

  getFileURL(file: string): Promise<any> {
    const storageRef = this.afStorage.ref(`profiles/${file}`);
    return storageRef.getDownloadURL().toPromise();
  }

  uploadPhoto(file: File, name: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStorage.ref(`profiles/${name}`).put(file);
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  markAttendance(users: string[]): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const markPresent = this.afFunctions.httpsCallable('markPresent');
        await markPresent(users).toPromise();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  markOutTime(users: string[]): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const markOutTime = this.afFunctions.httpsCallable('markOutTime');
        await markOutTime(users).toPromise();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  async deleteUser(
    uid: string,
    roleType: string,
    pin: string
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const deleteUserFunction = this.afFunctions.httpsCallable('deleteUser');
        await deleteUserFunction({
          pin: pin,
          uid: uid,
          roleType: roleType
        }).toPromise();
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  getSelectedUser(uid: string): Observable<User> {
    return this.afStore
      .collection('users')
      .doc<User>(uid)
      .valueChanges();
  }

  getUser(uid: string): Observable<User> {
    return this.afStore
      .collection('users')
      .doc<User>(uid)
      .valueChanges();
  }

  getMonthlyAttendance(uid: string, date: Date) {
    return this.afStore
      .collection('users')
      .doc(uid)
      .collection('attendance')
      .doc(
        date.getFullYear().toString() + '-' + (date.getMonth() + 1).toString()
      )
      .valueChanges()
      .pipe(shareReplay(1));
  }

  updateUserData(uid: string, userDetails: User): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStore
          .collection('users')
          .doc(uid)
          .set(userDetails, { merge: true });
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  getSubvendorAmountDue(subvendorUID: string): Observable<Transaction[]> {
    return this.afStore
      .collection<Transaction>('transactions', ref =>
        ref
          .where('paymentReceived', '==', false)
          .where('subvendorUID', '==', subvendorUID)
          .orderBy('createdAt', 'desc')
      )
      .valueChanges();
  }

  getCustomerCreditAmount(customerUID: string): Observable<Transaction[]> {
    return this.afStore
      .collection<Transaction>('transactions', ref =>
        ref
          .where('paymentReceived', '==', false)
          .where('customerUID', '==', customerUID)
          .orderBy('createdAt', 'desc')
      )
      .valueChanges();
  }

  private async checkUserLimit(userDetails: any): Promise<string> {
    const roles = (userDetails.role as any) as Roles;
    const userLimit = await this.core.userLimit.pipe(take(1)).toPromise();
    if (Object.keys(userDetails.role as Roles).length === 1) {
      if (roles.admin) {
        return this.checkRoleLimit(userLimit.admin, 'admin');
      } else if (roles.owner) {
        return this.checkRoleLimit(userLimit.owner, 'owner');
      } else if (roles.customer) {
        return this.checkRoleLimit(userLimit.customer, 'customer');
      } else if (roles.subvendor) {
        return this.checkRoleLimit(userLimit.subvendor, 'subvendor');
      } else if (roles.worker) {
        return this.checkRoleLimit(userLimit.worker, 'worker');
      }
    } else {
      if (roles.inspector) {
        return this.checkRoleLimit(userLimit.inspector, 'inspector');
      } else if (roles.receptionist) {
        return this.checkRoleLimit(userLimit.receptionist, 'receptionist');
      }
    }
  }

  checkRoleLimit(
    userLimit: IndividualUserLimit,
    role: string
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      if (userLimit.infinite) {
        resolve(role);
      } else if (userLimit.current < userLimit.max) {
        resolve(role);
      }
      reject();
    });
  }

  async registerUser(
    userDetails: firebase.User & { password: string }
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const role = await this.checkUserLimit(userDetails);
        const data = this.returnUserDetails(userDetails) as firebase.User & {
          password: string;
          userType: string;
        };
        data.userType = role;
        const createUser = this.afFunctions.httpsCallable('createUser');
        await createUser(data).toPromise();
        await this.router.navigate(['management']);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  returnUserDetails(userDetails): firebase.User & { password: string } {
    const data: any = {};
    if (userDetails.uid) {
      data.uid = userDetails.uid;
    }
    if (userDetails.email) {
      data.email = userDetails.email;
    }
    if (userDetails.password) {
      data.password = userDetails.password;
    }
    if (userDetails.displayName) {
      data.displayName = userDetails.displayName;
    }
    if (userDetails.emailVerified) {
      data.emailVerified = userDetails.emailVerified;
    }
    if (userDetails.department) {
      data.department = userDetails.department;
    }
    if (userDetails.creditPeriod) {
      data.creditPeriod = userDetails.creditPeriod;
    }
    if (userDetails.role) {
      data.roles = userDetails.role;
    }
    if (userDetails.photoURL) {
      data.photoURL = userDetails.photoURL;
    }
    if (userDetails.phoneNumber) {
      data.phoneNumber = userDetails.phoneNumber;
    }
    if (userDetails.address) {
      data.address = userDetails.address;
    }
    if (userDetails.salary) {
      data.salary = userDetails.salary;
    }
    if (userDetails.landlineNumber) {
      data.landlineNumber = userDetails.landlineNumber;
    }
    if (userDetails.gstin) {
      data.gstin = userDetails.gstin;
    }
    if (userDetails.tin) {
      data.tin = userDetails.tin;
    }
    if (userDetails.hsnCode) {
      data.hsnCode = userDetails.hsnCode;
    }
    if (userDetails.credit) {
      data.credit = userDetails.credit;
    }
    if (userDetails.esugam) {
      data.esugam = userDetails.esugam;
    }
    if (userDetails.stateCode) {
      data.stateCode = userDetails.stateCode;
    }
    return data;
  }
}
