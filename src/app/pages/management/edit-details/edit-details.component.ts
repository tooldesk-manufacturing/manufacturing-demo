import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ManagementService } from '../management.service';

import { ToastrService } from 'ngx-toastr';

import { Department } from 'src/app/shared/models/models';
import { CoreService } from 'src/app/shared/services/core.service';
import { take } from 'rxjs/operators';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-edit-details',
  templateUrl: './edit-details.component.html',
  styleUrls: ['./edit-details.component.scss']
})
export class EditDetailsComponent implements OnInit {
  user: any;
  departments = new Array<Department>();
  editForm: FormGroup = new FormGroup({
    photoURL: new FormControl(),
    address: new FormControl(''),
    phoneNumber: new FormControl('', [
      Validators.minLength(10),
      Validators.maxLength(10)
    ]),
    landlineNumber: new FormControl('', [
      Validators.minLength(8),
      Validators.maxLength(11)
    ]),
    gstin: new FormControl('', [
      Validators.minLength(15),
      Validators.maxLength(15)
    ]),
    department: new FormControl(''),
    tin: new FormControl('', [
      Validators.minLength(11),
      Validators.maxLength(11)
    ]),
    esugam: new FormControl(''),
    stateCode: new FormControl('', [
      Validators.minLength(2),
      Validators.maxLength(2)
    ]),
    SACCode: new FormControl(),
    credit: new FormControl(false),
    creditPeriod: new FormControl(''),
    salary: new FormControl()
  });
  uploadingImages = false;

  constructor(
    public dialogRef: MatDialogRef<EditDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mService: ManagementService,
    private core: CoreService,
    private toaster: ToastrService,
    private spinner: SpinnerOverlayService
  ) {}
  ngOnInit() {
    this.initData();
  }

  private async initData() {
    this.user = this.data;
    this.mService.updateData = '';
    const currentDepartments = await this.core.departments
      .pipe(take(1))
      .toPromise();
    currentDepartments.list.forEach(department => {
      this.departments.push(department);
    });
  }

  private getUserDetails() {
    const userDetails: any = {};
    if (this.editForm.controls['address'].dirty) {
      userDetails.address = this.editForm.controls['address'].value;
    }
    if (this.editForm.controls['phoneNumber'].dirty) {
      userDetails.phoneNumber = this.editForm.controls['phoneNumber'].value;
    }
    if (this.editForm.controls['landlineNumber'].dirty) {
      userDetails.landlineNumber = this.editForm.controls[
        'landlineNumber'
      ].value;
    }
    if (this.editForm.controls['photoURL'].dirty) {
      userDetails.photoURL = this.editForm.controls['photoURL'].value;
    }
    if (this.editForm.controls['salary'].dirty) {
      userDetails.salary = this.editForm.controls['salary'].value;
    }
    if (this.editForm.controls['creditPeriod'].dirty) {
      userDetails.creditPeriod = this.editForm.controls['creditPeriod'].value;
    }
    if (this.editForm.controls['credit'].dirty) {
      userDetails.credit = this.editForm.controls['credit'].value;
    }
    if (this.editForm.controls['gstin'].dirty) {
      userDetails.gstin = this.editForm.controls['gstin'].value;
    }
    if (this.editForm.controls['department'].dirty) {
      userDetails.department = this.editForm.controls['department'].value;
    }
    if (this.editForm.controls['tin'].dirty) {
      userDetails.tin = this.editForm.controls['tin'].value;
    }
    if (this.editForm.controls['esugam'].dirty) {
      userDetails.esugam = this.editForm.controls['esugam'].value;
    }
    if (this.editForm.controls['stateCode'].dirty) {
      userDetails.stateCode = this.editForm.controls['stateCode'].value;
    }
    if (this.editForm.controls['SACCode'].dirty) {
      userDetails.SACCode = this.editForm.controls['SACCode'].value;
    }
    return userDetails;
  }

  get formControls() {
    return this.editForm.controls;
  }

  clearForm() {
    this.editForm.reset();
  }

  onChangeCredit(event: any) {
    if (event.checked === true) {
      this.editForm.controls['credit'].setValue(true);
    } else {
      this.editForm.controls['credit'].setValue(false);
    }
  }

  isWorker() {
    if (this.user.roles.customer || this.user.roles.subvendor) {
      return false;
    } else {
      return true;
    }
  }

  async uploadPhoto(event: any) {
    this.uploadingImages = true;
    const file = event.target.files.item(0);
    const today = new Date();
    const name =
      today.getFullYear().toString() +
      '-' +
      today.getMonth().toString() +
      '-' +
      today.getDate().toString() +
      '-' +
      today.getHours().toString() +
      ':' +
      today.getMinutes().toString() +
      '-' +
      today.getSeconds().toString() +
      file.name;
    try {
      await this.mService.uploadPhoto(file, name);
      const path = await this.mService.getFileURL(name);
      this.editForm.controls['photoURL'].setValue(path);
      this.editForm.controls['photoURL'].markAsDirty();
      this.toaster.success('Successful!', 'Image upload');
      this.uploadingImages = false;
    } catch (reason) {
      this.toaster.error(reason, 'Image upload');
      this.uploadingImages = false;
    }
  }

  async updateUser() {
    this.core.loadingText = 'Updating user details...';
    this.spinner.show();
    try {
      const userDetails = this.getUserDetails();
      if (Object.keys(userDetails).length > 0) {
        await this.mService.updateUserData(this.user.uid, userDetails);
        this.spinner.hide();
        this.toaster.success('Successful!', 'Update user data');
        this.dialogRef.close();
      } else {
        this.dialogRef.close();
      }
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Update user data');
    }
  }
}
