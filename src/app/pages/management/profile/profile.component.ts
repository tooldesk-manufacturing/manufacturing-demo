import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  user: any;
  mapsURL = 'https://www.google.com/maps/search/?api=1';
  constructor(
    public dialogRef: MatDialogRef<UserProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.user = this.data;
    this.mapsURLConverter();
  }

  private mapsURLConverter() {
    if (this.user.address) {
      const temp: string = this.user.address;
      temp.replace(' ', '+');
      temp.replace('#', '');
      temp.replace(',', '%2C');
      this.mapsURL = 'https://www.google.com/maps/search/?api=1&query=' + temp;
    }
  }

  isWorker() {
    if (this.user.roles.customer || this.user.roles.subvendor) {
      return false;
    } else {
      return true;
    }
  }
}
