import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { startWith, map, take } from 'rxjs/operators';
import {
  TransactionDataSource,
  TransactionsDB
} from './transactions.datasource';
import { TransactionsService } from './transactions.service';
import {
  Transaction,
  CustomerSummary,
  SubvendorSummary
} from '../../shared/models/models';
import { DetailsComponent } from './details/details.component';
import { AddTransactionComponent } from './add-transaction/add-transaction.component';
import { DeleteTransactionComponent } from './delete/delete.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as firebase from 'firebase/app';
import { Timestamp } from '@firebase/firestore-types';
import { LoadingBarService } from '@ngx-loading-bar/core';

import { ToastrService } from 'ngx-toastr';
import { CoreService } from 'src/app/shared/services/core.service';
import { dateCompareValidator } from 'src/app/shared/validators/date-compare.validator';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { UpdatePaymentComponent } from './update-payment/update-payment.component';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  displayedColumns: string[];
  DB: TransactionsDB;
  dataSource: TransactionDataSource;

  customers: CustomerSummary[] = [];
  customerOptions: string[] = [];
  customerFilteredOptions: Observable<string[]>;

  subvendors: SubvendorSummary[] = [];
  subvendorOptions: string[] = [];
  subvendorFilteredOptions: Observable<string[]>;

  searchTransactionForm: FormGroup = new FormGroup({
    searchTerm: new FormControl('', [Validators.required]),
    searchField: new FormControl('', [Validators.required]),
    fromDate: new FormControl(''),
    toDate: new FormControl('')
  });

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private tService: TransactionsService,
    private changeDetectorRefs: ChangeDetectorRef,
    private dialog: MatDialog,
    private core: CoreService,
    private loadingService: LoadingBarService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.getRouteParams();
    this.initColumns();
    this.initialDataSource();
    this.getNames();
    this.initForm();
  }

  private initColumns() {
    this.isHandset$.subscribe(isHandset => {
      if (isHandset) {
        this.displayedColumns = [
          'type',
          'createdAt',
          'cash',
          'credit',
          'details'
        ];
      } else {
        this.displayedColumns = [
          'type',
          'createdAt',
          'description',
          'cash',
          'credit',
          'paymentReceived',
          'details'
        ];
      }
    });
  }

  private initForm() {
    const today = new Date();
    this.searchTransactionForm.controls['fromDate'].setValue(today);
    this.searchTransactionForm.controls['toDate'].setValue(today);
  }

  private getRouteParams() {
    if (
      this.core.URLParams &&
      !this.isEmpty(this.core.URLParams) &&
      this.core.initialURL.search('transaction') > -1
    ) {
      this.core.transactionSearchOverrride = {
        searchField: this.core.URLParams['searchField'],
        searchTerm: this.core.URLParams['searchTerm']
      };
      this.core.URLParams = null;
      this.core.initialURL = null;
    }
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  private getNames() {
    this.subscriptions.push(
      this.core.subvendors.subscribe(res => {
        this.subvendors = res;
        this.subvendorOptions = res.map(subvendor => subvendor.name);
      })
    );
    this.subscriptions.push(
      this.core.customers.subscribe(res => {
        this.customers = res;
        this.customerOptions = res.map(customer => customer.name);
      })
    );
    this.customerFilteredOptions = this.searchTransactionForm
      .get('searchTerm')
      .valueChanges.pipe(
        startWith(''),
        map(val => this.filterCustomerNames(val))
      );
    this.subvendorFilteredOptions = this.searchTransactionForm
      .get('searchTerm')
      .valueChanges.pipe(
        startWith(''),
        map(val => this.filterSubvendorNames(val))
      );
  }

  private filterCustomerNames(val: string): string[] {
    if (
      this.searchTransactionForm.controls['searchField'].value === 'createdAt'
    ) {
      return this.customerOptions;
    }
    return this.customerOptions.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  private filterSubvendorNames(val: string): string[] {
    if (
      this.searchTransactionForm.controls['searchField'].value === 'createdAt'
    ) {
      return this.subvendorOptions;
    }
    return this.subvendorOptions.filter(option =>
      option.toLowerCase().includes(val.toLowerCase())
    );
  }

  private selectDatasource(
    overrideSearchField?: string,
    overrideSearchTerm?: Date | string
  ) {
    if (!overrideSearchField || !overrideSearchTerm) {
      if (
        this.searchTransactionForm.controls['searchField'].value ===
        'customerUID'
      ) {
        overrideSearchTerm = this.customers.find(
          x =>
            x.name === this.searchTransactionForm.controls['searchTerm'].value
        ).uid;
        this.DB = new TransactionsDB(
          this.tService,
          this.searchTransactionForm.controls['searchField'].value,
          overrideSearchTerm,
          this.searchTransactionForm.controls['fromDate'].value,
          this.searchTransactionForm.controls['toDate'].value
        );
      } else if (
        this.searchTransactionForm.controls['searchField'].value ===
          'createdAt' ||
        this.searchTransactionForm.controls['searchField'].value ===
          'receivedAt'
      ) {
        this.DB = new TransactionsDB(
          this.tService,
          this.searchTransactionForm.controls['searchField'].value,
          null,
          this.searchTransactionForm.controls['fromDate'].value,
          this.searchTransactionForm.controls['toDate'].value
        );
      } else if (
        this.searchTransactionForm.controls['searchField'].value ===
        'subvendorUID'
      ) {
        overrideSearchTerm = this.subvendors.find(
          x =>
            x.name === this.searchTransactionForm.controls['searchTerm'].value
        ).uid;
        this.DB = new TransactionsDB(
          this.tService,
          this.searchTransactionForm.controls['searchField'].value,
          overrideSearchTerm
        );
      } else {
        this.DB = new TransactionsDB(
          this.tService,
          this.searchTransactionForm.controls['searchField'].value,
          this.searchTransactionForm.controls['searchTerm'].value
        );
      }
    } else {
      this.DB = new TransactionsDB(
        this.tService,
        overrideSearchField,
        overrideSearchTerm
      );
    }
    this.dataSource = new TransactionDataSource(
      this.DB,
      this.paginator,
      this.sort
    );
  }

  get formControls() {
    return this.searchTransactionForm.controls;
  }

  clearForm() {
    this.searchTransactionForm.controls['searchTerm'].setValue('');
    if (
      this.searchTransactionForm.controls['searchField'].value === 'customerUID'
    ) {
      this.searchTransactionForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchTransactionForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else if (
      this.searchTransactionForm.controls['searchField'].value ===
        'createdAt' ||
      this.searchTransactionForm.controls['searchField'].value === 'receivedAt'
    ) {
      this.searchTransactionForm.controls['searchTerm'].setValue('   ');
      this.searchTransactionForm.controls['fromDate'].setValidators([
        Validators.required
      ]);
      this.searchTransactionForm.controls['toDate'].setValidators([
        Validators.required,
        dateCompareValidator('fromDate')
      ]);
    } else {
      this.searchTransactionForm.controls['fromDate'].setValidators([]);
      this.searchTransactionForm.controls['toDate'].setValidators([]);
    }
    this.searchTransactionForm.updateValueAndValidity();
  }

  checkCustomerName() {
    const val = this.searchTransactionForm.controls['searchTerm'].value;
    if (!this.customerOptions.includes(val)) {
      this.searchTransactionForm.controls['searchTerm'].setErrors({
        inList: true
      });
      this.searchTransactionForm.updateValueAndValidity();
    }
  }

  checkSubvendorName() {
    const val = this.searchTransactionForm.controls['searchTerm'].value;
    if (!this.subvendorOptions.includes(val)) {
      this.searchTransactionForm.controls['searchTerm'].setErrors({
        inList: true
      });
      this.searchTransactionForm.updateValueAndValidity();
    }
  }

  initialDataSource() {
    if (this.core.transactionSearchOverrride) {
      this.changeDataSource(
        this.core.transactionSearchOverrride.searchField,
        this.core.transactionSearchOverrride.searchTerm
      );
      this.searchTransactionForm.controls['searchField'].setValue(
        this.core.transactionSearchOverrride.searchField
      );
      this.searchTransactionForm.controls['searchTerm'].setValue(
        this.core.transactionSearchOverrride.searchTerm
      );
      this.core.transactionSearchOverrride = null;
    } else {
      const today = new Date();
      this.searchTransactionForm.controls['searchField'].setValue('receivedAt');
      this.searchTransactionForm.controls['searchTerm'].setValue('  ');
      this.searchTransactionForm.controls['fromDate'].setValue(today);
      this.searchTransactionForm.controls['toDate'].setValue(today);
      this.changeDataSource();
    }
  }

  changeDataSource(
    overrideSearchField?: string,
    overrideSearchTerm?: string | Date
  ) {
    this.loadingService.start();
    this.initColumns();
    this.selectDatasource(overrideSearchField, overrideSearchTerm);
    this.changeDetectorRefs.detectChanges();
    this.loadingService.stop();
  }

  deleteTransaction(transaction: Transaction) {
    this.dialog.open(DeleteTransactionComponent, {
      data: transaction.id
    });
  }

  addTransactionDialog() {
    this.dialog.open(AddTransactionComponent);
  }

  openTransaction(transaction: Transaction) {
    this.dialog.open(DetailsComponent, {
      data: transaction
    });
  }

  paymentReceived(transaction: Transaction, transactionType: string) {
    this.dialog.open(UpdatePaymentComponent, {
      data: {
        transaction: transaction,
        transactionType: transactionType
      }
    });
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }
}
