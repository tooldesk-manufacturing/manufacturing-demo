import { Component, Inject, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TransactionsService } from '../transactions.service';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteTransactionComponent implements OnInit {
  transactionUID: string;
  enterPINForm: FormGroup = new FormGroup({
    currentPIN: new FormControl('', [
      Validators.minLength(6),
      Validators.maxLength(6),
      Validators.required
    ])
  });

  constructor(
    public dialogRef: MatDialogRef<DeleteTransactionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private tService: TransactionsService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.initData();
  }

  private initData() {
    this.transactionUID = this.data;
  }

  get formControls() {
    return this.enterPINForm.controls;
  }

  async deleteTransaction(transactionUID: string, pin: string) {
    this.core.loadingText = 'Deleting transaction...';
    this.spinner.show();
    try {
      await this.tService.deleteTransaction(transactionUID, pin);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Transaction deleted');
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Deleting transaction');
      this.dialogRef.close();
    }
  }
}
