import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  Transaction,
  SubvendorSummary,
  CustomerSummary
} from 'src/app/shared/models/models';
import { AngularFireFunctions } from '@angular/fire/functions';

@Injectable()
export class TransactionsService {
  constructor(
    private afFunctions: AngularFireFunctions,
    private afStore: AngularFirestore
  ) {}

  getTransactions(
    searchField: string,
    searchTerm: string | Date,
    fromDate?: Date,
    toDate?: Date
  ): Observable<Transaction[]> {
    if (searchField === 'createdAt') {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Transaction>('transactions', ref =>
          ref
            .where(searchField, '>', yesterday)
            .where(searchField, '<', tomorrow)
            .orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (
      searchField === 'customerDC' ||
      searchField === 'internalDC' ||
      searchField === 'orderUID'
    ) {
      return this.afStore
        .collection<Transaction>('transactions', ref =>
          ref
            .where(searchField, 'array-contains', searchTerm)
            .orderBy('createdAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField === 'receivedAt') {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Transaction>('transactions', ref =>
          ref
            .where(searchField, '>', yesterday)
            .where(searchField, '<', tomorrow)
            .orderBy('receivedAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField === 'customerUID' && fromDate && toDate) {
      const yesterday = new Date(fromDate);
      yesterday.setHours(0, 0, 0);
      const tomorrow = new Date(toDate);
      tomorrow.setHours(23, 59, 59);
      return this.afStore
        .collection<Transaction>('transactions', ref =>
          ref
            .where(searchField, '==', searchTerm)
            .where('createdAt', '>', yesterday)
            .where('createdAt', '<', tomorrow)
            .orderBy('createdAt', 'asc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    } else if (searchField !== 'customerUID') {
      return this.afStore
        .collection<Transaction>('transactions', ref =>
          ref.where(searchField, '==', searchTerm).orderBy('createdAt', 'desc')
        )
        .valueChanges()
        .pipe(shareReplay(1));
    }
  }

  getAmountDue(uid: string): Observable<SubvendorSummary> {
    return this.afStore
      .collection('subvendorSummary')
      .doc<SubvendorSummary>(uid)
      .valueChanges();
  }

  getCustomerSummary(uid: string): Observable<CustomerSummary> {
    return this.afStore
      .collection('customerSummary')
      .doc<CustomerSummary>(uid)
      .valueChanges();
  }

  addTransaction(transaction: Transaction): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStore
          .collection('transactions')
          .doc(transaction.id)
          .set(transaction);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  updatePaymentReceived(transaction: Transaction): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.afStore
          .collection('transactions')
          .doc(transaction.id)
          .update(transaction);
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  async updatePaymentDoneSubvendor(
    transaction: Transaction,
    summary: SubvendorSummary
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const batch = this.afStore.firestore.batch();
        const tRef = this.afStore.collection('transactions').doc(transaction.id)
          .ref;
        batch.update(tRef, transaction);
        const summaryRef = this.afStore
          .collection('subvendorSummary')
          .doc(transaction.subvendorUID).ref;
        batch.set(
          summaryRef,
          { amountDue: summary.amountDue - transaction.amount },
          { merge: true }
        );
        await batch.commit();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }

  updatePaymentDoneCustomer(
    transaction: Transaction,
    summary: CustomerSummary
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const batch = this.afStore.firestore.batch();
        const tRef = this.afStore.collection('transactions').doc(transaction.id)
          .ref;
        batch.update(tRef, transaction);
        const summaryRef = this.afStore
          .collection('customerSummary')
          .doc(transaction.customerUID).ref;
        batch.set(
          summaryRef,
          { creditAmount: summary.creditAmount - transaction.amount },
          { merge: true }
        );
        await batch.commit();
        resolve();
      } catch (reason) {
        reject();
      }
    });
  }

  deleteTransaction(transactionUID: string, pin: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        const deleteOrderFunction = this.afFunctions.httpsCallable(
          'deleteTransaction'
        );
        await deleteOrderFunction({
          pin: pin,
          transactionUID: transactionUID
        }).toPromise();
        resolve();
      } catch (reason) {
        reject(reason);
      }
    });
  }
}
