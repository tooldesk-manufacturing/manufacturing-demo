import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TransactionsComponent } from './transactions.component';
import { TransactionsService } from './transactions.service';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { DetailsComponent } from './details/details.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddTransactionComponent } from './add-transaction/add-transaction.component';
import { DeleteTransactionComponent } from './delete/delete.component';
import { MaterialModule } from 'src/app/shared/modules/material.module';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { UpdatePaymentComponent } from './update-payment/update-payment.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    TransactionsRoutingModule,
    PerfectScrollbarModule
  ],
  declarations: [
    TransactionsComponent,
    DetailsComponent,
    AddTransactionComponent,
    DeleteTransactionComponent,
    UpdatePaymentComponent
  ],
  entryComponents: [
    DetailsComponent,
    AddTransactionComponent,
    DeleteTransactionComponent,
    UpdatePaymentComponent
  ],
  providers: [
    TransactionsService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
  ]
})
export class TransactionsModule {}
