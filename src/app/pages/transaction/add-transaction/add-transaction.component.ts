import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TransactionsService } from '../transactions.service';
import * as firebase from 'firebase/app';
import { Timestamp } from '@firebase/firestore-types';
import { CoreService } from 'src/app/shared/services/core.service';

import { ToastrService } from 'ngx-toastr';
import { Transaction } from 'src/app/shared/models/models';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { MatDialogRef } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.scss']
})
export class AddTransactionComponent {
  addTransactionForm: FormGroup = new FormGroup({
    type: new FormControl('', [Validators.required]),
    amount: new FormControl('', [Validators.required, Validators.min(1)]),
    description: new FormControl('')
  });

  constructor(
    public dialogRef: MatDialogRef<AddTransactionComponent>,
    private tService: TransactionsService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService,
    private afStore: AngularFirestore
  ) {}

  get formControls() {
    return this.addTransactionForm.controls;
  }

  private getTransactionDetails() {
    const id = this.afStore.createId();
    const tempDoc = this.afStore.collection('transactions').doc(id).ref;
    const transaction: Transaction = {
      id: tempDoc.id,
      type: this.addTransactionForm.controls['type'].value,
      amount: this.addTransactionForm.controls['amount'].value,
      createdAt: firebase.firestore.FieldValue.serverTimestamp() as Timestamp,
      receivedAt: firebase.firestore.FieldValue.serverTimestamp() as Timestamp
    };
    transaction.cash = true;
    if (this.addTransactionForm.controls['description'].dirty) {
      transaction.description = this.addTransactionForm.controls[
        'description'
      ].value;
    }
    transaction.paymentReceived = true;
    return transaction;
  }

  async addTransaction() {
    this.core.loadingText = 'Creating transaction...';
    this.spinner.show();
    try {
      const transaction = this.getTransactionDetails();
      await this.tService.addTransaction(transaction);
      this.spinner.hide();
      this.toaster.success('Successful!', 'Transaction created');
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Transaction addition');
    }
  }
}
