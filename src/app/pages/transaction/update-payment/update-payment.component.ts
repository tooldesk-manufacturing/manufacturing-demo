import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TransactionsService } from '../transactions.service';
import { CoreService } from 'src/app/shared/services/core.service';
import { SpinnerOverlayService } from 'src/app/shared/spinner/spinner-overlay/spinner-overlay.service';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Transaction } from 'src/app/shared/models/models';
import { Timestamp } from '@firebase/firestore-types';
import * as firebase from 'firebase/app';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-payment',
  templateUrl: './update-payment.component.html',
  styleUrls: ['./update-payment.component.scss']
})
export class UpdatePaymentComponent implements OnInit {
  transaction: Transaction;
  transactionType: string;
  paymentUpdateForm: FormGroup = new FormGroup({
    description: new FormControl('')
  });
  constructor(
    public dialogRef: MatDialogRef<UpdatePaymentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private tService: TransactionsService,
    private core: CoreService,
    private spinner: SpinnerOverlayService,
    private toaster: ToastrService
  ) {}

  ngOnInit() {
    this.transaction = this.data.transaction;
    this.transactionType = this.data.transactionType;
    if (
      this.transaction.description &&
      this.transaction.description.length > 0
    ) {
      this.paymentUpdateForm.controls['description'].setValue(
        this.transaction.description
      );
    }
  }

  get formControls() {
    return this.paymentUpdateForm.controls;
  }

  updateReceived() {
    if (
      (this.paymentUpdateForm.controls['description'].value as string).length >
      0
    ) {
      this.transaction.description = this.paymentUpdateForm.controls[
        'description'
      ].value;
    }
    if (this.transactionType === 'customer') {
      this.paymentReceived();
    } else if (this.transactionType === 'subvendor') {
      this.paymentDoneSubvendor();
    }
  }

  async paymentReceived() {
    this.core.loadingText = 'Updating payment status...';
    this.spinner.show();
    try {
      if (!this.transaction.customerUID) {
        this.transaction.paymentReceived = true;
        this.transaction.receivedAt = firebase.firestore.FieldValue.serverTimestamp() as Timestamp;
        await this.tService.updatePaymentReceived(this.transaction);
        this.spinner.hide();
        this.toaster.success('Successful!', 'Payment received');
        this.dialogRef.close();
      } else {
        const customerSummary = await this.tService
          .getCustomerSummary(this.transaction.customerUID)
          .pipe(take(1))
          .toPromise();
        this.transaction.paymentReceived = true;
        this.transaction.receivedAt = firebase.firestore.FieldValue.serverTimestamp() as Timestamp;
        await this.tService.updatePaymentDoneCustomer(
          this.transaction,
          customerSummary
        );
        this.spinner.hide();
        this.toaster.success('Successful!', 'Payment done');
        this.dialogRef.close();
      }
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Payment status');
      this.dialogRef.close();
    }
  }

  async paymentDoneSubvendor() {
    this.core.loadingText = 'Updating payment status...';
    this.spinner.show();
    try {
      const amountDue = await this.tService
        .getAmountDue(this.transaction.subvendorUID)
        .pipe(take(1))
        .toPromise();
      this.transaction.paymentReceived = true;
      await this.tService.updatePaymentDoneSubvendor(
        this.transaction,
        amountDue
      );
      this.spinner.hide();
      this.toaster.success('Successful!', 'Payment done');
      this.dialogRef.close();
    } catch (reason) {
      this.spinner.hide();
      this.toaster.error(reason, 'Payment status');
      this.dialogRef.close();
    }
  }
}
