import { Component, OnInit } from '@angular/core';
import { UpdateService } from './shared/services/update-service.service';
import { CoreService } from './shared/services/core.service';
import { SpinnerOverlayService } from './shared/spinner/spinner-overlay/spinner-overlay.service';
import { Location } from '@angular/common';
import { AngularFirePerformance } from '@angular/fire/performance';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private spinner: SpinnerOverlayService,
    private update: UpdateService,
    public core: CoreService,
    private location: Location,
    private afPerformance: AngularFirePerformance
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.getOriginalURLAndParams();
  }

  private getOriginalURLAndParams() {
    if (this.location.path()) {
      const url = new URL('https://m.tooldesk.app/' + this.location.path());
      const params = this.getParams(url);
      if (!this.isEmpty(params)) {
        this.core.URLParams = params;
      }
      this.core.initialURL = this.location.path();
    }
  }

  private isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  private getParams(url): {} {
    const params = {};
    const parser = document.createElement('a');
    parser.href = url;
    const query = parser.search.substring(1);
    const vars = query.split('&');
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      if (pair[0].length > 0 && pair[1] !== undefined) {
        params[pair[0]] = decodeURIComponent(pair[1]);
      }
    }
    return params;
  }
}
