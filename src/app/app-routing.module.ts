import { NoaccessComponent } from './shared/noaccess/noaccess.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleGuard } from './shared/guards/module.guard';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: 'noaccess',
    component: NoaccessComponent
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'management',
    loadChildren: () =>
      import('./pages/management/management.module').then(
        m => m.ManagementModule
      ),
    canLoad: [ModuleGuard],
    data: {
      moduleName: 'basic'
    }
  },
  {
    path: 'order',
    loadChildren: () =>
      import('./pages/order/order.module').then(m => m.OrderModule),
    canLoad: [ModuleGuard],
    data: {
      moduleName: 'basic'
    }
  },
  {
    path: 'transactions',
    loadChildren: () =>
      import('./pages/transaction/transactions.module').then(
        m => m.TransactionsModule
      ),
    canLoad: [ModuleGuard],
    data: {
      moduleName: 'basic'
    }
  },
  {
    path: 'billing',
    loadChildren: () =>
      import('./pages/billing/billing.module').then(m => m.BillingModule),
    canLoad: [ModuleGuard],
    data: {
      moduleName: 'basic'
    }
  },
  {
    path: 'delivery-challan',
    loadChildren: () =>
      import('./pages/receipt/receipt.module').then(m => m.ReceiptModule),
    canLoad: [ModuleGuard],
    data: {
      moduleName: 'basic'
    }
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('./pages/settings/settings.module').then(m => m.SettingsModule),
    canLoad: [ModuleGuard],
    data: {
      moduleName: 'basic'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
